
## 1.2.4 [10-14-2024]

* Changes made at 2024.10.14_19:37PM

See merge request itentialopensource/adapters/adapter-digicert_pki!12

---

## 1.2.3 [08-25-2024]

* update dependencies and metadata

See merge request itentialopensource/adapters/adapter-digicert_pki!10

---

## 1.2.2 [08-14-2024]

* Changes made at 2024.08.14_17:40PM

See merge request itentialopensource/adapters/adapter-digicert_pki!9

---

## 1.2.1 [08-07-2024]

* Changes made at 2024.08.06_18:37PM

See merge request itentialopensource/adapters/adapter-digicert_pki!8

---

## 1.2.0 [07-23-2024]

* Minor/2024 auto migration

See merge request itentialopensource/adapters/security/adapter-digicert_pki!7

---

## 1.1.3 [03-27-2024]

* Changes made at 2024.03.27_13:37PM

See merge request itentialopensource/adapters/security/adapter-digicert_pki!6

---

## 1.1.2 [03-11-2024]

* Changes made at 2024.03.11_16:11PM

See merge request itentialopensource/adapters/security/adapter-digicert_pki!5

---

## 1.1.1 [02-27-2024]

* Changes made at 2024.02.27_11:44AM

See merge request itentialopensource/adapters/security/adapter-digicert_pki!4

---

## 1.1.0 [01-03-2024]

* Adapter Migration

See merge request itentialopensource/adapters/security/adapter-digicert_pki!3

---

## 1.0.0 [10-19-2023]

* Add calls and update sampleProperties

See merge request itentialopensource/adapters/security/adapter-digicert_pki!2

---

## 0.1.1 [03-21-2023]

* Bug fixes and performance improvements

See commit 809b9ec

---
