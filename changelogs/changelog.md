
## 1.0.0 [10-19-2023]

* Add calls and update sampleProperties

See merge request itentialopensource/adapters/security/adapter-digicert_pki!2

---

## 0.1.1 [03-21-2023]

* Bug fixes and performance improvements

See commit 809b9ec

---
