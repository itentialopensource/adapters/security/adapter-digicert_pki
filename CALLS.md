## Using this Adapter

The `adapter.js` file contains the calls the adapter makes available to the rest of the Itential Platform. The API detailed for these calls should be available through JSDOC. The following is a brief summary of the calls.

### Generic Adapter Calls

These are adapter methods that IAP or you might use. There are some other methods not shown here that might be used for internal adapter functionality.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">connect()</td>
    <td style="padding:15px">This call is run when the Adapter is first loaded by he Itential Platform. It validates the properties have been provided correctly.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">healthCheck(callback)</td>
    <td style="padding:15px">This call ensures that the adapter can communicate with Adapter for DigiCert PKI. The actual call that is used is defined in the adapter properties and .system entities action.json file.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">refreshProperties(properties)</td>
    <td style="padding:15px">This call provides the adapter the ability to accept property changes without having to restart the adapter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">encryptProperty(property, technique, callback)</td>
    <td style="padding:15px">This call will take the provided property and technique, and return the property encrypted with the technique. This allows the property to be used in the adapterProps section for the credential password so that the password does not have to be in clear text. The adapter will decrypt the property as needed for communications with Adapter for DigiCert PKI.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUpdateAdapterConfiguration(configFile, changes, entity, type, action, callback)</td>
    <td style="padding:15px">This call provides the ability to update the adapter configuration from IAP - includes actions, schema, mockdata and other configurations.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapSuspendAdapter(mode, callback)</td>
    <td style="padding:15px">This call provides the ability to suspend the adapter and either have requests rejected or put into a queue to be processed after the adapter is resumed.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUnsuspendAdapter(callback)</td>
    <td style="padding:15px">This call provides the ability to resume a suspended adapter. Any requests in queue will be processed before new requests.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterQueue(callback)</td>
    <td style="padding:15px">This call will return the requests that are waiting in the queue if throttling is enabled.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapFindAdapterPath(apiPath, callback)</td>
    <td style="padding:15px">This call provides the ability to see if a particular API path is supported by the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapTroubleshootAdapter(props, persistFlag, adapter, callback)</td>
    <td style="padding:15px">This call can be used to check on the performance of the adapter - it checks connectivity, healthcheck and basic get calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterHealthcheck(adapter, callback)</td>
    <td style="padding:15px">This call will return the results of a healthcheck.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterConnectivity(callback)</td>
    <td style="padding:15px">This call will return the results of a connectivity check.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterBasicGet(callback)</td>
    <td style="padding:15px">This call will return the results of running basic get API calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapMoveAdapterEntitiesToDB(callback)</td>
    <td style="padding:15px">This call will push the adapter configuration from the entities directory into the Adapter or IAP Database.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapDeactivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to remove tasks from the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapActivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to add deactivated tasks back into the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapExpandedGenericAdapterRequest(metadata, uriPath, restMethod, pathVars, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This is an expanded Generic Call. The metadata object allows us to provide many new capabilities within the generic request.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequest(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call allows you to provide the path to have the adapter call. It is an easy way to incorporate paths that have not been built into the adapter yet.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequestNoBasePath(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call is the same as the genericAdapterRequest only it does not add a base_path or version to the call.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterLint(callback)</td>
    <td style="padding:15px">Runs lint on the addapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterTests(callback)</td>
    <td style="padding:15px">Runs baseunit and unit tests on the adapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterInventory(callback)</td>
    <td style="padding:15px">This call provides some inventory related information about the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Cache Calls

These are adapter methods that are used for adapter caching. If configured, the adapter will cache based on the interval provided. However, you can force a population of the cache manually as well.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">iapPopulateEntityCache(entityTypes, callback)</td>
    <td style="padding:15px">This call populates the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRetrieveEntitiesCache(entityType, options, callback)</td>
    <td style="padding:15px">This call retrieves the specific items from the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Broker Calls

These are adapter methods that are used to integrate to IAP Brokers. This adapter currently supports the following broker calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">hasEntities(entityType, entityList, callback)</td>
    <td style="padding:15px">This call is utilized by the IAP Device Broker to determine if the adapter has a specific entity and item of the entity.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevice(deviceName, callback)</td>
    <td style="padding:15px">This call returns the details of the requested device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesFiltered(options, callback)</td>
    <td style="padding:15px">This call returns the list of devices that match the criteria provided in the options filter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">isAlive(deviceName, callback)</td>
    <td style="padding:15px">This call returns whether the device status is active</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfig(deviceName, format, callback)</td>
    <td style="padding:15px">This call returns the configuration for the selected device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetDeviceCount(callback)</td>
    <td style="padding:15px">This call returns the count of devices.</td>
    <td style="padding:15px">No</td>
  </tr>
</table>
<br>

### Specific Adapter Calls

Specific adapter calls are built based on the API of the Digicert. The Adapter Builder creates the proper method comments for generating JS-DOC for the adapter. This is the best way to get information on the calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Path</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">getAuditsUsingGET(action, adminRA, dateFrom, dateTo, limit, offset, callback)</td>
    <td style="padding:15px">This API is used to get audit details for given query param</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/audit-log?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAuditUsingGET(auditId, callback)</td>
    <td style="padding:15px">This API is used to get audit details for given audit Id.</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/audit-log/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCAInfoUsingGET(caId, callback)</td>
    <td style="padding:15px">This API is used to get public CA details for a given ca identifier</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/ca/id/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCAUsingGET(seatType, callback)</td>
    <td style="padding:15px">This API is used to get public CA details for a given account api key</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/ca/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enrollCertificateUsingPOST(enrollCertificateRequest, callback)</td>
    <td style="padding:15px">This API is used to enroll a certificate for a given profile.</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/certificate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createCertificateUsingPOST(enrollCertificateRequest, callback)</td>
    <td style="padding:15px">This API is used to enroll a certificate for a given profile.</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/certificate/create?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">renewExternalCertificateUsingPOST(enrollCertificateRequest, serialNumber, callback)</td>
    <td style="padding:15px">This API is used to enroll a certificate for a given profile.</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/certificate/renew/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCertificateUsingGET(serialNumber, callback)</td>
    <td style="padding:15px">This API is used to get certificate details for a given certificate serial number</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/certificate/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">recoverKeyUsingGET(serialNumber, callback)</td>
    <td style="padding:15px">This API is used to get private key information for key-escrowed certificate with given serial numb</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/certificate/{pathv1}/key?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">renewCertificateUsingPOST(enrollCertificateRequest, serialNumber, callback)</td>
    <td style="padding:15px">This API is used to renew certificate with the given serial number.</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/certificate/{pathv1}/renew?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">revokeCertificateUsingPUT(revokeCertificateRequest, serialNumber, callback)</td>
    <td style="padding:15px">This API can revoke certificate with the given serial number</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/certificate/{pathv1}/revoke?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">unRevokeCertificateUsingDELETE(serialNumber, callback)</td>
    <td style="padding:15px">This API can resume certificate with the given serial number.</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/certificate/{pathv1}/revoke?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createPasscodeUsingPOST(createPasscodeRequest, callback)</td>
    <td style="padding:15px">This API is used to enroll user for profile.</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/enrollment?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getEnrollmentUsingGET(enrollCode, seatId, callback)</td>
    <td style="padding:15px">This API is used to get details of an existing enrollment.</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/enrollment/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">resetPasscodeUsingPUT(enrollCode, resetPasscodeRequest, callback)</td>
    <td style="padding:15px">This API is used to reset enrollment code.</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/enrollment/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteEnrollmentUsingDELETE(deleteEnrollRequest, enrollCode, callback)</td>
    <td style="padding:15px">This API can delete an enrollment for given enrollment id</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/enrollment/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enrollStatusUsingGET(profileId, seatId, callback)</td>
    <td style="padding:15px">This API is used to get certificate enrollment status.</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/enrollstatus/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">helloUsingGET(callback)</td>
    <td style="padding:15px">Hello API</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/hello?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllProfilesUsingGET(callback)</td>
    <td style="padding:15px">This API is used to get profile details of all profiles in an account and its subaccounts</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/profile?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProfileUsingGET(profileId, callback)</td>
    <td style="padding:15px">This API is used to get profile details for a given profile id</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/profile/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">activateProfileUsingPUT(profileCreationRequest, callback)</td>
    <td style="padding:15px">This API is used to suspend a profile</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/profile/activate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createProfileUsingPOST(profileCreationRequest, callback)</td>
    <td style="padding:15px">This API is used to create a profile</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/profile/create?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteProfileUsingPUT(profileCreationRequest, callback)</td>
    <td style="padding:15px">This API is used to delete a profile</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/profile/delete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateProfileUsingPUT(profileCreationRequest, callback)</td>
    <td style="padding:15px">This API is used to create a profile</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/profile/edit?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">suspendProfileUsingPUT(profileCreationRequest, callback)</td>
    <td style="padding:15px">This API is used to suspend a profile</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/profile/suspend?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">testCreateUsingPOST(callback)</td>
    <td style="padding:15px">This API is used to create a profile</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/profile/test?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">searchCertUsingPOST(searchReq, callback)</td>
    <td style="padding:15px">This API is used to search for certificates.</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/searchcert?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createSeatUsingPOST(createSeatRequest, callback)</td>
    <td style="padding:15px">This API is used to create a seat. This seat can be used for  a user, device, server or organizatio</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/seat?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSeatUsingGET(seatId, callback)</td>
    <td style="padding:15px">This API is used to get information regarding a seat.</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/seat/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateSeatUsingPUT(seatId, updateSeatRequest, callback)</td>
    <td style="padding:15px">This API is used to change the seat details.</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/seat/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSeatUsingDELETE(seatId, callback)</td>
    <td style="padding:15px">This API is used to delete a seat. Deleting a seat will revoke all the certificates associated to t</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/seat/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listCertificates(accountId, divisionId, status, serialNumber, commonName, certificateValue, deviceIdentifier, enrollmentMethod, createdFrom, createdTo, validToStart, validToEnd, issuerCommonName, issuerSerialNumber, keyType, enrollmentProfileId, revokedFrom, revokedTo, ocspGroup, includedInOcspGroup, deviceId, certificateType, createdInLastNDays, createdInCurrentMonth, createdInPreviousMonth, expiringInNextNDays, expiringInRemainingOfCurrentMonth, expiringInNextMonth, keyUsage, extendedKeyUsage, certificatePolicies, authenticationId, authenticationType, authenticationName, caConnectorType, renewed, sortBy, sortDirection, limit, offset, callback)</td>
    <td style="padding:15px">listCertificates</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/certificate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getExampleCertificateRequestCertificateID(certificateId, callback)</td>
    <td style="padding:15px">getExampleCertificateRequestCertificateID</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/certificate/{pathv1}/example-request?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">renewCertificate(certificateId, body, callback)</td>
    <td style="padding:15px">renewCertificate</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/certificate/{pathv1}/renew?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">renewBySerial(certificateSerialNumber, body, callback)</td>
    <td style="padding:15px">renewBySerial</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/certificate/serial-number/{pathv1}/renew?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">revokeCertificate(certificateId, body, callback)</td>
    <td style="padding:15px">revokeCertificate</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/certificate/{pathv1}/revoke?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNote(certificateId, body, callback)</td>
    <td style="padding:15px">updateNote</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/certificate/{pathv1}/note?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">revokeCertificateBySerialNumber(certificateSerialNumber, body, callback)</td>
    <td style="padding:15px">revokeCertificateBySerialNumber</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/certificate/serial-number/{pathv1}/revoke?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">downloadCertificate(certificateId, format, callback)</td>
    <td style="padding:15px">downloadCertificate</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/certificate/{pathv1}/download/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">downloadCertificateBySerialNumber(certificateSerialNumber, format, callback)</td>
    <td style="padding:15px">downloadCertificateBySerialNumber</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/certificate/serial-number/{pathv1}/download/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">importCertificate(enrollmentProfileId, file, callback)</td>
    <td style="padding:15px">importCertificate</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/certificate-import/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getImportDetails(jobId, callback)</td>
    <td style="padding:15px">getImportDetails</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/certificate-import/job/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCertificateStatus(certificateSerialNumber, callback)</td>
    <td style="padding:15px">getCertificateStatus</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/certificate-status/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCertificateStatusById(certificateId, callback)</td>
    <td style="padding:15px">getCertificateStatusById</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/certificate-status/certificate-id/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listCertificatesByCertificateValue(certificateValue, limit, offset, callback)</td>
    <td style="padding:15px">listCertificatesByCertificateValue</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/certificate/certificate-value/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">downloadApprovedCertificate(certificateId, body, callback)</td>
    <td style="padding:15px">downloadApprovedCertificate</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/certificate/{pathv1}/download-approved-certificate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listAuthenticationCACertificates(accountId, name, enrollmentProfileId, enrollmentProfileName, createdFrom, createdTo, validFromStart, validFromEnd, validToStart, validToEnd, status, sortBy, sortDirection, limit, offset, callback)</td>
    <td style="padding:15px">listAuthenticationCACertificates</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/authentication-ca?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listAuthenticationCACertificatesByAccount(accountId, name, enrollmentProfileId, enrollmentProfileName, createdFrom, createdTo, validFromStart, validFromEnd, validToStart, validToEnd, status, sortBy, sortDirection, limit, offset, callback)</td>
    <td style="padding:15px">listAuthenticationCACertificatesByAccount</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/account/{pathv1}/authentication-ca?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">uploadAuthenticationCACertificate(accountId, file, callback)</td>
    <td style="padding:15px">uploadAuthenticationCACertificate</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/account/{pathv1}/authentication-ca?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAuthenticationCACertificate(accountId, authenticationCaId, callback)</td>
    <td style="padding:15px">getAuthenticationCACertificate</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/account/{pathv1}/authentication-ca/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAuthenticationCACertificate(accountId, authenticationCaId, callback)</td>
    <td style="padding:15px">deleteAuthenticationCACertificate</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/account/{pathv1}/authentication-ca/{pathv2}/delete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createAuthenticationCATemplate(body, callback)</td>
    <td style="padding:15px">createAuthenticationCATemplate</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/cert-auth-template?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listAuthenticationCATemplate(name, accountId, divisionId, enrollmentProfileId, icaId, status, useForAllCertificates, createdAtFrom, createdAtTo, sortBy, sortDirection, limit, offset, callback)</td>
    <td style="padding:15px">listAuthenticationCATemplate</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/cert-auth-template?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateAuthenticationCATemplate(authenticationCaTemplateId, body, callback)</td>
    <td style="padding:15px">updateAuthenticationCATemplate</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/cert-auth-template/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAuthenticationCATemplate(authenticationCaTemplateId, callback)</td>
    <td style="padding:15px">getAuthenticationCATemplate</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/cert-auth-template/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">disableAuthenticationCATemplate(authenticationCaTemplateId, callback)</td>
    <td style="padding:15px">disableAuthenticationCATemplate</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/cert-auth-template/{pathv1}/disable?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAuthenticationCATemplate(authenticationCaTemplateId, callback)</td>
    <td style="padding:15px">deleteAuthenticationCATemplate</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/cert-auth-template/{pathv1}/delete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">restoreAuthenticationCATemplate(authenticationCaTemplateId, callback)</td>
    <td style="padding:15px">restoreAuthenticationCATemplate</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/cert-auth-template/{pathv1}/undelete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enableAuthenticationCATemplate(authenticationCaTemplateId, callback)</td>
    <td style="padding:15px">enableAuthenticationCATemplate</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/cert-auth-template/{pathv1}/enable?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listAssignableAuthenticationCACertificates(enrollmentProfileId, name, limit, offset, callback)</td>
    <td style="padding:15px">listAssignableAuthenticationCACertificates</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/cert-auth-template/assignable-ca/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addAuthenticationCertificate(body, callback)</td>
    <td style="padding:15px">addAuthenticationCertificate</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/authentication-certificate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listAuthenticationCertificates(accountId, divisionId, name, authenticationCaCommonName, templateId, authenticationCaId, enrollmentProfileId, enrollmentProfileName, createdFrom, createdTo, usageLimitFrom, usageLimitTo, expiresFrom, expiresTo, status, sortBy, sortDirection, limit, offset, callback)</td>
    <td style="padding:15px">listAuthenticationCertificates</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/authentication-certificate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAuthenticationCertificateDetails(authenticationCertificateId, callback)</td>
    <td style="padding:15px">getAuthenticationCertificateDetails</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/authentication-certificate/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateAuthenticationCertificate(authenticationCertificateId, body, callback)</td>
    <td style="padding:15px">updateAuthenticationCertificate</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/authentication-certificate/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">disableAuthenticationCertificate(authenticationCertificateId, callback)</td>
    <td style="padding:15px">disableAuthenticationCertificate</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/authentication-certificate/{pathv1}/disable?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enableAuthenticationCertificate(authenticationCertificateId, callback)</td>
    <td style="padding:15px">enableAuthenticationCertificate</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/authentication-certificate/{pathv1}/enable?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAuthenticationCertificate(authenticationCertificateId, callback)</td>
    <td style="padding:15px">deleteAuthenticationCertificate</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/authentication-certificate/{pathv1}/delete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">restoreAuthenticationCertificate(authenticationCertificateId, callback)</td>
    <td style="padding:15px">restoreAuthenticationCertificate</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/authentication-certificate/{pathv1}/undelete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listCertificateProfiles(name, accountId, divisionId, templateName, status, createdFrom, createdTo, sortBy, sortDirection, limit, offset, callback)</td>
    <td style="padding:15px">listCertificateProfiles</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/certificate-profile?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createCertificateProfile(body, callback)</td>
    <td style="padding:15px">createCertificateProfile</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/certificate-profile?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCertificateProfile(certificateProfileId, callback)</td>
    <td style="padding:15px">getCertificateProfile</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/certificate-profile/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateCertificateProfile(certificateProfileId, body, callback)</td>
    <td style="padding:15px">updateCertificateProfile</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/certificate-profile/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">disableCertificateProfile(certificateProfileId, callback)</td>
    <td style="padding:15px">disableCertificateProfile</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/certificate-profile/{pathv1}/disable?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enableCertificateProfile(certificateProfileId, callback)</td>
    <td style="padding:15px">enableCertificateProfile</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/certificate-profile/{pathv1}/enable?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCertificateProfile(certificateProfileId, callback)</td>
    <td style="padding:15px">deleteCertificateProfile</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/certificate-profile/{pathv1}/delete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">restoreCertificateProfile(certificateProfileId, callback)</td>
    <td style="padding:15px">restoreCertificateProfile</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/certificate-profile/{pathv1}/undelete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">assignCertificateProfileDivisions(certificateProfileId, body, callback)</td>
    <td style="padding:15px">assignCertificateProfileDivisions</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/certificate-profile/{pathv1}/division/assign?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">unassignCertificateProfileDivisions(certificateProfileId, body, callback)</td>
    <td style="padding:15px">unassignCertificateProfileDivisions</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/certificate-profile/{pathv1}/division/unassign?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRequestStatus(certificateRequestId, callback)</td>
    <td style="padding:15px">getRequestStatus</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/certificate-request/{pathv1}/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listCertificateTemplates(accountId, name, createdFrom, createdTo, status, type, limitByAccount, format, sortBy, sortDirection, limit, offset, callback)</td>
    <td style="padding:15px">listCertificateTemplates</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/certificate-template?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createCertificateTemplate(body, callback)</td>
    <td style="padding:15px">createCertificateTemplate</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/certificate-template?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCertificateTemplate(certificateTemplateId, callback)</td>
    <td style="padding:15px">getCertificateTemplate</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/certificate-template/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateCertificateTemplate(certificateTemplateId, body, callback)</td>
    <td style="padding:15px">updateCertificateTemplate</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/certificate-template/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">clonecertificatetemplate(certificateTemplateId, body, callback)</td>
    <td style="padding:15px">Clone certificate template</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/certificate-template/{pathv1}/clone?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">disableCertificateTemplate(certificateTemplateId, callback)</td>
    <td style="padding:15px">disableCertificateTemplate</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/certificate-template/{pathv1}/disable?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enableCertificateTemplate(certificateTemplateId, callback)</td>
    <td style="padding:15px">enableCertificateTemplate</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/certificate-template/{pathv1}/enable?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCertificateTemplate(certificateTemplateId, callback)</td>
    <td style="padding:15px">deleteCertificateTemplate</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/certificate-template/{pathv1}/delete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">restoreCertificateTemplate(certificateTemplateId, callback)</td>
    <td style="padding:15px">restoreCertificateTemplate</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/certificate-template/{pathv1}/undelete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createDevice(body, callback)</td>
    <td style="padding:15px">createDevice</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/device?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceList(deviceIdentifier, createdFrom, createdTo, divisionId, updatedFrom, updatedTo, deviceProfileId, pagination, limit, offset, callback)</td>
    <td style="padding:15px">getDeviceList</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/device?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceDetails(deviceIdentifier, divisionId, callback)</td>
    <td style="padding:15px">getDeviceDetails</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/device/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateDevice(deviceIdentifier, divisionId, body, callback)</td>
    <td style="padding:15px">updateDevice</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/device/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">archiveDevice(deviceIdentifier, divisionId, callback)</td>
    <td style="padding:15px">archiveDevice</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/device/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enableDevice(deviceIdentifier, divisionId, callback)</td>
    <td style="padding:15px">enableDevice</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/device/{pathv1}/enable?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">disableDevice(deviceIdentifier, divisionId, callback)</td>
    <td style="padding:15px">disableDevice</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/device/{pathv1}/disable?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">restoreDevice(deviceIdentifier, divisionId, callback)</td>
    <td style="padding:15px">restoreDevice</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/device/{pathv1}/undelete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createDeviceV2(body, callback)</td>
    <td style="padding:15px">createDevice_v2</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/device?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceListV2(deviceIdentifier, createdFrom, createdTo, divisionId, updatedFrom, updatedTo, deviceProfileId, pagination, limit, offset, callback)</td>
    <td style="padding:15px">getDeviceList_v2</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/device?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceDetailsV2(deviceId, callback)</td>
    <td style="padding:15px">getDeviceDetails_v2</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/device/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateDeviceV2(deviceId, body, callback)</td>
    <td style="padding:15px">updateDevice_v2</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/device/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">archiveDeviceV2(deviceId, callback)</td>
    <td style="padding:15px">archiveDevice_v2</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/device/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enableDeviceV2(deviceId, callback)</td>
    <td style="padding:15px">enableDevice_v2</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/device/{pathv1}/enable?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">disableDeviceV2(deviceId, callback)</td>
    <td style="padding:15px">disableDevice_v2</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/device/{pathv1}/disable?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">restoreDeviceV2(deviceId, callback)</td>
    <td style="padding:15px">restoreDevice_v2</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/device/{pathv1}/undelete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">renewDeviceCertificateV2(deviceId, body, callback)</td>
    <td style="padding:15px">renewDeviceCertificate_v2</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/device/{pathv1}/renew?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listDivisions(name, accountId, status, sortBy, sortDirection, limit, offset, callback)</td>
    <td style="padding:15px">listDivisions</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/division/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createDivision(body, callback)</td>
    <td style="padding:15px">createDivision</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/division/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDivision(divisionId, callback)</td>
    <td style="padding:15px">getDivision</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/division/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateDivision(divisionId, body, callback)</td>
    <td style="padding:15px">updateDivision</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/division/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">disableDivision(divisionId, callback)</td>
    <td style="padding:15px">disableDivision</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/division/{pathv1}/disable?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enableDivision(divisionId, callback)</td>
    <td style="padding:15px">enableDivision</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/division/{pathv1}/enable?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDivision(divisionId, callback)</td>
    <td style="padding:15px">deleteDivision</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/division/{pathv1}/delete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">restoreDivision(divisionId, callback)</td>
    <td style="padding:15px">restoreDivision</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/division/{pathv1}/undelete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">assignDivisionUsers(divisionId, body, callback)</td>
    <td style="padding:15px">assignDivisionUsers</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/division/{pathv1}/users/assign?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listDivisionUsers(divisionId, limit, offset, callback)</td>
    <td style="padding:15px">listDivisionUsers</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/division/{pathv1}/users?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">removeDivisionUsers(divisionId, body, callback)</td>
    <td style="padding:15px">removeDivisionUsers</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/division/{pathv1}/users/remove?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">assignICAstodivision(divisionId, body, callback)</td>
    <td style="padding:15px">Assign ICAs to division</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/division/{pathv1}/ica/assign?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">removeICAsfromdivision(divisionId, body, callback)</td>
    <td style="padding:15px">Remove ICAs from division</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/division/{pathv1}/ica/remove?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listdivisionICAassignments(divisionId, commonName, issuerCommonName, serialNumber, status, enrollmentProfileId, validFromFrom, validFromTo, validToFrom, validToTo, sortBy, sortDirection, limit, offset, callback)</td>
    <td style="padding:15px">List division ICA assignments</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/division/{pathv1}/ica?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listEnrollmentProfiles(name, deviceProfileId, deviceProfileName, enrollmentMethods, ipAddresses, createdFrom, createdTo, certificateType, status, sortBy, sortDirection, limit, offset, callback)</td>
    <td style="padding:15px">listEnrollmentProfiles</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/enrollment-profile?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createEnrollmentProfile(body, callback)</td>
    <td style="padding:15px">createEnrollmentProfile</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/enrollment-profile?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getExampleCertificateRequestEnrollmentProfileID(enrollmentProfileId, callback)</td>
    <td style="padding:15px">getExampleCertificateRequestEnrollmentProfileID</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/enrollment-profile/{pathv1}/example-request?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getEnrollmentProfile(enrollmentProfileId, callback)</td>
    <td style="padding:15px">getEnrollmentProfile</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/enrollment-profile/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateEnrollmentProfile(enrollmentProfileId, body, callback)</td>
    <td style="padding:15px">updateEnrollmentProfile</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/enrollment-profile/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listAssignableICAs(enrollmentProfileId, limit, offset, callback)</td>
    <td style="padding:15px">listAssignableICAs</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/enrollment-profile/{pathv1}/assignable-ca?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIssuingICA(enrollmentProfileId, includeChain, callback)</td>
    <td style="padding:15px">getIssuingICA</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/enrollment-profile/{pathv1}/issuer?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getEnrollmentProfileSpecification(enrollmentProfileId, callback)</td>
    <td style="padding:15px">getEnrollmentProfileSpecification</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/enrollment-profile/{pathv1}/enrollment-specification?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getEnrollmentProfileStatus(enrollmentProfileId, callback)</td>
    <td style="padding:15px">getEnrollmentProfileStatus</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/enrollment-profile/{pathv1}/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listEnrollmentPasscodes(accountId, divisionId, name, enrollmentProfileId, enrollmentProfileName, deviceProfileId, createdFrom, createdTo, usageLimitFrom, usageLimitTo, expiresFrom, expiresTo, status, username, sortBy, sortDirection, limit, offset, callback)</td>
    <td style="padding:15px">listEnrollmentPasscodes</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/passcode?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createEnrollmentPasscode(enrollmentProfileId, body, callback)</td>
    <td style="padding:15px">createEnrollmentPasscode</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/enrollment-profile/{pathv1}/passcode?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getEnrollmentPasscodeDetails(enrollmentProfileId, passcodeId, callback)</td>
    <td style="padding:15px">getEnrollmentPasscodeDetails</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/enrollment-profile/{pathv1}/passcode/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateEnrollmentPasscode(enrollmentProfileId, passcodeId, body, callback)</td>
    <td style="padding:15px">updateEnrollmentPasscode</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/enrollment-profile/{pathv1}/passcode/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteEnrollmentPasscode(enrollmentProfileId, passcodeId, callback)</td>
    <td style="padding:15px">deleteEnrollmentPasscode</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/enrollment-profile/{pathv1}/passcode/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">restoreEnrollmentPasscode(enrollmentProfileId, passcodeId, callback)</td>
    <td style="padding:15px">restoreEnrollmentPasscode</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/enrollment-profile/{pathv1}/passcode/{pathv2}/undelete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">disableEnrollmentPasscode(enrollmentProfileId, passcodeId, callback)</td>
    <td style="padding:15px">disableEnrollmentPasscode</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/enrollment-profile/{pathv1}/passcode/{pathv2}/disable?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enableEnrollmentPasscode(enrollmentProfileId, passcodeId, callback)</td>
    <td style="padding:15px">enableEnrollmentPasscode</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/enrollment-profile/{pathv1}/passcode/{pathv2}/enable?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">regenerateEnrollmentPasscode(enrollmentProfileId, passcodeId, callback)</td>
    <td style="padding:15px">regenerateEnrollmentPasscode</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/enrollment-profile/{pathv1}/passcode/{pathv2}/regenerate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createAcmeAuthentication(body, callback)</td>
    <td style="padding:15px">createAcmeAuthentication</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/acme-authentication?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listAcmeAuthentication(accountId, name, createdAtFrom, createdAtTo, type, certificateCommonName, issuerCommonName, status, enrollmentProfileId, sortBy, sortDirection, limit, offset, callback)</td>
    <td style="padding:15px">listAcmeAuthentication</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/acme-authentication?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAcmeAuthentication(acmeAuthenticationId, callback)</td>
    <td style="padding:15px">getAcmeAuthentication</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/acme-authentication/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateAcmeAuthentication(acmeAuthenticationId, body, callback)</td>
    <td style="padding:15px">updateAcmeAuthentication</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/acme-authentication/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">disableAcmeAuthentication(acmeAuthenticationId, callback)</td>
    <td style="padding:15px">disableAcmeAuthentication</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/acme-authentication/{pathv1}/disable?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enableAcmeAuthentication(acmeAuthenticationId, callback)</td>
    <td style="padding:15px">enableAcmeAuthentication</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/acme-authentication/{pathv1}/enable?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAcmeAuthentication(acmeAuthenticationId, callback)</td>
    <td style="padding:15px">deleteAcmeAuthentication</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/acme-authentication/{pathv1}/delete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">restoreAcmeAuthentication(acmeAuthenticationId, callback)</td>
    <td style="padding:15px">restoreAcmeAuthentication</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/acme-authentication/{pathv1}/undelete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">batchCSREnroll(enrollmentProfileId, name, certificateFormat, data, emails, description, externalEmails, passcodeGenerationOption, passcode, inputFormat, reportFormat, callback)</td>
    <td style="padding:15px">batchCSREnroll</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/enrollment-profile/{pathv1}/batch-enroll?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">batchEnrollWithKeyGen(enrollmentProfileId, name, certificateFormat, privateKeyFormat, privateKeySyntax, reportFormat, data, emails, keyType, certificate, description, externalEmails, passcodeGenerationOption, passcode, callback)</td>
    <td style="padding:15px">batchEnrollWithKeyGen</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/enrollment-profile/{pathv1}/batch-enroll-key-gen?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">batchEnrollWithKeyGenMac(enrollmentProfileId, name, certificateFormat, privateKeyFormat, privateKeySyntax, reportFormat, macAddressFrom, macAddressNumber, macAddressStep, emails, keyType, certificate, description, externalEmails, passcodeGenerationOption, passcode, callback)</td>
    <td style="padding:15px">batchEnrollWithKeyGenMac</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/enrollment-profile/{pathv1}/batch-enroll-key-gen-mac?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listBatchJobs(accountId, divisionId, statuses, name, fileName, enrollmentProfileId, createdAtFrom, createdAtTo, requestedById, approvedById, dataAvailable, sortBy, sortDirection, limit, offset, callback)</td>
    <td style="padding:15px">listBatchJobs</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/batch-enroll?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getBatchJobSettings(jobId, callback)</td>
    <td style="padding:15px">getBatchJobSettings</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/batch-enroll/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateBatchJobSettings(jobId, body, callback)</td>
    <td style="padding:15px">updateBatchJobSettings</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/batch-enroll/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">regenerateBatchJobPasscode(jobId, callback)</td>
    <td style="padding:15px">regenerateBatchJobPasscode</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/batch-enroll/{pathv1}/regenerate-passcode?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">approveJob(jobId, callback)</td>
    <td style="padding:15px">approveJob</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/batch-enroll/{pathv1}/approve?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rejectJob(jobId, body, callback)</td>
    <td style="padding:15px">rejectJob</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/batch-enroll/{pathv1}/reject?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">downloadReport(jobId, callback)</td>
    <td style="padding:15px">downloadReport</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/batch-enroll/{pathv1}/download-report?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">downloadCertificates(jobId, callback)</td>
    <td style="padding:15px">downloadCertificates</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/batch-enroll/{pathv1}/download-certificates?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDownloadUrls(jobId, callback)</td>
    <td style="padding:15px">deleteDownloadUrls</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/batch-enroll/{pathv1}/delete-download-urls?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">resendDownloadUrls(jobId, callback)</td>
    <td style="padding:15px">resendDownloadUrls</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/batch-enroll/{pathv1}/resend-download-urls?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">batchEnrollExternal(enrollmentProfileId, name, data, emails, keyType, signatureAlgorithm, description, callback)</td>
    <td style="padding:15px">batchEnrollExternal</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/enrollment-profile/{pathv1}/external-batch-enroll?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">downloadMapping(jobId, callback)</td>
    <td style="padding:15px">downloadMapping</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/batch-enroll/{pathv1}/download-mapping?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">downloadByJobIdAndUniqueId(jobId, uniqueIdentifier, format, callback)</td>
    <td style="padding:15px">downloadByJobIdAndUniqueId</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/certificate-download/job/{pathv1}/unique-identifier/{pathv2}/format/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">downloadByCommonName(commonName, format, callback)</td>
    <td style="padding:15px">downloadByCommonName</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/certificate-download/common-name/{pathv1}/format/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">downloadByJobIdAndCommonName(jobId, commonName, format, callback)</td>
    <td style="padding:15px">downloadByJobIdAndCommonName</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/certificate-download/job/{pathv1}/common-name/{pathv2}/format/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">downloadByCertificateValue(certificateValue, format, callback)</td>
    <td style="padding:15px">downloadByCertificateValue</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/certificate-download/certificate-value/{pathv1}/format/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">downloadByJobIdAndCertificateValue(jobId, certificateValue, format, callback)</td>
    <td style="padding:15px">downloadByJobIdAndCertificateValue</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/certificate-download/job/{pathv1}/certificate-value/{pathv2}/format/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createDeviceProfile(body, callback)</td>
    <td style="padding:15px">createDeviceProfile</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/device-profile?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listdeviceprofiles(name, status, accountId, divisionId, limit, offset, callback)</td>
    <td style="padding:15px">List device profiles</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/device-profile?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceProfile(deviceProfileId, callback)</td>
    <td style="padding:15px">getDeviceProfile</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/device-profile/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateDeviceProfile(deviceProfileId, body, callback)</td>
    <td style="padding:15px">updateDeviceProfile</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/device-profile/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDeviceProfile(deviceProfileId, callback)</td>
    <td style="padding:15px">deleteDeviceProfile</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/device-profile/{pathv1}/delete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">restoreDeviceProfile(deviceProfileId, callback)</td>
    <td style="padding:15px">restoreDeviceProfile</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/device-profile/{pathv1}/undelete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getICA(icaId, callback)</td>
    <td style="padding:15px">getICA</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/managed-ca/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listICAs(accountId, divisionId, commonName, serialNumber, includePemCaBody, includePemCaChain, validFromFrom, validFromTo, validToFrom, validToTo, sortBy, sortDirection, limit, offset, callback)</td>
    <td style="padding:15px">List ICAs</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/managed-ca?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getGatewayDetails(gatewayId, callback)</td>
    <td style="padding:15px">getGatewayDetails</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/gateway/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateGateway(gatewayId, body, callback)</td>
    <td style="padding:15px">updateGateway</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/gateway/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listGateways(accountId, name, ipAddress, status, createdAtFrom, createdAtTo, configStatus, macAddress, sortBy, sortDirection, limit, offset, callback)</td>
    <td style="padding:15px">listGateways</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/gateway?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createGateway(body, callback)</td>
    <td style="padding:15px">createGateway</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/gateway?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enableGateway(gatewayId, callback)</td>
    <td style="padding:15px">enableGateway</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/gateway/{pathv1}/enable?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">disableGateway(gatewayId, callback)</td>
    <td style="padding:15px">disableGateway</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/gateway/{pathv1}/disable?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteGateway(gatewayId, callback)</td>
    <td style="padding:15px">deleteGateway</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/gateway/{pathv1}/delete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">restoreGateway(gatewayId, callback)</td>
    <td style="padding:15px">restoreGateway</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/gateway/{pathv1}/undelete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">downloadGatewayConfig(gatewayId, callback)</td>
    <td style="padding:15px">downloadGatewayConfig</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/gateway/{pathv1}/config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listOCSPGroups(accountId, divisionId, name, status, certificateCommonName, certificateSerialNumber, issuerCommonName, issuerSerialNumber, deviceIdentifier, certificateProfileId, enrollmentProfileId, createdAtFrom, createdAtTo, sortBy, sortDirection, limit, offset, callback)</td>
    <td style="padding:15px">listOCSPGroups</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/ocsp-group?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOCSPGroup(body, callback)</td>
    <td style="padding:15px">createOCSPGroup</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/ocsp-group?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOCSPGroupDetails(ocspGroupId, callback)</td>
    <td style="padding:15px">getOCSPGroupDetails</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/ocsp-group/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateOCSPGroup(ocspGroupId, body, callback)</td>
    <td style="padding:15px">updateOCSPGroup</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/ocsp-group/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteOCSPGroup(ocspGroupId, callback)</td>
    <td style="padding:15px">deleteOCSPGroup</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/ocsp-group/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addCertsOCSPGroup(ocspGroupId, body, callback)</td>
    <td style="padding:15px">addCertsOCSPGroup</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/ocsp-group/{pathv1}/add-certificates?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">removeCertsOCSPGroup(ocspGroupId, body, callback)</td>
    <td style="padding:15px">removeCertsOCSPGroup</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/ocsp-group/{pathv1}/remove-certificates?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listOCSPGroupCerts(ocspGroupId, accountId, divisionId, status, serialNumber, commonName, certificateValue, deviceIdentifier, enrollmentMethod, createdFrom, createdTo, validToStart, validToEnd, issuerCommonName, issuerSerialNumber, keyType, enrollmentProfileId, revokedFrom, revokedTo, deviceId, certificateType, createdInLastNDays, createdInCurrentMonth, createdInPreviousMonth, expiringInNextNDays, expiringInRemainingOfCurrentMonth, expiringInNextMonth, keyUsage, extendedKeyUsage, certificatePolicies, authenticationId, authenticationType, authenticationName, caConnectorType, sortBy, sortDirection, limit, offset, pagination, callback)</td>
    <td style="padding:15px">listOCSPGroupCerts</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/ocsp-group/{pathv1}/certificates?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
