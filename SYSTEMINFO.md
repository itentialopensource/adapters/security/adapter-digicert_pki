# DigiCert PKI

Vendor: DigiCert
Homepage: https://www.digicert.com/

Product: PKI Platform
Product Page: https://knowledge.digicert.com/content/dam/kb/attachments/pki-platform/soap-api-client-package/pki-web-services-developers-guide.pdf

## Introduction
We classify DigiCert PKI into the Security (SASE) domain as DigiCert PKI provides essential services for managing digital certificates, which are crucial for securing communications, authenticating identities and ensuring data integrity.
 
## Why Integrate
The DigiCert PKI adapter from Itential is used to integrate the Itential Automation Platform (IAP) with DigiCert PKI Platform. With this adapter you have the ability to perform operations such as:

- Certificates

## Additional Product Documentation
The [API documents for DigiCert PKI](https://knowledge.digicert.com/general-information/digicert-rest-api-documentation)