/* @copyright Itential, LLC 2019 (pre-modifications) */

// Set globals
/* global describe it log pronghornProps */
/* eslint global-require: warn */
/* eslint no-unused-vars: warn */
/* eslint import/no-dynamic-require:warn */

// include required items for testing & logging
const assert = require('assert');
const path = require('path');
const util = require('util');
const execute = require('child_process').execSync;
const fs = require('fs-extra');
const mocha = require('mocha');
const winston = require('winston');
const { expect } = require('chai');
const { use } = require('chai');
const td = require('testdouble');
const Ajv = require('ajv');

const ajv = new Ajv({ strictSchema: false, allErrors: true, allowUnionTypes: true });
const anything = td.matchers.anything();
let logLevel = 'none';
const isRapidFail = false;

// read in the properties from the sampleProperties files
let adaptdir = __dirname;
if (adaptdir.endsWith('/test/integration')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 17);
} else if (adaptdir.endsWith('/test/unit')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 10);
}
const samProps = require(`${adaptdir}/sampleProperties.json`).properties;

// these variables can be changed to run in integrated mode so easier to set them here
// always check these in with bogus data!!!
samProps.stub = true;
samProps.host = 'replace.hostorip.here';
samProps.authentication.username = 'username';
samProps.authentication.password = 'password';
samProps.protocol = 'http';
samProps.port = 80;
samProps.ssl.enabled = false;
samProps.ssl.accept_invalid_cert = false;
samProps.request.attempt_timeout = 1200000;
const attemptTimeout = samProps.request.attempt_timeout;
const { stub } = samProps;

// these are the adapter properties. You generally should not need to alter
// any of these after they are initially set up
global.pronghornProps = {
  pathProps: {
    encrypted: false
  },
  adapterProps: {
    adapters: [{
      id: 'Test-digicert_pki',
      type: 'Digicert',
      properties: samProps
    }]
  }
};

global.$HOME = `${__dirname}/../..`;

// set the log levels that Pronghorn uses, spam and trace are not defaulted in so without
// this you may error on log.trace calls.
const myCustomLevels = {
  levels: {
    spam: 6,
    trace: 5,
    debug: 4,
    info: 3,
    warn: 2,
    error: 1,
    none: 0
  }
};

// need to see if there is a log level passed in
process.argv.forEach((val) => {
  // is there a log level defined to be passed in?
  if (val.indexOf('--LOG') === 0) {
    // get the desired log level
    const inputVal = val.split('=')[1];

    // validate the log level is supported, if so set it
    if (Object.hasOwnProperty.call(myCustomLevels.levels, inputVal)) {
      logLevel = inputVal;
    }
  }
});

// need to set global logging
global.log = winston.createLogger({
  level: logLevel,
  levels: myCustomLevels.levels,
  transports: [
    new winston.transports.Console()
  ]
});

/**
 * Runs the error asserts for the test
 */
function runErrorAsserts(data, error, code, origin, displayStr) {
  assert.equal(null, data);
  assert.notEqual(undefined, error);
  assert.notEqual(null, error);
  assert.notEqual(undefined, error.IAPerror);
  assert.notEqual(null, error.IAPerror);
  assert.notEqual(undefined, error.IAPerror.displayString);
  assert.notEqual(null, error.IAPerror.displayString);
  assert.equal(code, error.icode);
  assert.equal(origin, error.IAPerror.origin);
  assert.equal(displayStr, error.IAPerror.displayString);
}

// require the adapter that we are going to be using
const Digicert = require('../../adapter');

// delete the .DS_Store directory in entities -- otherwise this will cause errors
const dirPath = path.join(__dirname, '../../entities/.DS_Store');
if (fs.existsSync(dirPath)) {
  try {
    fs.removeSync(dirPath);
    console.log('.DS_Store deleted');
  } catch (e) {
    console.log('Error when deleting .DS_Store:', e);
  }
}

// begin the testing - these should be pretty well defined between the describe and the it!
describe('[unit] Digicert Adapter Test', () => {
  describe('Digicert Class Tests', () => {
    const a = new Digicert(
      pronghornProps.adapterProps.adapters[0].id,
      pronghornProps.adapterProps.adapters[0].properties
    );

    if (isRapidFail) {
      const state = {};
      state.passed = true;

      mocha.afterEach(function x() {
        state.passed = state.passed
        && (this.currentTest.state === 'passed');
      });
      mocha.beforeEach(function x() {
        if (!state.passed) {
          return this.currentTest.skip();
        }
        return true;
      });
    }

    describe('#class instance created', () => {
      it('should be a class with properties', (done) => {
        try {
          assert.notEqual(null, a);
          assert.notEqual(undefined, a);
          const checkId = global.pronghornProps.adapterProps.adapters[0].id;
          assert.equal(checkId, a.id);
          assert.notEqual(null, a.allProps);
          const check = global.pronghornProps.adapterProps.adapters[0].properties.healthcheck.type;
          assert.equal(check, a.healthcheckType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('adapterBase.js', () => {
      it('should have an adapterBase.js', (done) => {
        try {
          fs.exists('adapterBase.js', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    let wffunctions = [];
    describe('#iapGetAdapterWorkflowFunctions', () => {
      it('should retrieve workflow functions', (done) => {
        try {
          wffunctions = a.iapGetAdapterWorkflowFunctions([]);

          try {
            assert.notEqual(0, wffunctions.length);
            done();
          } catch (err) {
            log.error(`Test Failure: ${err}`);
            done(err);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('package.json', () => {
      it('should have a package.json', (done) => {
        try {
          fs.exists('package.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json should be validated', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          // Define the JSON schema for package.json
          const packageJsonSchema = {
            type: 'object',
            properties: {
              name: { type: 'string' },
              version: { type: 'string' }
              // May need to add more properties as needed
            },
            required: ['name', 'version']
          };
          const validate = ajv.compile(packageJsonSchema);
          const isValid = validate(packageDotJson);

          if (isValid === false) {
            log.error('The package.json contains errors');
            assert.equal(true, isValid);
          } else {
            assert.equal(true, isValid);
          }

          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json standard fields should be customized', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(-1, packageDotJson.name.indexOf('digicert_pki'));
          assert.notEqual(undefined, packageDotJson.version);
          assert.notEqual(null, packageDotJson.version);
          assert.notEqual('', packageDotJson.version);
          assert.notEqual(undefined, packageDotJson.description);
          assert.notEqual(null, packageDotJson.description);
          assert.notEqual('', packageDotJson.description);
          assert.equal('adapter.js', packageDotJson.main);
          assert.notEqual(undefined, packageDotJson.wizardVersion);
          assert.notEqual(null, packageDotJson.wizardVersion);
          assert.notEqual('', packageDotJson.wizardVersion);
          assert.notEqual(undefined, packageDotJson.engineVersion);
          assert.notEqual(null, packageDotJson.engineVersion);
          assert.notEqual('', packageDotJson.engineVersion);
          assert.equal('http', packageDotJson.adapterType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper scripts should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.scripts);
          assert.notEqual(null, packageDotJson.scripts);
          assert.notEqual('', packageDotJson.scripts);
          assert.equal('node utils/setup.js', packageDotJson.scripts.preinstall);
          assert.equal('node --max_old_space_size=4096 ./node_modules/eslint/bin/eslint.js . --ext .json --ext .js', packageDotJson.scripts.lint);
          assert.equal('node --max_old_space_size=4096 ./node_modules/eslint/bin/eslint.js . --ext .json --ext .js --quiet', packageDotJson.scripts['lint:errors']);
          assert.equal('mocha test/unit/adapterBaseTestUnit.js --LOG=error', packageDotJson.scripts['test:baseunit']);
          assert.equal('mocha test/unit/adapterTestUnit.js --LOG=error', packageDotJson.scripts['test:unit']);
          assert.equal('mocha test/integration/adapterTestIntegration.js --LOG=error', packageDotJson.scripts['test:integration']);
          assert.equal('npm run test:baseunit && npm run test:unit && npm run test:integration', packageDotJson.scripts.test);
          assert.equal('npm publish --registry=https://registry.npmjs.org --access=public', packageDotJson.scripts.deploy);
          assert.equal('npm run deploy', packageDotJson.scripts.build);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper directories should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.repository);
          assert.notEqual(null, packageDotJson.repository);
          assert.notEqual('', packageDotJson.repository);
          assert.equal('git', packageDotJson.repository.type);
          assert.equal('git@gitlab.com:itentialopensource/adapters/', packageDotJson.repository.url.substring(0, 43));
          assert.equal('https://gitlab.com/itentialopensource/adapters/', packageDotJson.homepage.substring(0, 47));
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper dependencies should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.dependencies);
          assert.notEqual(null, packageDotJson.dependencies);
          assert.notEqual('', packageDotJson.dependencies);
          assert.equal('^8.17.1', packageDotJson.dependencies.ajv);
          assert.equal('^1.7.9', packageDotJson.dependencies.axios);
          assert.equal('^11.0.0', packageDotJson.dependencies.commander);
          assert.equal('^11.2.0', packageDotJson.dependencies['fs-extra']);
          assert.equal('^10.8.2', packageDotJson.dependencies.mocha);
          assert.equal('^2.0.1', packageDotJson.dependencies['mocha-param']);
          assert.equal('^0.4.4', packageDotJson.dependencies.ping);
          assert.equal('^1.4.10', packageDotJson.dependencies['readline-sync']);
          assert.equal('^7.6.3', packageDotJson.dependencies.semver);
          assert.equal('^3.17.0', packageDotJson.dependencies.winston);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper dev dependencies should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.devDependencies);
          assert.notEqual(null, packageDotJson.devDependencies);
          assert.notEqual('', packageDotJson.devDependencies);
          assert.equal('^4.3.7', packageDotJson.devDependencies.chai);
          assert.equal('^8.44.0', packageDotJson.devDependencies.eslint);
          assert.equal('^15.0.0', packageDotJson.devDependencies['eslint-config-airbnb-base']);
          assert.equal('^2.27.5', packageDotJson.devDependencies['eslint-plugin-import']);
          assert.equal('^3.1.0', packageDotJson.devDependencies['eslint-plugin-json']);
          assert.equal('^3.18.0', packageDotJson.devDependencies.testdouble);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('pronghorn.json', () => {
      it('should have a pronghorn.json', (done) => {
        try {
          fs.exists('pronghorn.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json should be customized', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');
          assert.notEqual(-1, pronghornDotJson.id.indexOf('digicert_pki'));
          assert.equal('Adapter', pronghornDotJson.type);
          assert.equal('Digicert', pronghornDotJson.export);
          assert.equal('Digicert', pronghornDotJson.title);
          assert.equal('adapter.js', pronghornDotJson.src);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json should contain generic adapter methods', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');
          assert.notEqual(undefined, pronghornDotJson.methods);
          assert.notEqual(null, pronghornDotJson.methods);
          assert.notEqual('', pronghornDotJson.methods);
          assert.equal(true, Array.isArray(pronghornDotJson.methods));
          assert.notEqual(0, pronghornDotJson.methods.length);
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapUpdateAdapterConfiguration'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapSuspendAdapter'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapUnsuspendAdapter'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapGetAdapterQueue'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapFindAdapterPath'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapTroubleshootAdapter'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterHealthcheck'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterConnectivity'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterBasicGet'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapMoveAdapterEntitiesToDB'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapDeactivateTasks'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapActivateTasks'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapPopulateEntityCache'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRetrieveEntitiesCache'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'getDevice'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'getDevicesFiltered'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'isAlive'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'getConfig'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapGetDeviceCount'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapExpandedGenericAdapterRequest'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'genericAdapterRequest'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'genericAdapterRequestNoBasePath'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterLint'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterTests'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapGetAdapterInventory'));
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json should only expose workflow functions', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');

          for (let m = 0; m < pronghornDotJson.methods.length; m += 1) {
            let found = false;
            let paramissue = false;

            for (let w = 0; w < wffunctions.length; w += 1) {
              if (pronghornDotJson.methods[m].name === wffunctions[w]) {
                found = true;
                const methLine = execute(`grep "  ${wffunctions[w]}(" adapter.js | grep "callback) {"`).toString();
                let wfparams = [];

                if (methLine && methLine.indexOf('(') >= 0 && methLine.indexOf(')') >= 0) {
                  const temp = methLine.substring(methLine.indexOf('(') + 1, methLine.lastIndexOf(')'));
                  wfparams = temp.split(',');

                  for (let t = 0; t < wfparams.length; t += 1) {
                    // remove default value from the parameter name
                    wfparams[t] = wfparams[t].substring(0, wfparams[t].search(/=/) > 0 ? wfparams[t].search(/#|\?|=/) : wfparams[t].length);
                    // remove spaces
                    wfparams[t] = wfparams[t].trim();

                    if (wfparams[t] === 'callback') {
                      wfparams.splice(t, 1);
                    }
                  }
                }

                // if there are inputs defined but not on the method line
                if (wfparams.length === 0 && (pronghornDotJson.methods[m].input
                    && pronghornDotJson.methods[m].input.length > 0)) {
                  paramissue = true;
                } else if (wfparams.length > 0 && (!pronghornDotJson.methods[m].input
                    || pronghornDotJson.methods[m].input.length === 0)) {
                  // if there are no inputs defined but there are on the method line
                  paramissue = true;
                } else {
                  for (let p = 0; p < pronghornDotJson.methods[m].input.length; p += 1) {
                    let pfound = false;
                    for (let wfp = 0; wfp < wfparams.length; wfp += 1) {
                      if (pronghornDotJson.methods[m].input[p].name.toUpperCase() === wfparams[wfp].toUpperCase()) {
                        pfound = true;
                      }
                    }

                    if (!pfound) {
                      paramissue = true;
                    }
                  }
                  for (let wfp = 0; wfp < wfparams.length; wfp += 1) {
                    let pfound = false;
                    for (let p = 0; p < pronghornDotJson.methods[m].input.length; p += 1) {
                      if (pronghornDotJson.methods[m].input[p].name.toUpperCase() === wfparams[wfp].toUpperCase()) {
                        pfound = true;
                      }
                    }

                    if (!pfound) {
                      paramissue = true;
                    }
                  }
                }

                break;
              }
            }

            if (!found) {
              // this is the reason to go through both loops - log which ones are not found so
              // they can be worked
              log.error(`${pronghornDotJson.methods[m].name} not found in workflow functions`);
            }
            if (paramissue) {
              // this is the reason to go through both loops - log which ones are not found so
              // they can be worked
              log.error(`${pronghornDotJson.methods[m].name} has a parameter mismatch`);
            }
            assert.equal(true, found);
            assert.equal(false, paramissue);
          }
          done();
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('pronghorn.json should expose all workflow functions', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');
          for (let w = 0; w < wffunctions.length; w += 1) {
            let found = false;

            for (let m = 0; m < pronghornDotJson.methods.length; m += 1) {
              if (pronghornDotJson.methods[m].name === wffunctions[w]) {
                found = true;
                break;
              }
            }

            if (!found) {
              // this is the reason to go through both loops - log which ones are not found so
              // they can be worked
              log.error(`${wffunctions[w]} not found in pronghorn.json`);
            }
            assert.equal(true, found);
          }
          done();
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json verify input/output schema objects', (done) => {
        const verifySchema = (methodName, schema) => {
          try {
            ajv.compile(schema);
          } catch (error) {
            const errorMessage = `Invalid schema found in '${methodName}' method.
          Schema => ${JSON.stringify(schema)}.
          Details => ${error.message}`;
            throw new Error(errorMessage);
          }
        };

        try {
          const pronghornDotJson = require('../../pronghorn.json');
          const { methods } = pronghornDotJson;
          for (let i = 0; i < methods.length; i += 1) {
            for (let j = 0; j < methods[i].input.length; j += 1) {
              const inputSchema = methods[i].input[j].schema;
              if (inputSchema) {
                verifySchema(methods[i].name, inputSchema);
              }
            }
            const outputSchema = methods[i].output.schema;
            if (outputSchema) {
              verifySchema(methods[i].name, outputSchema);
            }
          }
          done();
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('propertiesSchema.json', () => {
      it('should have a propertiesSchema.json', (done) => {
        try {
          fs.exists('propertiesSchema.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('propertiesSchema.json should be customized', (done) => {
        try {
          const propertiesDotJson = require('../../propertiesSchema.json');
          assert.equal('adapter-digicert_pki', propertiesDotJson.$id);
          assert.equal('object', propertiesDotJson.type);
          assert.equal('http://json-schema.org/draft-07/schema#', propertiesDotJson.$schema);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('propertiesSchema.json should contain generic adapter properties', (done) => {
        try {
          const propertiesDotJson = require('../../propertiesSchema.json');
          assert.notEqual(undefined, propertiesDotJson.properties);
          assert.notEqual(null, propertiesDotJson.properties);
          assert.notEqual('', propertiesDotJson.properties);
          assert.equal('string', propertiesDotJson.properties.host.type);
          assert.equal('integer', propertiesDotJson.properties.port.type);
          assert.equal('boolean', propertiesDotJson.properties.stub.type);
          assert.equal('string', propertiesDotJson.properties.protocol.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.authentication);
          assert.notEqual(null, propertiesDotJson.definitions.authentication);
          assert.notEqual('', propertiesDotJson.definitions.authentication);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.auth_method.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.username.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.password.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.token.type);
          assert.equal('integer', propertiesDotJson.definitions.authentication.properties.invalid_token_error.type);
          assert.equal('integer', propertiesDotJson.definitions.authentication.properties.token_timeout.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.token_cache.type);
          assert.equal(true, Array.isArray(propertiesDotJson.definitions.authentication.properties.auth_field.type));
          assert.equal(true, Array.isArray(propertiesDotJson.definitions.authentication.properties.auth_field_format.type));
          assert.equal('boolean', propertiesDotJson.definitions.authentication.properties.auth_logging.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.client_id.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.client_secret.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.grant_type.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.ssl);
          assert.notEqual(null, propertiesDotJson.definitions.ssl);
          assert.notEqual('', propertiesDotJson.definitions.ssl);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.ecdhCurve.type);
          assert.equal('boolean', propertiesDotJson.definitions.ssl.properties.enabled.type);
          assert.equal('boolean', propertiesDotJson.definitions.ssl.properties.accept_invalid_cert.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.ca_file.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.key_file.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.cert_file.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.secure_protocol.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.ciphers.type);
          assert.equal('string', propertiesDotJson.properties.base_path.type);
          assert.equal('string', propertiesDotJson.properties.version.type);
          assert.equal('string', propertiesDotJson.properties.cache_location.type);
          assert.equal('boolean', propertiesDotJson.properties.encode_pathvars.type);
          assert.equal('boolean', propertiesDotJson.properties.encode_queryvars.type);
          assert.equal(true, Array.isArray(propertiesDotJson.properties.save_metric.type));
          assert.notEqual(undefined, propertiesDotJson.definitions);
          assert.notEqual(null, propertiesDotJson.definitions);
          assert.notEqual('', propertiesDotJson.definitions);
          assert.notEqual(undefined, propertiesDotJson.definitions.healthcheck);
          assert.notEqual(null, propertiesDotJson.definitions.healthcheck);
          assert.notEqual('', propertiesDotJson.definitions.healthcheck);
          assert.equal('string', propertiesDotJson.definitions.healthcheck.properties.type.type);
          assert.equal('integer', propertiesDotJson.definitions.healthcheck.properties.frequency.type);
          assert.equal('object', propertiesDotJson.definitions.healthcheck.properties.query_object.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.throttle);
          assert.notEqual(null, propertiesDotJson.definitions.throttle);
          assert.notEqual('', propertiesDotJson.definitions.throttle);
          assert.equal('boolean', propertiesDotJson.definitions.throttle.properties.throttle_enabled.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.number_pronghorns.type);
          assert.equal('string', propertiesDotJson.definitions.throttle.properties.sync_async.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.max_in_queue.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.concurrent_max.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.expire_timeout.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.avg_runtime.type);
          assert.equal('array', propertiesDotJson.definitions.throttle.properties.priorities.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.request);
          assert.notEqual(null, propertiesDotJson.definitions.request);
          assert.notEqual('', propertiesDotJson.definitions.request);
          assert.equal('integer', propertiesDotJson.definitions.request.properties.number_redirects.type);
          assert.equal('integer', propertiesDotJson.definitions.request.properties.number_retries.type);
          assert.equal(true, Array.isArray(propertiesDotJson.definitions.request.properties.limit_retry_error.type));
          assert.equal('array', propertiesDotJson.definitions.request.properties.failover_codes.type);
          assert.equal('integer', propertiesDotJson.definitions.request.properties.attempt_timeout.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.payload.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.uriOptions.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.addlHeaders.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.authData.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.healthcheck_on_timeout.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.return_raw.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.archiving.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.return_request.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.proxy);
          assert.notEqual(null, propertiesDotJson.definitions.proxy);
          assert.notEqual('', propertiesDotJson.definitions.proxy);
          assert.equal('boolean', propertiesDotJson.definitions.proxy.properties.enabled.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.host.type);
          assert.equal('integer', propertiesDotJson.definitions.proxy.properties.port.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.protocol.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.username.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.password.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.mongo);
          assert.notEqual(null, propertiesDotJson.definitions.mongo);
          assert.notEqual('', propertiesDotJson.definitions.mongo);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.host.type);
          assert.equal('integer', propertiesDotJson.definitions.mongo.properties.port.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.database.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.username.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.password.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.replSet.type);
          assert.equal('object', propertiesDotJson.definitions.mongo.properties.db_ssl.type);
          assert.equal('boolean', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.enabled.type);
          assert.equal('boolean', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.accept_invalid_cert.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.ca_file.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.key_file.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.cert_file.type);
          assert.notEqual('', propertiesDotJson.definitions.devicebroker);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getDevice.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getDevicesFiltered.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.isAlive.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getConfig.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getCount.type);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('error.json', () => {
      it('should have an error.json', (done) => {
        try {
          fs.exists('error.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('error.json should have standard adapter errors', (done) => {
        try {
          const errorDotJson = require('../../error.json');
          assert.notEqual(undefined, errorDotJson.errors);
          assert.notEqual(null, errorDotJson.errors);
          assert.notEqual('', errorDotJson.errors);
          assert.equal(true, Array.isArray(errorDotJson.errors));
          assert.notEqual(0, errorDotJson.errors.length);
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.100'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.101'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.102'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.110'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.111'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.112'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.113'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.114'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.115'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.116'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.300'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.301'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.302'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.303'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.304'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.305'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.310'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.311'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.312'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.320'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.321'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.400'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.401'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.402'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.500'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.501'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.502'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.503'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.600'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.900'));
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('sampleProperties.json', () => {
      it('should have a sampleProperties.json', (done) => {
        try {
          fs.exists('sampleProperties.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('sampleProperties.json should contain generic adapter properties', (done) => {
        try {
          const sampleDotJson = require('../../sampleProperties.json');
          assert.notEqual(-1, sampleDotJson.id.indexOf('digicert_pki'));
          assert.equal('Digicert', sampleDotJson.type);
          assert.notEqual(undefined, sampleDotJson.properties);
          assert.notEqual(null, sampleDotJson.properties);
          assert.notEqual('', sampleDotJson.properties);
          assert.notEqual(undefined, sampleDotJson.properties.host);
          assert.notEqual(undefined, sampleDotJson.properties.port);
          assert.notEqual(undefined, sampleDotJson.properties.stub);
          assert.notEqual(undefined, sampleDotJson.properties.protocol);
          assert.notEqual(undefined, sampleDotJson.properties.authentication);
          assert.notEqual(null, sampleDotJson.properties.authentication);
          assert.notEqual('', sampleDotJson.properties.authentication);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_method);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.username);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.password);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.token);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.invalid_token_error);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.token_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.token_cache);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_field);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_field_format);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_logging);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.client_id);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.client_secret);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.grant_type);
          assert.notEqual(undefined, sampleDotJson.properties.ssl);
          assert.notEqual(null, sampleDotJson.properties.ssl);
          assert.notEqual('', sampleDotJson.properties.ssl);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.ecdhCurve);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.enabled);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.accept_invalid_cert);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.ca_file);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.key_file);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.cert_file);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.secure_protocol);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.ciphers);
          assert.notEqual(undefined, sampleDotJson.properties.base_path);
          assert.notEqual(undefined, sampleDotJson.properties.version);
          assert.notEqual(undefined, sampleDotJson.properties.cache_location);
          assert.notEqual(undefined, sampleDotJson.properties.encode_pathvars);
          assert.notEqual(undefined, sampleDotJson.properties.encode_queryvars);
          assert.notEqual(undefined, sampleDotJson.properties.save_metric);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck);
          assert.notEqual(null, sampleDotJson.properties.healthcheck);
          assert.notEqual('', sampleDotJson.properties.healthcheck);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck.type);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck.frequency);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck.query_object);
          assert.notEqual(undefined, sampleDotJson.properties.throttle);
          assert.notEqual(null, sampleDotJson.properties.throttle);
          assert.notEqual('', sampleDotJson.properties.throttle);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.throttle_enabled);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.number_pronghorns);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.sync_async);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.max_in_queue);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.concurrent_max);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.expire_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.avg_runtime);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.priorities);
          assert.notEqual(undefined, sampleDotJson.properties.request);
          assert.notEqual(null, sampleDotJson.properties.request);
          assert.notEqual('', sampleDotJson.properties.request);
          assert.notEqual(undefined, sampleDotJson.properties.request.number_redirects);
          assert.notEqual(undefined, sampleDotJson.properties.request.number_retries);
          assert.notEqual(undefined, sampleDotJson.properties.request.limit_retry_error);
          assert.notEqual(undefined, sampleDotJson.properties.request.failover_codes);
          assert.notEqual(undefined, sampleDotJson.properties.request.attempt_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.payload);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.uriOptions);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.addlHeaders);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.authData);
          assert.notEqual(undefined, sampleDotJson.properties.request.healthcheck_on_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.request.return_raw);
          assert.notEqual(undefined, sampleDotJson.properties.request.archiving);
          assert.notEqual(undefined, sampleDotJson.properties.request.return_request);
          assert.notEqual(undefined, sampleDotJson.properties.proxy);
          assert.notEqual(null, sampleDotJson.properties.proxy);
          assert.notEqual('', sampleDotJson.properties.proxy);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.enabled);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.host);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.port);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.protocol);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.username);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.password);
          assert.notEqual(undefined, sampleDotJson.properties.mongo);
          assert.notEqual(null, sampleDotJson.properties.mongo);
          assert.notEqual('', sampleDotJson.properties.mongo);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.host);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.port);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.database);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.username);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.password);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.replSet);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.enabled);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.accept_invalid_cert);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.ca_file);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.key_file);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.cert_file);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getDevice);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getDevicesFiltered);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.isAlive);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getConfig);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getCount);
          assert.notEqual(undefined, sampleDotJson.properties.cache);
          assert.notEqual(undefined, sampleDotJson.properties.cache.entities);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#checkProperties', () => {
      it('should have a checkProperties function', (done) => {
        try {
          assert.equal(true, typeof a.checkProperties === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('the sample properties should be good - if failure change the log level', (done) => {
        try {
          const samplePropsJson = require('../../sampleProperties.json');
          const clean = a.checkProperties(samplePropsJson.properties);

          try {
            assert.notEqual(0, Object.keys(clean));
            assert.equal(undefined, clean.exception);
            assert.notEqual(undefined, clean.host);
            assert.notEqual(null, clean.host);
            assert.notEqual('', clean.host);
            done();
          } catch (err) {
            log.error(`Test Failure: ${err}`);
            done(err);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('README.md', () => {
      it('should have a README', (done) => {
        try {
          fs.exists('README.md', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('README.md should be customized', (done) => {
        try {
          fs.readFile('README.md', 'utf8', (err, data) => {
            assert.equal(-1, data.indexOf('[System]'));
            assert.equal(-1, data.indexOf('[system]'));
            assert.equal(-1, data.indexOf('[version]'));
            assert.equal(-1, data.indexOf('[namespace]'));
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#connect', () => {
      it('should have a connect function', (done) => {
        try {
          assert.equal(true, typeof a.connect === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#healthCheck', () => {
      it('should have a healthCheck function', (done) => {
        try {
          assert.equal(true, typeof a.healthCheck === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapUpdateAdapterConfiguration', () => {
      it('should have a iapUpdateAdapterConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.iapUpdateAdapterConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapSuspendAdapter', () => {
      it('should have a iapSuspendAdapter function', (done) => {
        try {
          assert.equal(true, typeof a.iapSuspendAdapter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapUnsuspendAdapter', () => {
      it('should have a iapUnsuspendAdapter function', (done) => {
        try {
          assert.equal(true, typeof a.iapUnsuspendAdapter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapGetAdapterQueue', () => {
      it('should have a iapGetAdapterQueue function', (done) => {
        try {
          assert.equal(true, typeof a.iapGetAdapterQueue === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapFindAdapterPath', () => {
      it('should have a iapFindAdapterPath function', (done) => {
        try {
          assert.equal(true, typeof a.iapFindAdapterPath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('iapFindAdapterPath should find atleast one path that matches', (done) => {
        try {
          a.iapFindAdapterPath('{base_path}/{version}', (data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.equal(true, data.found);
              assert.notEqual(undefined, data.foundIn);
              assert.notEqual(null, data.foundIn);
              assert.notEqual(0, data.foundIn.length);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapTroubleshootAdapter', () => {
      it('should have a iapTroubleshootAdapter function', (done) => {
        try {
          assert.equal(true, typeof a.iapTroubleshootAdapter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterHealthcheck', () => {
      it('should have a iapRunAdapterHealthcheck function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterHealthcheck === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterConnectivity', () => {
      it('should have a iapRunAdapterConnectivity function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterConnectivity === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterBasicGet', () => {
      it('should have a iapRunAdapterBasicGet function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterBasicGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapMoveAdapterEntitiesToDB', () => {
      it('should have a iapMoveAdapterEntitiesToDB function', (done) => {
        try {
          assert.equal(true, typeof a.iapMoveAdapterEntitiesToDB === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#checkActionFiles', () => {
      it('should have a checkActionFiles function', (done) => {
        try {
          assert.equal(true, typeof a.checkActionFiles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('the action files should be good - if failure change the log level as most issues are warnings', (done) => {
        try {
          const clean = a.checkActionFiles();

          try {
            for (let c = 0; c < clean.length; c += 1) {
              log.error(clean[c]);
            }
            assert.equal(0, clean.length);
            done();
          } catch (err) {
            log.error(`Test Failure: ${err}`);
            done(err);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#encryptProperty', () => {
      it('should have a encryptProperty function', (done) => {
        try {
          assert.equal(true, typeof a.encryptProperty === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('should get base64 encoded property', (done) => {
        try {
          a.encryptProperty('testing', 'base64', (data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.notEqual(undefined, data.response);
              assert.notEqual(null, data.response);
              assert.equal(0, data.response.indexOf('{code}'));
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should get encrypted property', (done) => {
        try {
          a.encryptProperty('testing', 'encrypt', (data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.notEqual(undefined, data.response);
              assert.notEqual(null, data.response);
              assert.equal(0, data.response.indexOf('{crypt}'));
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapDeactivateTasks', () => {
      it('should have a iapDeactivateTasks function', (done) => {
        try {
          assert.equal(true, typeof a.iapDeactivateTasks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapActivateTasks', () => {
      it('should have a iapActivateTasks function', (done) => {
        try {
          assert.equal(true, typeof a.iapActivateTasks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapPopulateEntityCache', () => {
      it('should have a iapPopulateEntityCache function', (done) => {
        try {
          assert.equal(true, typeof a.iapPopulateEntityCache === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRetrieveEntitiesCache', () => {
      it('should have a iapRetrieveEntitiesCache function', (done) => {
        try {
          assert.equal(true, typeof a.iapRetrieveEntitiesCache === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#hasEntities', () => {
      it('should have a hasEntities function', (done) => {
        try {
          assert.equal(true, typeof a.hasEntities === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getDevice', () => {
      it('should have a getDevice function', (done) => {
        try {
          assert.equal(true, typeof a.getDevice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getDevicesFiltered', () => {
      it('should have a getDevicesFiltered function', (done) => {
        try {
          assert.equal(true, typeof a.getDevicesFiltered === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#isAlive', () => {
      it('should have a isAlive function', (done) => {
        try {
          assert.equal(true, typeof a.isAlive === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getConfig', () => {
      it('should have a getConfig function', (done) => {
        try {
          assert.equal(true, typeof a.getConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapGetDeviceCount', () => {
      it('should have a iapGetDeviceCount function', (done) => {
        try {
          assert.equal(true, typeof a.iapGetDeviceCount === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapExpandedGenericAdapterRequest', () => {
      it('should have a iapExpandedGenericAdapterRequest function', (done) => {
        try {
          assert.equal(true, typeof a.iapExpandedGenericAdapterRequest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#genericAdapterRequest', () => {
      it('should have a genericAdapterRequest function', (done) => {
        try {
          assert.equal(true, typeof a.genericAdapterRequest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#genericAdapterRequestNoBasePath', () => {
      it('should have a genericAdapterRequestNoBasePath function', (done) => {
        try {
          assert.equal(true, typeof a.genericAdapterRequestNoBasePath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterLint', () => {
      it('should have a iapRunAdapterLint function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterLint === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('retrieve the lint results', (done) => {
        try {
          a.iapRunAdapterLint((data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.notEqual(undefined, data.status);
              assert.notEqual(null, data.status);
              assert.equal('SUCCESS', data.status);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapRunAdapterTests', () => {
      it('should have a iapRunAdapterTests function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterTests === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapGetAdapterInventory', () => {
      it('should have a iapGetAdapterInventory function', (done) => {
        try {
          assert.equal(true, typeof a.iapGetAdapterInventory === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('retrieve the inventory', (done) => {
        try {
          a.iapGetAdapterInventory((data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
    describe('metadata.json', () => {
      it('should have a metadata.json', (done) => {
        try {
          fs.exists('metadata.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('metadata.json is customized', (done) => {
        try {
          const metadataDotJson = require('../../metadata.json');
          assert.equal('adapter-digicert_pki', metadataDotJson.name);
          assert.notEqual(undefined, metadataDotJson.webName);
          assert.notEqual(null, metadataDotJson.webName);
          assert.notEqual('', metadataDotJson.webName);
          assert.equal('Adapter', metadataDotJson.type);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('metadata.json contains accurate documentation', (done) => {
        try {
          const metadataDotJson = require('../../metadata.json');
          assert.notEqual(undefined, metadataDotJson.documentation);
          assert.equal('https://www.npmjs.com/package/@itentialopensource/adapter-digicert_pki', metadataDotJson.documentation.npmLink);
          assert.equal('https://docs.itential.com/opensource/docs/troubleshooting-an-adapter', metadataDotJson.documentation.faqLink);
          assert.equal('https://gitlab.com/itentialopensource/adapters/contributing-guide', metadataDotJson.documentation.contributeLink);
          assert.equal('https://itential.atlassian.net/servicedesk/customer/portals', metadataDotJson.documentation.issueLink);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('metadata.json has related items', (done) => {
        try {
          const metadataDotJson = require('../../metadata.json');
          assert.notEqual(undefined, metadataDotJson.relatedItems);
          assert.notEqual(undefined, metadataDotJson.relatedItems.adapters);
          assert.notEqual(undefined, metadataDotJson.relatedItems.integrations);
          assert.notEqual(undefined, metadataDotJson.relatedItems.ecosystemApplications);
          assert.notEqual(undefined, metadataDotJson.relatedItems.workflowProjects);
          assert.notEqual(undefined, metadataDotJson.relatedItems.transformationProjects);
          assert.notEqual(undefined, metadataDotJson.relatedItems.exampleProjects);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });
    /*
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    *** All code above this comment will be replaced during a migration ***
    ******************* DO NOT REMOVE THIS COMMENT BLOCK ******************
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    */

    describe('#getAuditsUsingGET - errors', () => {
      it('should have a getAuditsUsingGET function', (done) => {
        try {
          assert.equal(true, typeof a.getAuditsUsingGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing dateFrom', (done) => {
        try {
          a.getAuditsUsingGET('fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'dateFrom is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-getAuditsUsingGET', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing dateTo', (done) => {
        try {
          a.getAuditsUsingGET('fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'dateTo is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-getAuditsUsingGET', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAuditUsingGET - errors', () => {
      it('should have a getAuditUsingGET function', (done) => {
        try {
          assert.equal(true, typeof a.getAuditUsingGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing auditId', (done) => {
        try {
          a.getAuditUsingGET(null, (data, error) => {
            try {
              const displayE = 'auditId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-getAuditUsingGET', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCAInfoUsingGET - errors', () => {
      it('should have a getCAInfoUsingGET function', (done) => {
        try {
          assert.equal(true, typeof a.getCAInfoUsingGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing caId', (done) => {
        try {
          a.getCAInfoUsingGET(null, (data, error) => {
            try {
              const displayE = 'caId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-getCAInfoUsingGET', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCAUsingGET - errors', () => {
      it('should have a getCAUsingGET function', (done) => {
        try {
          assert.equal(true, typeof a.getCAUsingGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing seatType', (done) => {
        try {
          a.getCAUsingGET(null, (data, error) => {
            try {
              const displayE = 'seatType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-getCAUsingGET', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#enrollCertificateUsingPOST - errors', () => {
      it('should have a enrollCertificateUsingPOST function', (done) => {
        try {
          assert.equal(true, typeof a.enrollCertificateUsingPOST === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing enrollCertificateRequest', (done) => {
        try {
          a.enrollCertificateUsingPOST(null, (data, error) => {
            try {
              const displayE = 'enrollCertificateRequest is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-enrollCertificateUsingPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createCertificateUsingPOST - errors', () => {
      it('should have a createCertificateUsingPOST function', (done) => {
        try {
          assert.equal(true, typeof a.createCertificateUsingPOST === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing enrollCertificateRequest', (done) => {
        try {
          a.createCertificateUsingPOST(null, (data, error) => {
            try {
              const displayE = 'enrollCertificateRequest is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-createCertificateUsingPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#renewExternalCertificateUsingPOST - errors', () => {
      it('should have a renewExternalCertificateUsingPOST function', (done) => {
        try {
          assert.equal(true, typeof a.renewExternalCertificateUsingPOST === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing enrollCertificateRequest', (done) => {
        try {
          a.renewExternalCertificateUsingPOST(null, null, (data, error) => {
            try {
              const displayE = 'enrollCertificateRequest is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-renewExternalCertificateUsingPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serialNumber', (done) => {
        try {
          a.renewExternalCertificateUsingPOST('fakeparam', null, (data, error) => {
            try {
              const displayE = 'serialNumber is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-renewExternalCertificateUsingPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCertificateUsingGET - errors', () => {
      it('should have a getCertificateUsingGET function', (done) => {
        try {
          assert.equal(true, typeof a.getCertificateUsingGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serialNumber', (done) => {
        try {
          a.getCertificateUsingGET(null, (data, error) => {
            try {
              const displayE = 'serialNumber is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-getCertificateUsingGET', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#recoverKeyUsingGET - errors', () => {
      it('should have a recoverKeyUsingGET function', (done) => {
        try {
          assert.equal(true, typeof a.recoverKeyUsingGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serialNumber', (done) => {
        try {
          a.recoverKeyUsingGET(null, (data, error) => {
            try {
              const displayE = 'serialNumber is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-recoverKeyUsingGET', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#renewCertificateUsingPOST - errors', () => {
      it('should have a renewCertificateUsingPOST function', (done) => {
        try {
          assert.equal(true, typeof a.renewCertificateUsingPOST === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing enrollCertificateRequest', (done) => {
        try {
          a.renewCertificateUsingPOST(null, null, (data, error) => {
            try {
              const displayE = 'enrollCertificateRequest is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-renewCertificateUsingPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serialNumber', (done) => {
        try {
          a.renewCertificateUsingPOST('fakeparam', null, (data, error) => {
            try {
              const displayE = 'serialNumber is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-renewCertificateUsingPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#revokeCertificateUsingPUT - errors', () => {
      it('should have a revokeCertificateUsingPUT function', (done) => {
        try {
          assert.equal(true, typeof a.revokeCertificateUsingPUT === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serialNumber', (done) => {
        try {
          a.revokeCertificateUsingPUT('fakeparam', null, (data, error) => {
            try {
              const displayE = 'serialNumber is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-revokeCertificateUsingPUT', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#unRevokeCertificateUsingDELETE - errors', () => {
      it('should have a unRevokeCertificateUsingDELETE function', (done) => {
        try {
          assert.equal(true, typeof a.unRevokeCertificateUsingDELETE === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serialNumber', (done) => {
        try {
          a.unRevokeCertificateUsingDELETE(null, (data, error) => {
            try {
              const displayE = 'serialNumber is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-unRevokeCertificateUsingDELETE', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createPasscodeUsingPOST - errors', () => {
      it('should have a createPasscodeUsingPOST function', (done) => {
        try {
          assert.equal(true, typeof a.createPasscodeUsingPOST === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing createPasscodeRequest', (done) => {
        try {
          a.createPasscodeUsingPOST(null, (data, error) => {
            try {
              const displayE = 'createPasscodeRequest is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-createPasscodeUsingPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getEnrollmentUsingGET - errors', () => {
      it('should have a getEnrollmentUsingGET function', (done) => {
        try {
          assert.equal(true, typeof a.getEnrollmentUsingGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing enrollCode', (done) => {
        try {
          a.getEnrollmentUsingGET(null, null, (data, error) => {
            try {
              const displayE = 'enrollCode is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-getEnrollmentUsingGET', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing seatId', (done) => {
        try {
          a.getEnrollmentUsingGET('fakeparam', null, (data, error) => {
            try {
              const displayE = 'seatId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-getEnrollmentUsingGET', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#resetPasscodeUsingPUT - errors', () => {
      it('should have a resetPasscodeUsingPUT function', (done) => {
        try {
          assert.equal(true, typeof a.resetPasscodeUsingPUT === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing enrollCode', (done) => {
        try {
          a.resetPasscodeUsingPUT(null, null, (data, error) => {
            try {
              const displayE = 'enrollCode is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-resetPasscodeUsingPUT', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resetPasscodeRequest', (done) => {
        try {
          a.resetPasscodeUsingPUT('fakeparam', null, (data, error) => {
            try {
              const displayE = 'resetPasscodeRequest is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-resetPasscodeUsingPUT', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteEnrollmentUsingDELETE - errors', () => {
      it('should have a deleteEnrollmentUsingDELETE function', (done) => {
        try {
          assert.equal(true, typeof a.deleteEnrollmentUsingDELETE === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deleteEnrollRequest', (done) => {
        try {
          a.deleteEnrollmentUsingDELETE(null, null, (data, error) => {
            try {
              const displayE = 'deleteEnrollRequest is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-deleteEnrollmentUsingDELETE', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing enrollCode', (done) => {
        try {
          a.deleteEnrollmentUsingDELETE('fakeparam', null, (data, error) => {
            try {
              const displayE = 'enrollCode is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-deleteEnrollmentUsingDELETE', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#enrollStatusUsingGET - errors', () => {
      it('should have a enrollStatusUsingGET function', (done) => {
        try {
          assert.equal(true, typeof a.enrollStatusUsingGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing seatId', (done) => {
        try {
          a.enrollStatusUsingGET('fakeparam', null, (data, error) => {
            try {
              const displayE = 'seatId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-enrollStatusUsingGET', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#helloUsingGET - errors', () => {
      it('should have a helloUsingGET function', (done) => {
        try {
          assert.equal(true, typeof a.helloUsingGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllProfilesUsingGET - errors', () => {
      it('should have a getAllProfilesUsingGET function', (done) => {
        try {
          assert.equal(true, typeof a.getAllProfilesUsingGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getProfileUsingGET - errors', () => {
      it('should have a getProfileUsingGET function', (done) => {
        try {
          assert.equal(true, typeof a.getProfileUsingGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing profileId', (done) => {
        try {
          a.getProfileUsingGET(null, (data, error) => {
            try {
              const displayE = 'profileId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-getProfileUsingGET', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#activateProfileUsingPUT - errors', () => {
      it('should have a activateProfileUsingPUT function', (done) => {
        try {
          assert.equal(true, typeof a.activateProfileUsingPUT === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing profileCreationRequest', (done) => {
        try {
          a.activateProfileUsingPUT(null, (data, error) => {
            try {
              const displayE = 'profileCreationRequest is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-activateProfileUsingPUT', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createProfileUsingPOST - errors', () => {
      it('should have a createProfileUsingPOST function', (done) => {
        try {
          assert.equal(true, typeof a.createProfileUsingPOST === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing profileCreationRequest', (done) => {
        try {
          a.createProfileUsingPOST(null, (data, error) => {
            try {
              const displayE = 'profileCreationRequest is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-createProfileUsingPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteProfileUsingPUT - errors', () => {
      it('should have a deleteProfileUsingPUT function', (done) => {
        try {
          assert.equal(true, typeof a.deleteProfileUsingPUT === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing profileCreationRequest', (done) => {
        try {
          a.deleteProfileUsingPUT(null, (data, error) => {
            try {
              const displayE = 'profileCreationRequest is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-deleteProfileUsingPUT', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateProfileUsingPUT - errors', () => {
      it('should have a updateProfileUsingPUT function', (done) => {
        try {
          assert.equal(true, typeof a.updateProfileUsingPUT === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing profileCreationRequest', (done) => {
        try {
          a.updateProfileUsingPUT(null, (data, error) => {
            try {
              const displayE = 'profileCreationRequest is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-updateProfileUsingPUT', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#suspendProfileUsingPUT - errors', () => {
      it('should have a suspendProfileUsingPUT function', (done) => {
        try {
          assert.equal(true, typeof a.suspendProfileUsingPUT === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing profileCreationRequest', (done) => {
        try {
          a.suspendProfileUsingPUT(null, (data, error) => {
            try {
              const displayE = 'profileCreationRequest is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-suspendProfileUsingPUT', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#testCreateUsingPOST - errors', () => {
      it('should have a testCreateUsingPOST function', (done) => {
        try {
          assert.equal(true, typeof a.testCreateUsingPOST === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#searchCertUsingPOST - errors', () => {
      it('should have a searchCertUsingPOST function', (done) => {
        try {
          assert.equal(true, typeof a.searchCertUsingPOST === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing searchReq', (done) => {
        try {
          a.searchCertUsingPOST(null, (data, error) => {
            try {
              const displayE = 'searchReq is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-searchCertUsingPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createSeatUsingPOST - errors', () => {
      it('should have a createSeatUsingPOST function', (done) => {
        try {
          assert.equal(true, typeof a.createSeatUsingPOST === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing createSeatRequest', (done) => {
        try {
          a.createSeatUsingPOST(null, (data, error) => {
            try {
              const displayE = 'createSeatRequest is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-createSeatUsingPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSeatUsingGET - errors', () => {
      it('should have a getSeatUsingGET function', (done) => {
        try {
          assert.equal(true, typeof a.getSeatUsingGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing seatId', (done) => {
        try {
          a.getSeatUsingGET(null, (data, error) => {
            try {
              const displayE = 'seatId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-getSeatUsingGET', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateSeatUsingPUT - errors', () => {
      it('should have a updateSeatUsingPUT function', (done) => {
        try {
          assert.equal(true, typeof a.updateSeatUsingPUT === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing seatId', (done) => {
        try {
          a.updateSeatUsingPUT(null, null, (data, error) => {
            try {
              const displayE = 'seatId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-updateSeatUsingPUT', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing updateSeatRequest', (done) => {
        try {
          a.updateSeatUsingPUT('fakeparam', null, (data, error) => {
            try {
              const displayE = 'updateSeatRequest is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-updateSeatUsingPUT', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSeatUsingDELETE - errors', () => {
      it('should have a deleteSeatUsingDELETE function', (done) => {
        try {
          assert.equal(true, typeof a.deleteSeatUsingDELETE === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing seatId', (done) => {
        try {
          a.deleteSeatUsingDELETE(null, (data, error) => {
            try {
              const displayE = 'seatId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-deleteSeatUsingDELETE', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listCertificates - errors', () => {
      it('should have a listCertificates function', (done) => {
        try {
          assert.equal(true, typeof a.listCertificates === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getExampleCertificateRequestCertificateID - errors', () => {
      it('should have a getExampleCertificateRequestCertificateID function', (done) => {
        try {
          assert.equal(true, typeof a.getExampleCertificateRequestCertificateID === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing certificateId', (done) => {
        try {
          a.getExampleCertificateRequestCertificateID(null, (data, error) => {
            try {
              const displayE = 'certificateId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-getExampleCertificateRequestCertificateID', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#renewCertificate - errors', () => {
      it('should have a renewCertificate function', (done) => {
        try {
          assert.equal(true, typeof a.renewCertificate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing certificateId', (done) => {
        try {
          a.renewCertificate(null, null, (data, error) => {
            try {
              const displayE = 'certificateId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-renewCertificate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#renewBySerial - errors', () => {
      it('should have a renewBySerial function', (done) => {
        try {
          assert.equal(true, typeof a.renewBySerial === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing certificateSerialNumber', (done) => {
        try {
          a.renewBySerial(null, null, (data, error) => {
            try {
              const displayE = 'certificateSerialNumber is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-renewBySerial', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#revokeCertificate - errors', () => {
      it('should have a revokeCertificate function', (done) => {
        try {
          assert.equal(true, typeof a.revokeCertificate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing certificateId', (done) => {
        try {
          a.revokeCertificate(null, null, (data, error) => {
            try {
              const displayE = 'certificateId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-revokeCertificate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateNote - errors', () => {
      it('should have a updateNote function', (done) => {
        try {
          assert.equal(true, typeof a.updateNote === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing certificateId', (done) => {
        try {
          a.updateNote(null, null, (data, error) => {
            try {
              const displayE = 'certificateId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-updateNote', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#revokeCertificateBySerialNumber - errors', () => {
      it('should have a revokeCertificateBySerialNumber function', (done) => {
        try {
          assert.equal(true, typeof a.revokeCertificateBySerialNumber === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing certificateSerialNumber', (done) => {
        try {
          a.revokeCertificateBySerialNumber(null, null, (data, error) => {
            try {
              const displayE = 'certificateSerialNumber is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-revokeCertificateBySerialNumber', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#downloadCertificate - errors', () => {
      it('should have a downloadCertificate function', (done) => {
        try {
          assert.equal(true, typeof a.downloadCertificate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing certificateId', (done) => {
        try {
          a.downloadCertificate(null, null, (data, error) => {
            try {
              const displayE = 'certificateId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-downloadCertificate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing format', (done) => {
        try {
          a.downloadCertificate('fakeparam', null, (data, error) => {
            try {
              const displayE = 'format is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-downloadCertificate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#downloadCertificateBySerialNumber - errors', () => {
      it('should have a downloadCertificateBySerialNumber function', (done) => {
        try {
          assert.equal(true, typeof a.downloadCertificateBySerialNumber === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing certificateSerialNumber', (done) => {
        try {
          a.downloadCertificateBySerialNumber(null, null, (data, error) => {
            try {
              const displayE = 'certificateSerialNumber is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-downloadCertificateBySerialNumber', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing format', (done) => {
        try {
          a.downloadCertificateBySerialNumber('fakeparam', null, (data, error) => {
            try {
              const displayE = 'format is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-downloadCertificateBySerialNumber', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#importCertificate - errors', () => {
      it('should have a importCertificate function', (done) => {
        try {
          assert.equal(true, typeof a.importCertificate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing enrollmentProfileId', (done) => {
        try {
          a.importCertificate(null, null, (data, error) => {
            try {
              const displayE = 'enrollmentProfileId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-importCertificate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getImportDetails - errors', () => {
      it('should have a getImportDetails function', (done) => {
        try {
          assert.equal(true, typeof a.getImportDetails === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing jobId', (done) => {
        try {
          a.getImportDetails(null, (data, error) => {
            try {
              const displayE = 'jobId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-getImportDetails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCertificateStatus - errors', () => {
      it('should have a getCertificateStatus function', (done) => {
        try {
          assert.equal(true, typeof a.getCertificateStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing certificateSerialNumber', (done) => {
        try {
          a.getCertificateStatus(null, (data, error) => {
            try {
              const displayE = 'certificateSerialNumber is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-getCertificateStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCertificateStatusById - errors', () => {
      it('should have a getCertificateStatusById function', (done) => {
        try {
          assert.equal(true, typeof a.getCertificateStatusById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing certificateId', (done) => {
        try {
          a.getCertificateStatusById(null, (data, error) => {
            try {
              const displayE = 'certificateId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-getCertificateStatusById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listCertificatesByCertificateValue - errors', () => {
      it('should have a listCertificatesByCertificateValue function', (done) => {
        try {
          assert.equal(true, typeof a.listCertificatesByCertificateValue === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing certificateValue', (done) => {
        try {
          a.listCertificatesByCertificateValue(null, null, null, (data, error) => {
            try {
              const displayE = 'certificateValue is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-listCertificatesByCertificateValue', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#downloadApprovedCertificate - errors', () => {
      it('should have a downloadApprovedCertificate function', (done) => {
        try {
          assert.equal(true, typeof a.downloadApprovedCertificate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing certificateId', (done) => {
        try {
          a.downloadApprovedCertificate(null, null, (data, error) => {
            try {
              const displayE = 'certificateId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-downloadApprovedCertificate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.downloadApprovedCertificate('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-downloadApprovedCertificate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listAuthenticationCACertificates - errors', () => {
      it('should have a listAuthenticationCACertificates function', (done) => {
        try {
          assert.equal(true, typeof a.listAuthenticationCACertificates === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listAuthenticationCACertificatesByAccount - errors', () => {
      it('should have a listAuthenticationCACertificatesByAccount function', (done) => {
        try {
          assert.equal(true, typeof a.listAuthenticationCACertificatesByAccount === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing accountId', (done) => {
        try {
          a.listAuthenticationCACertificatesByAccount(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'accountId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-listAuthenticationCACertificatesByAccount', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#uploadAuthenticationCACertificate - errors', () => {
      it('should have a uploadAuthenticationCACertificate function', (done) => {
        try {
          assert.equal(true, typeof a.uploadAuthenticationCACertificate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing accountId', (done) => {
        try {
          a.uploadAuthenticationCACertificate(null, null, (data, error) => {
            try {
              const displayE = 'accountId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-uploadAuthenticationCACertificate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAuthenticationCACertificate - errors', () => {
      it('should have a getAuthenticationCACertificate function', (done) => {
        try {
          assert.equal(true, typeof a.getAuthenticationCACertificate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing accountId', (done) => {
        try {
          a.getAuthenticationCACertificate(null, null, (data, error) => {
            try {
              const displayE = 'accountId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-getAuthenticationCACertificate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing authenticationCaId', (done) => {
        try {
          a.getAuthenticationCACertificate('fakeparam', null, (data, error) => {
            try {
              const displayE = 'authenticationCaId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-getAuthenticationCACertificate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAuthenticationCACertificate - errors', () => {
      it('should have a deleteAuthenticationCACertificate function', (done) => {
        try {
          assert.equal(true, typeof a.deleteAuthenticationCACertificate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing accountId', (done) => {
        try {
          a.deleteAuthenticationCACertificate(null, null, (data, error) => {
            try {
              const displayE = 'accountId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-deleteAuthenticationCACertificate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing authenticationCaId', (done) => {
        try {
          a.deleteAuthenticationCACertificate('fakeparam', null, (data, error) => {
            try {
              const displayE = 'authenticationCaId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-deleteAuthenticationCACertificate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createAuthenticationCATemplate - errors', () => {
      it('should have a createAuthenticationCATemplate function', (done) => {
        try {
          assert.equal(true, typeof a.createAuthenticationCATemplate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listAuthenticationCATemplate - errors', () => {
      it('should have a listAuthenticationCATemplate function', (done) => {
        try {
          assert.equal(true, typeof a.listAuthenticationCATemplate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateAuthenticationCATemplate - errors', () => {
      it('should have a updateAuthenticationCATemplate function', (done) => {
        try {
          assert.equal(true, typeof a.updateAuthenticationCATemplate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing authenticationCaTemplateId', (done) => {
        try {
          a.updateAuthenticationCATemplate(null, null, (data, error) => {
            try {
              const displayE = 'authenticationCaTemplateId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-updateAuthenticationCATemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAuthenticationCATemplate - errors', () => {
      it('should have a getAuthenticationCATemplate function', (done) => {
        try {
          assert.equal(true, typeof a.getAuthenticationCATemplate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing authenticationCaTemplateId', (done) => {
        try {
          a.getAuthenticationCATemplate(null, (data, error) => {
            try {
              const displayE = 'authenticationCaTemplateId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-getAuthenticationCATemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#disableAuthenticationCATemplate - errors', () => {
      it('should have a disableAuthenticationCATemplate function', (done) => {
        try {
          assert.equal(true, typeof a.disableAuthenticationCATemplate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing authenticationCaTemplateId', (done) => {
        try {
          a.disableAuthenticationCATemplate(null, (data, error) => {
            try {
              const displayE = 'authenticationCaTemplateId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-disableAuthenticationCATemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAuthenticationCATemplate - errors', () => {
      it('should have a deleteAuthenticationCATemplate function', (done) => {
        try {
          assert.equal(true, typeof a.deleteAuthenticationCATemplate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing authenticationCaTemplateId', (done) => {
        try {
          a.deleteAuthenticationCATemplate(null, (data, error) => {
            try {
              const displayE = 'authenticationCaTemplateId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-deleteAuthenticationCATemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#restoreAuthenticationCATemplate - errors', () => {
      it('should have a restoreAuthenticationCATemplate function', (done) => {
        try {
          assert.equal(true, typeof a.restoreAuthenticationCATemplate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing authenticationCaTemplateId', (done) => {
        try {
          a.restoreAuthenticationCATemplate(null, (data, error) => {
            try {
              const displayE = 'authenticationCaTemplateId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-restoreAuthenticationCATemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#enableAuthenticationCATemplate - errors', () => {
      it('should have a enableAuthenticationCATemplate function', (done) => {
        try {
          assert.equal(true, typeof a.enableAuthenticationCATemplate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing authenticationCaTemplateId', (done) => {
        try {
          a.enableAuthenticationCATemplate(null, (data, error) => {
            try {
              const displayE = 'authenticationCaTemplateId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-enableAuthenticationCATemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listAssignableAuthenticationCACertificates - errors', () => {
      it('should have a listAssignableAuthenticationCACertificates function', (done) => {
        try {
          assert.equal(true, typeof a.listAssignableAuthenticationCACertificates === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing enrollmentProfileId', (done) => {
        try {
          a.listAssignableAuthenticationCACertificates(null, null, null, null, (data, error) => {
            try {
              const displayE = 'enrollmentProfileId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-listAssignableAuthenticationCACertificates', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addAuthenticationCertificate - errors', () => {
      it('should have a addAuthenticationCertificate function', (done) => {
        try {
          assert.equal(true, typeof a.addAuthenticationCertificate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listAuthenticationCertificates - errors', () => {
      it('should have a listAuthenticationCertificates function', (done) => {
        try {
          assert.equal(true, typeof a.listAuthenticationCertificates === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAuthenticationCertificateDetails - errors', () => {
      it('should have a getAuthenticationCertificateDetails function', (done) => {
        try {
          assert.equal(true, typeof a.getAuthenticationCertificateDetails === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing authenticationCertificateId', (done) => {
        try {
          a.getAuthenticationCertificateDetails(null, (data, error) => {
            try {
              const displayE = 'authenticationCertificateId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-getAuthenticationCertificateDetails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateAuthenticationCertificate - errors', () => {
      it('should have a updateAuthenticationCertificate function', (done) => {
        try {
          assert.equal(true, typeof a.updateAuthenticationCertificate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing authenticationCertificateId', (done) => {
        try {
          a.updateAuthenticationCertificate(null, null, (data, error) => {
            try {
              const displayE = 'authenticationCertificateId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-updateAuthenticationCertificate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#disableAuthenticationCertificate - errors', () => {
      it('should have a disableAuthenticationCertificate function', (done) => {
        try {
          assert.equal(true, typeof a.disableAuthenticationCertificate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing authenticationCertificateId', (done) => {
        try {
          a.disableAuthenticationCertificate(null, (data, error) => {
            try {
              const displayE = 'authenticationCertificateId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-disableAuthenticationCertificate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#enableAuthenticationCertificate - errors', () => {
      it('should have a enableAuthenticationCertificate function', (done) => {
        try {
          assert.equal(true, typeof a.enableAuthenticationCertificate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing authenticationCertificateId', (done) => {
        try {
          a.enableAuthenticationCertificate(null, (data, error) => {
            try {
              const displayE = 'authenticationCertificateId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-enableAuthenticationCertificate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAuthenticationCertificate - errors', () => {
      it('should have a deleteAuthenticationCertificate function', (done) => {
        try {
          assert.equal(true, typeof a.deleteAuthenticationCertificate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing authenticationCertificateId', (done) => {
        try {
          a.deleteAuthenticationCertificate(null, (data, error) => {
            try {
              const displayE = 'authenticationCertificateId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-deleteAuthenticationCertificate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#restoreAuthenticationCertificate - errors', () => {
      it('should have a restoreAuthenticationCertificate function', (done) => {
        try {
          assert.equal(true, typeof a.restoreAuthenticationCertificate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing authenticationCertificateId', (done) => {
        try {
          a.restoreAuthenticationCertificate(null, (data, error) => {
            try {
              const displayE = 'authenticationCertificateId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-restoreAuthenticationCertificate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listCertificateProfiles - errors', () => {
      it('should have a listCertificateProfiles function', (done) => {
        try {
          assert.equal(true, typeof a.listCertificateProfiles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createCertificateProfile - errors', () => {
      it('should have a createCertificateProfile function', (done) => {
        try {
          assert.equal(true, typeof a.createCertificateProfile === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCertificateProfile - errors', () => {
      it('should have a getCertificateProfile function', (done) => {
        try {
          assert.equal(true, typeof a.getCertificateProfile === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing certificateProfileId', (done) => {
        try {
          a.getCertificateProfile(null, (data, error) => {
            try {
              const displayE = 'certificateProfileId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-getCertificateProfile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateCertificateProfile - errors', () => {
      it('should have a updateCertificateProfile function', (done) => {
        try {
          assert.equal(true, typeof a.updateCertificateProfile === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing certificateProfileId', (done) => {
        try {
          a.updateCertificateProfile(null, null, (data, error) => {
            try {
              const displayE = 'certificateProfileId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-updateCertificateProfile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#disableCertificateProfile - errors', () => {
      it('should have a disableCertificateProfile function', (done) => {
        try {
          assert.equal(true, typeof a.disableCertificateProfile === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing certificateProfileId', (done) => {
        try {
          a.disableCertificateProfile(null, (data, error) => {
            try {
              const displayE = 'certificateProfileId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-disableCertificateProfile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#enableCertificateProfile - errors', () => {
      it('should have a enableCertificateProfile function', (done) => {
        try {
          assert.equal(true, typeof a.enableCertificateProfile === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing certificateProfileId', (done) => {
        try {
          a.enableCertificateProfile(null, (data, error) => {
            try {
              const displayE = 'certificateProfileId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-enableCertificateProfile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteCertificateProfile - errors', () => {
      it('should have a deleteCertificateProfile function', (done) => {
        try {
          assert.equal(true, typeof a.deleteCertificateProfile === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing certificateProfileId', (done) => {
        try {
          a.deleteCertificateProfile(null, (data, error) => {
            try {
              const displayE = 'certificateProfileId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-deleteCertificateProfile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#restoreCertificateProfile - errors', () => {
      it('should have a restoreCertificateProfile function', (done) => {
        try {
          assert.equal(true, typeof a.restoreCertificateProfile === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing certificateProfileId', (done) => {
        try {
          a.restoreCertificateProfile(null, (data, error) => {
            try {
              const displayE = 'certificateProfileId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-restoreCertificateProfile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#assignCertificateProfileDivisions - errors', () => {
      it('should have a assignCertificateProfileDivisions function', (done) => {
        try {
          assert.equal(true, typeof a.assignCertificateProfileDivisions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing certificateProfileId', (done) => {
        try {
          a.assignCertificateProfileDivisions(null, null, (data, error) => {
            try {
              const displayE = 'certificateProfileId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-assignCertificateProfileDivisions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#unassignCertificateProfileDivisions - errors', () => {
      it('should have a unassignCertificateProfileDivisions function', (done) => {
        try {
          assert.equal(true, typeof a.unassignCertificateProfileDivisions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing certificateProfileId', (done) => {
        try {
          a.unassignCertificateProfileDivisions(null, null, (data, error) => {
            try {
              const displayE = 'certificateProfileId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-unassignCertificateProfileDivisions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRequestStatus - errors', () => {
      it('should have a getRequestStatus function', (done) => {
        try {
          assert.equal(true, typeof a.getRequestStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing certificateRequestId', (done) => {
        try {
          a.getRequestStatus(null, (data, error) => {
            try {
              const displayE = 'certificateRequestId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-getRequestStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listCertificateTemplates - errors', () => {
      it('should have a listCertificateTemplates function', (done) => {
        try {
          assert.equal(true, typeof a.listCertificateTemplates === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createCertificateTemplate - errors', () => {
      it('should have a createCertificateTemplate function', (done) => {
        try {
          assert.equal(true, typeof a.createCertificateTemplate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCertificateTemplate - errors', () => {
      it('should have a getCertificateTemplate function', (done) => {
        try {
          assert.equal(true, typeof a.getCertificateTemplate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing certificateTemplateId', (done) => {
        try {
          a.getCertificateTemplate(null, (data, error) => {
            try {
              const displayE = 'certificateTemplateId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-getCertificateTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateCertificateTemplate - errors', () => {
      it('should have a updateCertificateTemplate function', (done) => {
        try {
          assert.equal(true, typeof a.updateCertificateTemplate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing certificateTemplateId', (done) => {
        try {
          a.updateCertificateTemplate(null, null, (data, error) => {
            try {
              const displayE = 'certificateTemplateId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-updateCertificateTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#clonecertificatetemplate - errors', () => {
      it('should have a clonecertificatetemplate function', (done) => {
        try {
          assert.equal(true, typeof a.clonecertificatetemplate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing certificateTemplateId', (done) => {
        try {
          a.clonecertificatetemplate(null, null, (data, error) => {
            try {
              const displayE = 'certificateTemplateId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-clonecertificatetemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#disableCertificateTemplate - errors', () => {
      it('should have a disableCertificateTemplate function', (done) => {
        try {
          assert.equal(true, typeof a.disableCertificateTemplate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing certificateTemplateId', (done) => {
        try {
          a.disableCertificateTemplate(null, (data, error) => {
            try {
              const displayE = 'certificateTemplateId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-disableCertificateTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#enableCertificateTemplate - errors', () => {
      it('should have a enableCertificateTemplate function', (done) => {
        try {
          assert.equal(true, typeof a.enableCertificateTemplate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing certificateTemplateId', (done) => {
        try {
          a.enableCertificateTemplate(null, (data, error) => {
            try {
              const displayE = 'certificateTemplateId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-enableCertificateTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteCertificateTemplate - errors', () => {
      it('should have a deleteCertificateTemplate function', (done) => {
        try {
          assert.equal(true, typeof a.deleteCertificateTemplate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing certificateTemplateId', (done) => {
        try {
          a.deleteCertificateTemplate(null, (data, error) => {
            try {
              const displayE = 'certificateTemplateId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-deleteCertificateTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#restoreCertificateTemplate - errors', () => {
      it('should have a restoreCertificateTemplate function', (done) => {
        try {
          assert.equal(true, typeof a.restoreCertificateTemplate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing certificateTemplateId', (done) => {
        try {
          a.restoreCertificateTemplate(null, (data, error) => {
            try {
              const displayE = 'certificateTemplateId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-restoreCertificateTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createDevice - errors', () => {
      it('should have a createDevice function', (done) => {
        try {
          assert.equal(true, typeof a.createDevice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceList - errors', () => {
      it('should have a getDeviceList function', (done) => {
        try {
          assert.equal(true, typeof a.getDeviceList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceDetails - errors', () => {
      it('should have a getDeviceDetails function', (done) => {
        try {
          assert.equal(true, typeof a.getDeviceDetails === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceIdentifier', (done) => {
        try {
          a.getDeviceDetails(null, null, (data, error) => {
            try {
              const displayE = 'deviceIdentifier is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-getDeviceDetails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing divisionId', (done) => {
        try {
          a.getDeviceDetails('fakeparam', null, (data, error) => {
            try {
              const displayE = 'divisionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-getDeviceDetails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateDevice - errors', () => {
      it('should have a updateDevice function', (done) => {
        try {
          assert.equal(true, typeof a.updateDevice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceIdentifier', (done) => {
        try {
          a.updateDevice(null, null, null, (data, error) => {
            try {
              const displayE = 'deviceIdentifier is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-updateDevice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing divisionId', (done) => {
        try {
          a.updateDevice('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'divisionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-updateDevice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#archiveDevice - errors', () => {
      it('should have a archiveDevice function', (done) => {
        try {
          assert.equal(true, typeof a.archiveDevice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceIdentifier', (done) => {
        try {
          a.archiveDevice(null, null, (data, error) => {
            try {
              const displayE = 'deviceIdentifier is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-archiveDevice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing divisionId', (done) => {
        try {
          a.archiveDevice('fakeparam', null, (data, error) => {
            try {
              const displayE = 'divisionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-archiveDevice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#enableDevice - errors', () => {
      it('should have a enableDevice function', (done) => {
        try {
          assert.equal(true, typeof a.enableDevice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceIdentifier', (done) => {
        try {
          a.enableDevice(null, null, (data, error) => {
            try {
              const displayE = 'deviceIdentifier is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-enableDevice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing divisionId', (done) => {
        try {
          a.enableDevice('fakeparam', null, (data, error) => {
            try {
              const displayE = 'divisionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-enableDevice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#disableDevice - errors', () => {
      it('should have a disableDevice function', (done) => {
        try {
          assert.equal(true, typeof a.disableDevice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceIdentifier', (done) => {
        try {
          a.disableDevice(null, null, (data, error) => {
            try {
              const displayE = 'deviceIdentifier is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-disableDevice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing divisionId', (done) => {
        try {
          a.disableDevice('fakeparam', null, (data, error) => {
            try {
              const displayE = 'divisionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-disableDevice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#restoreDevice - errors', () => {
      it('should have a restoreDevice function', (done) => {
        try {
          assert.equal(true, typeof a.restoreDevice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceIdentifier', (done) => {
        try {
          a.restoreDevice(null, null, (data, error) => {
            try {
              const displayE = 'deviceIdentifier is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-restoreDevice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing divisionId', (done) => {
        try {
          a.restoreDevice('fakeparam', null, (data, error) => {
            try {
              const displayE = 'divisionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-restoreDevice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createDeviceV2 - errors', () => {
      it('should have a createDeviceV2 function', (done) => {
        try {
          assert.equal(true, typeof a.createDeviceV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceListV2 - errors', () => {
      it('should have a getDeviceListV2 function', (done) => {
        try {
          assert.equal(true, typeof a.getDeviceListV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceDetailsV2 - errors', () => {
      it('should have a getDeviceDetailsV2 function', (done) => {
        try {
          assert.equal(true, typeof a.getDeviceDetailsV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceId', (done) => {
        try {
          a.getDeviceDetailsV2(null, (data, error) => {
            try {
              const displayE = 'deviceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-getDeviceDetailsV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateDeviceV2 - errors', () => {
      it('should have a updateDeviceV2 function', (done) => {
        try {
          assert.equal(true, typeof a.updateDeviceV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceId', (done) => {
        try {
          a.updateDeviceV2(null, null, (data, error) => {
            try {
              const displayE = 'deviceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-updateDeviceV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#archiveDeviceV2 - errors', () => {
      it('should have a archiveDeviceV2 function', (done) => {
        try {
          assert.equal(true, typeof a.archiveDeviceV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceId', (done) => {
        try {
          a.archiveDeviceV2(null, (data, error) => {
            try {
              const displayE = 'deviceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-archiveDeviceV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#enableDeviceV2 - errors', () => {
      it('should have a enableDeviceV2 function', (done) => {
        try {
          assert.equal(true, typeof a.enableDeviceV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceId', (done) => {
        try {
          a.enableDeviceV2(null, (data, error) => {
            try {
              const displayE = 'deviceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-enableDeviceV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#disableDeviceV2 - errors', () => {
      it('should have a disableDeviceV2 function', (done) => {
        try {
          assert.equal(true, typeof a.disableDeviceV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceId', (done) => {
        try {
          a.disableDeviceV2(null, (data, error) => {
            try {
              const displayE = 'deviceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-disableDeviceV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#restoreDeviceV2 - errors', () => {
      it('should have a restoreDeviceV2 function', (done) => {
        try {
          assert.equal(true, typeof a.restoreDeviceV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceId', (done) => {
        try {
          a.restoreDeviceV2(null, (data, error) => {
            try {
              const displayE = 'deviceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-restoreDeviceV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#renewDeviceCertificateV2 - errors', () => {
      it('should have a renewDeviceCertificateV2 function', (done) => {
        try {
          assert.equal(true, typeof a.renewDeviceCertificateV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceId', (done) => {
        try {
          a.renewDeviceCertificateV2(null, null, (data, error) => {
            try {
              const displayE = 'deviceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-renewDeviceCertificateV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listDivisions - errors', () => {
      it('should have a listDivisions function', (done) => {
        try {
          assert.equal(true, typeof a.listDivisions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createDivision - errors', () => {
      it('should have a createDivision function', (done) => {
        try {
          assert.equal(true, typeof a.createDivision === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDivision - errors', () => {
      it('should have a getDivision function', (done) => {
        try {
          assert.equal(true, typeof a.getDivision === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing divisionId', (done) => {
        try {
          a.getDivision(null, (data, error) => {
            try {
              const displayE = 'divisionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-getDivision', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateDivision - errors', () => {
      it('should have a updateDivision function', (done) => {
        try {
          assert.equal(true, typeof a.updateDivision === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing divisionId', (done) => {
        try {
          a.updateDivision(null, null, (data, error) => {
            try {
              const displayE = 'divisionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-updateDivision', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#disableDivision - errors', () => {
      it('should have a disableDivision function', (done) => {
        try {
          assert.equal(true, typeof a.disableDivision === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing divisionId', (done) => {
        try {
          a.disableDivision(null, (data, error) => {
            try {
              const displayE = 'divisionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-disableDivision', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#enableDivision - errors', () => {
      it('should have a enableDivision function', (done) => {
        try {
          assert.equal(true, typeof a.enableDivision === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing divisionId', (done) => {
        try {
          a.enableDivision(null, (data, error) => {
            try {
              const displayE = 'divisionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-enableDivision', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDivision - errors', () => {
      it('should have a deleteDivision function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDivision === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing divisionId', (done) => {
        try {
          a.deleteDivision(null, (data, error) => {
            try {
              const displayE = 'divisionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-deleteDivision', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#restoreDivision - errors', () => {
      it('should have a restoreDivision function', (done) => {
        try {
          assert.equal(true, typeof a.restoreDivision === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing divisionId', (done) => {
        try {
          a.restoreDivision(null, (data, error) => {
            try {
              const displayE = 'divisionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-restoreDivision', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#assignDivisionUsers - errors', () => {
      it('should have a assignDivisionUsers function', (done) => {
        try {
          assert.equal(true, typeof a.assignDivisionUsers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing divisionId', (done) => {
        try {
          a.assignDivisionUsers(null, null, (data, error) => {
            try {
              const displayE = 'divisionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-assignDivisionUsers', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listDivisionUsers - errors', () => {
      it('should have a listDivisionUsers function', (done) => {
        try {
          assert.equal(true, typeof a.listDivisionUsers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing divisionId', (done) => {
        try {
          a.listDivisionUsers(null, null, null, (data, error) => {
            try {
              const displayE = 'divisionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-listDivisionUsers', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#removeDivisionUsers - errors', () => {
      it('should have a removeDivisionUsers function', (done) => {
        try {
          assert.equal(true, typeof a.removeDivisionUsers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing divisionId', (done) => {
        try {
          a.removeDivisionUsers(null, null, (data, error) => {
            try {
              const displayE = 'divisionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-removeDivisionUsers', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#assignICAstodivision - errors', () => {
      it('should have a assignICAstodivision function', (done) => {
        try {
          assert.equal(true, typeof a.assignICAstodivision === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing divisionId', (done) => {
        try {
          a.assignICAstodivision(null, null, (data, error) => {
            try {
              const displayE = 'divisionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-assignICAstodivision', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#removeICAsfromdivision - errors', () => {
      it('should have a removeICAsfromdivision function', (done) => {
        try {
          assert.equal(true, typeof a.removeICAsfromdivision === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing divisionId', (done) => {
        try {
          a.removeICAsfromdivision(null, null, (data, error) => {
            try {
              const displayE = 'divisionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-removeICAsfromdivision', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listdivisionICAassignments - errors', () => {
      it('should have a listdivisionICAassignments function', (done) => {
        try {
          assert.equal(true, typeof a.listdivisionICAassignments === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing divisionId', (done) => {
        try {
          a.listdivisionICAassignments(null, null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'divisionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-listdivisionICAassignments', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listEnrollmentProfiles - errors', () => {
      it('should have a listEnrollmentProfiles function', (done) => {
        try {
          assert.equal(true, typeof a.listEnrollmentProfiles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createEnrollmentProfile - errors', () => {
      it('should have a createEnrollmentProfile function', (done) => {
        try {
          assert.equal(true, typeof a.createEnrollmentProfile === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getExampleCertificateRequestEnrollmentProfileID - errors', () => {
      it('should have a getExampleCertificateRequestEnrollmentProfileID function', (done) => {
        try {
          assert.equal(true, typeof a.getExampleCertificateRequestEnrollmentProfileID === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing enrollmentProfileId', (done) => {
        try {
          a.getExampleCertificateRequestEnrollmentProfileID(null, (data, error) => {
            try {
              const displayE = 'enrollmentProfileId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-getExampleCertificateRequestEnrollmentProfileID', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getEnrollmentProfile - errors', () => {
      it('should have a getEnrollmentProfile function', (done) => {
        try {
          assert.equal(true, typeof a.getEnrollmentProfile === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing enrollmentProfileId', (done) => {
        try {
          a.getEnrollmentProfile(null, (data, error) => {
            try {
              const displayE = 'enrollmentProfileId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-getEnrollmentProfile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateEnrollmentProfile - errors', () => {
      it('should have a updateEnrollmentProfile function', (done) => {
        try {
          assert.equal(true, typeof a.updateEnrollmentProfile === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing enrollmentProfileId', (done) => {
        try {
          a.updateEnrollmentProfile(null, null, (data, error) => {
            try {
              const displayE = 'enrollmentProfileId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-updateEnrollmentProfile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listAssignableICAs - errors', () => {
      it('should have a listAssignableICAs function', (done) => {
        try {
          assert.equal(true, typeof a.listAssignableICAs === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing enrollmentProfileId', (done) => {
        try {
          a.listAssignableICAs(null, null, null, (data, error) => {
            try {
              const displayE = 'enrollmentProfileId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-listAssignableICAs', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIssuingICA - errors', () => {
      it('should have a getIssuingICA function', (done) => {
        try {
          assert.equal(true, typeof a.getIssuingICA === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing enrollmentProfileId', (done) => {
        try {
          a.getIssuingICA(null, null, (data, error) => {
            try {
              const displayE = 'enrollmentProfileId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-getIssuingICA', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getEnrollmentProfileSpecification - errors', () => {
      it('should have a getEnrollmentProfileSpecification function', (done) => {
        try {
          assert.equal(true, typeof a.getEnrollmentProfileSpecification === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing enrollmentProfileId', (done) => {
        try {
          a.getEnrollmentProfileSpecification(null, (data, error) => {
            try {
              const displayE = 'enrollmentProfileId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-getEnrollmentProfileSpecification', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getEnrollmentProfileStatus - errors', () => {
      it('should have a getEnrollmentProfileStatus function', (done) => {
        try {
          assert.equal(true, typeof a.getEnrollmentProfileStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing enrollmentProfileId', (done) => {
        try {
          a.getEnrollmentProfileStatus(null, (data, error) => {
            try {
              const displayE = 'enrollmentProfileId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-getEnrollmentProfileStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listEnrollmentPasscodes - errors', () => {
      it('should have a listEnrollmentPasscodes function', (done) => {
        try {
          assert.equal(true, typeof a.listEnrollmentPasscodes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createEnrollmentPasscode - errors', () => {
      it('should have a createEnrollmentPasscode function', (done) => {
        try {
          assert.equal(true, typeof a.createEnrollmentPasscode === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing enrollmentProfileId', (done) => {
        try {
          a.createEnrollmentPasscode(null, null, (data, error) => {
            try {
              const displayE = 'enrollmentProfileId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-createEnrollmentPasscode', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getEnrollmentPasscodeDetails - errors', () => {
      it('should have a getEnrollmentPasscodeDetails function', (done) => {
        try {
          assert.equal(true, typeof a.getEnrollmentPasscodeDetails === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing enrollmentProfileId', (done) => {
        try {
          a.getEnrollmentPasscodeDetails(null, null, (data, error) => {
            try {
              const displayE = 'enrollmentProfileId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-getEnrollmentPasscodeDetails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing passcodeId', (done) => {
        try {
          a.getEnrollmentPasscodeDetails('fakeparam', null, (data, error) => {
            try {
              const displayE = 'passcodeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-getEnrollmentPasscodeDetails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateEnrollmentPasscode - errors', () => {
      it('should have a updateEnrollmentPasscode function', (done) => {
        try {
          assert.equal(true, typeof a.updateEnrollmentPasscode === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing enrollmentProfileId', (done) => {
        try {
          a.updateEnrollmentPasscode(null, null, null, (data, error) => {
            try {
              const displayE = 'enrollmentProfileId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-updateEnrollmentPasscode', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing passcodeId', (done) => {
        try {
          a.updateEnrollmentPasscode('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'passcodeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-updateEnrollmentPasscode', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteEnrollmentPasscode - errors', () => {
      it('should have a deleteEnrollmentPasscode function', (done) => {
        try {
          assert.equal(true, typeof a.deleteEnrollmentPasscode === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing enrollmentProfileId', (done) => {
        try {
          a.deleteEnrollmentPasscode(null, null, (data, error) => {
            try {
              const displayE = 'enrollmentProfileId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-deleteEnrollmentPasscode', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing passcodeId', (done) => {
        try {
          a.deleteEnrollmentPasscode('fakeparam', null, (data, error) => {
            try {
              const displayE = 'passcodeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-deleteEnrollmentPasscode', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#restoreEnrollmentPasscode - errors', () => {
      it('should have a restoreEnrollmentPasscode function', (done) => {
        try {
          assert.equal(true, typeof a.restoreEnrollmentPasscode === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing enrollmentProfileId', (done) => {
        try {
          a.restoreEnrollmentPasscode(null, null, (data, error) => {
            try {
              const displayE = 'enrollmentProfileId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-restoreEnrollmentPasscode', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing passcodeId', (done) => {
        try {
          a.restoreEnrollmentPasscode('fakeparam', null, (data, error) => {
            try {
              const displayE = 'passcodeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-restoreEnrollmentPasscode', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#disableEnrollmentPasscode - errors', () => {
      it('should have a disableEnrollmentPasscode function', (done) => {
        try {
          assert.equal(true, typeof a.disableEnrollmentPasscode === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing enrollmentProfileId', (done) => {
        try {
          a.disableEnrollmentPasscode(null, null, (data, error) => {
            try {
              const displayE = 'enrollmentProfileId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-disableEnrollmentPasscode', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing passcodeId', (done) => {
        try {
          a.disableEnrollmentPasscode('fakeparam', null, (data, error) => {
            try {
              const displayE = 'passcodeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-disableEnrollmentPasscode', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#enableEnrollmentPasscode - errors', () => {
      it('should have a enableEnrollmentPasscode function', (done) => {
        try {
          assert.equal(true, typeof a.enableEnrollmentPasscode === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing enrollmentProfileId', (done) => {
        try {
          a.enableEnrollmentPasscode(null, null, (data, error) => {
            try {
              const displayE = 'enrollmentProfileId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-enableEnrollmentPasscode', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing passcodeId', (done) => {
        try {
          a.enableEnrollmentPasscode('fakeparam', null, (data, error) => {
            try {
              const displayE = 'passcodeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-enableEnrollmentPasscode', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#regenerateEnrollmentPasscode - errors', () => {
      it('should have a regenerateEnrollmentPasscode function', (done) => {
        try {
          assert.equal(true, typeof a.regenerateEnrollmentPasscode === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing enrollmentProfileId', (done) => {
        try {
          a.regenerateEnrollmentPasscode(null, null, (data, error) => {
            try {
              const displayE = 'enrollmentProfileId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-regenerateEnrollmentPasscode', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing passcodeId', (done) => {
        try {
          a.regenerateEnrollmentPasscode('fakeparam', null, (data, error) => {
            try {
              const displayE = 'passcodeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-regenerateEnrollmentPasscode', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createAcmeAuthentication - errors', () => {
      it('should have a createAcmeAuthentication function', (done) => {
        try {
          assert.equal(true, typeof a.createAcmeAuthentication === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createAcmeAuthentication(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-createAcmeAuthentication', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listAcmeAuthentication - errors', () => {
      it('should have a listAcmeAuthentication function', (done) => {
        try {
          assert.equal(true, typeof a.listAcmeAuthentication === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAcmeAuthentication - errors', () => {
      it('should have a getAcmeAuthentication function', (done) => {
        try {
          assert.equal(true, typeof a.getAcmeAuthentication === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing acmeAuthenticationId', (done) => {
        try {
          a.getAcmeAuthentication(null, (data, error) => {
            try {
              const displayE = 'acmeAuthenticationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-getAcmeAuthentication', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateAcmeAuthentication - errors', () => {
      it('should have a updateAcmeAuthentication function', (done) => {
        try {
          assert.equal(true, typeof a.updateAcmeAuthentication === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing acmeAuthenticationId', (done) => {
        try {
          a.updateAcmeAuthentication(null, null, (data, error) => {
            try {
              const displayE = 'acmeAuthenticationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-updateAcmeAuthentication', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#disableAcmeAuthentication - errors', () => {
      it('should have a disableAcmeAuthentication function', (done) => {
        try {
          assert.equal(true, typeof a.disableAcmeAuthentication === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing acmeAuthenticationId', (done) => {
        try {
          a.disableAcmeAuthentication(null, (data, error) => {
            try {
              const displayE = 'acmeAuthenticationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-disableAcmeAuthentication', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#enableAcmeAuthentication - errors', () => {
      it('should have a enableAcmeAuthentication function', (done) => {
        try {
          assert.equal(true, typeof a.enableAcmeAuthentication === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing acmeAuthenticationId', (done) => {
        try {
          a.enableAcmeAuthentication(null, (data, error) => {
            try {
              const displayE = 'acmeAuthenticationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-enableAcmeAuthentication', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAcmeAuthentication - errors', () => {
      it('should have a deleteAcmeAuthentication function', (done) => {
        try {
          assert.equal(true, typeof a.deleteAcmeAuthentication === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing acmeAuthenticationId', (done) => {
        try {
          a.deleteAcmeAuthentication(null, (data, error) => {
            try {
              const displayE = 'acmeAuthenticationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-deleteAcmeAuthentication', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#restoreAcmeAuthentication - errors', () => {
      it('should have a restoreAcmeAuthentication function', (done) => {
        try {
          assert.equal(true, typeof a.restoreAcmeAuthentication === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing acmeAuthenticationId', (done) => {
        try {
          a.restoreAcmeAuthentication(null, (data, error) => {
            try {
              const displayE = 'acmeAuthenticationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-restoreAcmeAuthentication', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#batchCSREnroll - errors', () => {
      it('should have a batchCSREnroll function', (done) => {
        try {
          assert.equal(true, typeof a.batchCSREnroll === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing enrollmentProfileId', (done) => {
        try {
          a.batchCSREnroll(null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'enrollmentProfileId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-batchCSREnroll', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing certificateFormat', (done) => {
        try {
          a.batchCSREnroll('fakeparam', 'fakeparam', null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'certificateFormat is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-batchCSREnroll', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.batchCSREnroll('fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-batchCSREnroll', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing emails', (done) => {
        try {
          a.batchCSREnroll('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'emails is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-batchCSREnroll', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#batchEnrollWithKeyGen - errors', () => {
      it('should have a batchEnrollWithKeyGen function', (done) => {
        try {
          assert.equal(true, typeof a.batchEnrollWithKeyGen === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing enrollmentProfileId', (done) => {
        try {
          a.batchEnrollWithKeyGen(null, null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'enrollmentProfileId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-batchEnrollWithKeyGen', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing certificateFormat', (done) => {
        try {
          a.batchEnrollWithKeyGen('fakeparam', 'fakeparam', null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'certificateFormat is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-batchEnrollWithKeyGen', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.batchEnrollWithKeyGen('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-batchEnrollWithKeyGen', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing emails', (done) => {
        try {
          a.batchEnrollWithKeyGen('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'emails is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-batchEnrollWithKeyGen', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing keyType', (done) => {
        try {
          a.batchEnrollWithKeyGen('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'keyType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-batchEnrollWithKeyGen', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing certificate', (done) => {
        try {
          a.batchEnrollWithKeyGen('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'certificate is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-batchEnrollWithKeyGen', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#batchEnrollWithKeyGenMac - errors', () => {
      it('should have a batchEnrollWithKeyGenMac function', (done) => {
        try {
          assert.equal(true, typeof a.batchEnrollWithKeyGenMac === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing enrollmentProfileId', (done) => {
        try {
          a.batchEnrollWithKeyGenMac(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'enrollmentProfileId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-batchEnrollWithKeyGenMac', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing certificateFormat', (done) => {
        try {
          a.batchEnrollWithKeyGenMac('fakeparam', 'fakeparam', null, null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'certificateFormat is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-batchEnrollWithKeyGenMac', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing macAddressFrom', (done) => {
        try {
          a.batchEnrollWithKeyGenMac('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'macAddressFrom is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-batchEnrollWithKeyGenMac', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing macAddressNumber', (done) => {
        try {
          a.batchEnrollWithKeyGenMac('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'macAddressNumber is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-batchEnrollWithKeyGenMac', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing macAddressStep', (done) => {
        try {
          a.batchEnrollWithKeyGenMac('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'macAddressStep is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-batchEnrollWithKeyGenMac', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing emails', (done) => {
        try {
          a.batchEnrollWithKeyGenMac('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'emails is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-batchEnrollWithKeyGenMac', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing keyType', (done) => {
        try {
          a.batchEnrollWithKeyGenMac('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'keyType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-batchEnrollWithKeyGenMac', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing certificate', (done) => {
        try {
          a.batchEnrollWithKeyGenMac('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'certificate is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-batchEnrollWithKeyGenMac', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listBatchJobs - errors', () => {
      it('should have a listBatchJobs function', (done) => {
        try {
          assert.equal(true, typeof a.listBatchJobs === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getBatchJobSettings - errors', () => {
      it('should have a getBatchJobSettings function', (done) => {
        try {
          assert.equal(true, typeof a.getBatchJobSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing jobId', (done) => {
        try {
          a.getBatchJobSettings(null, (data, error) => {
            try {
              const displayE = 'jobId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-getBatchJobSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateBatchJobSettings - errors', () => {
      it('should have a updateBatchJobSettings function', (done) => {
        try {
          assert.equal(true, typeof a.updateBatchJobSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing jobId', (done) => {
        try {
          a.updateBatchJobSettings(null, null, (data, error) => {
            try {
              const displayE = 'jobId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-updateBatchJobSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#regenerateBatchJobPasscode - errors', () => {
      it('should have a regenerateBatchJobPasscode function', (done) => {
        try {
          assert.equal(true, typeof a.regenerateBatchJobPasscode === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing jobId', (done) => {
        try {
          a.regenerateBatchJobPasscode(null, (data, error) => {
            try {
              const displayE = 'jobId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-regenerateBatchJobPasscode', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#approveJob - errors', () => {
      it('should have a approveJob function', (done) => {
        try {
          assert.equal(true, typeof a.approveJob === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing jobId', (done) => {
        try {
          a.approveJob(null, (data, error) => {
            try {
              const displayE = 'jobId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-approveJob', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rejectJob - errors', () => {
      it('should have a rejectJob function', (done) => {
        try {
          assert.equal(true, typeof a.rejectJob === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing jobId', (done) => {
        try {
          a.rejectJob(null, null, (data, error) => {
            try {
              const displayE = 'jobId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-rejectJob', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#downloadReport - errors', () => {
      it('should have a downloadReport function', (done) => {
        try {
          assert.equal(true, typeof a.downloadReport === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing jobId', (done) => {
        try {
          a.downloadReport(null, (data, error) => {
            try {
              const displayE = 'jobId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-downloadReport', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#downloadCertificates - errors', () => {
      it('should have a downloadCertificates function', (done) => {
        try {
          assert.equal(true, typeof a.downloadCertificates === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing jobId', (done) => {
        try {
          a.downloadCertificates(null, (data, error) => {
            try {
              const displayE = 'jobId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-downloadCertificates', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDownloadUrls - errors', () => {
      it('should have a deleteDownloadUrls function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDownloadUrls === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing jobId', (done) => {
        try {
          a.deleteDownloadUrls(null, (data, error) => {
            try {
              const displayE = 'jobId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-deleteDownloadUrls', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#resendDownloadUrls - errors', () => {
      it('should have a resendDownloadUrls function', (done) => {
        try {
          assert.equal(true, typeof a.resendDownloadUrls === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing jobId', (done) => {
        try {
          a.resendDownloadUrls(null, (data, error) => {
            try {
              const displayE = 'jobId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-resendDownloadUrls', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#batchEnrollExternal - errors', () => {
      it('should have a batchEnrollExternal function', (done) => {
        try {
          assert.equal(true, typeof a.batchEnrollExternal === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing enrollmentProfileId', (done) => {
        try {
          a.batchEnrollExternal(null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'enrollmentProfileId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-batchEnrollExternal', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.batchEnrollExternal('fakeparam', 'fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-batchEnrollExternal', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing emails', (done) => {
        try {
          a.batchEnrollExternal('fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'emails is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-batchEnrollExternal', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#downloadMapping - errors', () => {
      it('should have a downloadMapping function', (done) => {
        try {
          assert.equal(true, typeof a.downloadMapping === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing jobId', (done) => {
        try {
          a.downloadMapping(null, (data, error) => {
            try {
              const displayE = 'jobId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-downloadMapping', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#downloadByJobIdAndUniqueId - errors', () => {
      it('should have a downloadByJobIdAndUniqueId function', (done) => {
        try {
          assert.equal(true, typeof a.downloadByJobIdAndUniqueId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing jobId', (done) => {
        try {
          a.downloadByJobIdAndUniqueId(null, null, null, (data, error) => {
            try {
              const displayE = 'jobId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-downloadByJobIdAndUniqueId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uniqueIdentifier', (done) => {
        try {
          a.downloadByJobIdAndUniqueId('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'uniqueIdentifier is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-downloadByJobIdAndUniqueId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing format', (done) => {
        try {
          a.downloadByJobIdAndUniqueId('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'format is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-downloadByJobIdAndUniqueId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#downloadByCommonName - errors', () => {
      it('should have a downloadByCommonName function', (done) => {
        try {
          assert.equal(true, typeof a.downloadByCommonName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing commonName', (done) => {
        try {
          a.downloadByCommonName(null, null, (data, error) => {
            try {
              const displayE = 'commonName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-downloadByCommonName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing format', (done) => {
        try {
          a.downloadByCommonName('fakeparam', null, (data, error) => {
            try {
              const displayE = 'format is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-downloadByCommonName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#downloadByJobIdAndCommonName - errors', () => {
      it('should have a downloadByJobIdAndCommonName function', (done) => {
        try {
          assert.equal(true, typeof a.downloadByJobIdAndCommonName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing jobId', (done) => {
        try {
          a.downloadByJobIdAndCommonName(null, null, null, (data, error) => {
            try {
              const displayE = 'jobId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-downloadByJobIdAndCommonName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing commonName', (done) => {
        try {
          a.downloadByJobIdAndCommonName('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'commonName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-downloadByJobIdAndCommonName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing format', (done) => {
        try {
          a.downloadByJobIdAndCommonName('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'format is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-downloadByJobIdAndCommonName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#downloadByCertificateValue - errors', () => {
      it('should have a downloadByCertificateValue function', (done) => {
        try {
          assert.equal(true, typeof a.downloadByCertificateValue === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing certificateValue', (done) => {
        try {
          a.downloadByCertificateValue(null, null, (data, error) => {
            try {
              const displayE = 'certificateValue is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-downloadByCertificateValue', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing format', (done) => {
        try {
          a.downloadByCertificateValue('fakeparam', null, (data, error) => {
            try {
              const displayE = 'format is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-downloadByCertificateValue', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#downloadByJobIdAndCertificateValue - errors', () => {
      it('should have a downloadByJobIdAndCertificateValue function', (done) => {
        try {
          assert.equal(true, typeof a.downloadByJobIdAndCertificateValue === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing jobId', (done) => {
        try {
          a.downloadByJobIdAndCertificateValue(null, null, null, (data, error) => {
            try {
              const displayE = 'jobId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-downloadByJobIdAndCertificateValue', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing certificateValue', (done) => {
        try {
          a.downloadByJobIdAndCertificateValue('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'certificateValue is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-downloadByJobIdAndCertificateValue', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing format', (done) => {
        try {
          a.downloadByJobIdAndCertificateValue('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'format is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-downloadByJobIdAndCertificateValue', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createDeviceProfile - errors', () => {
      it('should have a createDeviceProfile function', (done) => {
        try {
          assert.equal(true, typeof a.createDeviceProfile === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listdeviceprofiles - errors', () => {
      it('should have a listdeviceprofiles function', (done) => {
        try {
          assert.equal(true, typeof a.listdeviceprofiles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceProfile - errors', () => {
      it('should have a getDeviceProfile function', (done) => {
        try {
          assert.equal(true, typeof a.getDeviceProfile === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceProfileId', (done) => {
        try {
          a.getDeviceProfile(null, (data, error) => {
            try {
              const displayE = 'deviceProfileId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-getDeviceProfile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateDeviceProfile - errors', () => {
      it('should have a updateDeviceProfile function', (done) => {
        try {
          assert.equal(true, typeof a.updateDeviceProfile === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceProfileId', (done) => {
        try {
          a.updateDeviceProfile(null, null, (data, error) => {
            try {
              const displayE = 'deviceProfileId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-updateDeviceProfile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDeviceProfile - errors', () => {
      it('should have a deleteDeviceProfile function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDeviceProfile === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceProfileId', (done) => {
        try {
          a.deleteDeviceProfile(null, (data, error) => {
            try {
              const displayE = 'deviceProfileId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-deleteDeviceProfile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#restoreDeviceProfile - errors', () => {
      it('should have a restoreDeviceProfile function', (done) => {
        try {
          assert.equal(true, typeof a.restoreDeviceProfile === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceProfileId', (done) => {
        try {
          a.restoreDeviceProfile(null, (data, error) => {
            try {
              const displayE = 'deviceProfileId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-restoreDeviceProfile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getICA - errors', () => {
      it('should have a getICA function', (done) => {
        try {
          assert.equal(true, typeof a.getICA === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing icaId', (done) => {
        try {
          a.getICA(null, (data, error) => {
            try {
              const displayE = 'icaId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-getICA', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listICAs - errors', () => {
      it('should have a listICAs function', (done) => {
        try {
          assert.equal(true, typeof a.listICAs === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getGatewayDetails - errors', () => {
      it('should have a getGatewayDetails function', (done) => {
        try {
          assert.equal(true, typeof a.getGatewayDetails === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing gatewayId', (done) => {
        try {
          a.getGatewayDetails(null, (data, error) => {
            try {
              const displayE = 'gatewayId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-getGatewayDetails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateGateway - errors', () => {
      it('should have a updateGateway function', (done) => {
        try {
          assert.equal(true, typeof a.updateGateway === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing gatewayId', (done) => {
        try {
          a.updateGateway(null, null, (data, error) => {
            try {
              const displayE = 'gatewayId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-updateGateway', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listGateways - errors', () => {
      it('should have a listGateways function', (done) => {
        try {
          assert.equal(true, typeof a.listGateways === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createGateway - errors', () => {
      it('should have a createGateway function', (done) => {
        try {
          assert.equal(true, typeof a.createGateway === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#enableGateway - errors', () => {
      it('should have a enableGateway function', (done) => {
        try {
          assert.equal(true, typeof a.enableGateway === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing gatewayId', (done) => {
        try {
          a.enableGateway(null, (data, error) => {
            try {
              const displayE = 'gatewayId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-enableGateway', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#disableGateway - errors', () => {
      it('should have a disableGateway function', (done) => {
        try {
          assert.equal(true, typeof a.disableGateway === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing gatewayId', (done) => {
        try {
          a.disableGateway(null, (data, error) => {
            try {
              const displayE = 'gatewayId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-disableGateway', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteGateway - errors', () => {
      it('should have a deleteGateway function', (done) => {
        try {
          assert.equal(true, typeof a.deleteGateway === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing gatewayId', (done) => {
        try {
          a.deleteGateway(null, (data, error) => {
            try {
              const displayE = 'gatewayId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-deleteGateway', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#restoreGateway - errors', () => {
      it('should have a restoreGateway function', (done) => {
        try {
          assert.equal(true, typeof a.restoreGateway === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing gatewayId', (done) => {
        try {
          a.restoreGateway(null, (data, error) => {
            try {
              const displayE = 'gatewayId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-restoreGateway', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#downloadGatewayConfig - errors', () => {
      it('should have a downloadGatewayConfig function', (done) => {
        try {
          assert.equal(true, typeof a.downloadGatewayConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing gatewayId', (done) => {
        try {
          a.downloadGatewayConfig(null, (data, error) => {
            try {
              const displayE = 'gatewayId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-downloadGatewayConfig', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listOCSPGroups - errors', () => {
      it('should have a listOCSPGroups function', (done) => {
        try {
          assert.equal(true, typeof a.listOCSPGroups === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createOCSPGroup - errors', () => {
      it('should have a createOCSPGroup function', (done) => {
        try {
          assert.equal(true, typeof a.createOCSPGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOCSPGroupDetails - errors', () => {
      it('should have a getOCSPGroupDetails function', (done) => {
        try {
          assert.equal(true, typeof a.getOCSPGroupDetails === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ocspGroupId', (done) => {
        try {
          a.getOCSPGroupDetails(null, (data, error) => {
            try {
              const displayE = 'ocspGroupId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-getOCSPGroupDetails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateOCSPGroup - errors', () => {
      it('should have a updateOCSPGroup function', (done) => {
        try {
          assert.equal(true, typeof a.updateOCSPGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ocspGroupId', (done) => {
        try {
          a.updateOCSPGroup(null, null, (data, error) => {
            try {
              const displayE = 'ocspGroupId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-updateOCSPGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteOCSPGroup - errors', () => {
      it('should have a deleteOCSPGroup function', (done) => {
        try {
          assert.equal(true, typeof a.deleteOCSPGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ocspGroupId', (done) => {
        try {
          a.deleteOCSPGroup(null, (data, error) => {
            try {
              const displayE = 'ocspGroupId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-deleteOCSPGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addCertsOCSPGroup - errors', () => {
      it('should have a addCertsOCSPGroup function', (done) => {
        try {
          assert.equal(true, typeof a.addCertsOCSPGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ocspGroupId', (done) => {
        try {
          a.addCertsOCSPGroup(null, null, (data, error) => {
            try {
              const displayE = 'ocspGroupId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-addCertsOCSPGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#removeCertsOCSPGroup - errors', () => {
      it('should have a removeCertsOCSPGroup function', (done) => {
        try {
          assert.equal(true, typeof a.removeCertsOCSPGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ocspGroupId', (done) => {
        try {
          a.removeCertsOCSPGroup(null, null, (data, error) => {
            try {
              const displayE = 'ocspGroupId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-removeCertsOCSPGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listOCSPGroupCerts - errors', () => {
      it('should have a listOCSPGroupCerts function', (done) => {
        try {
          assert.equal(true, typeof a.listOCSPGroupCerts === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ocspGroupId', (done) => {
        try {
          a.listOCSPGroupCerts(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'ocspGroupId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-digicert_pki-adapter-listOCSPGroupCerts', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
  });
});
