/* @copyright Itential, LLC 2019 (pre-modifications) */

// Set globals
/* global describe it log pronghornProps */
/* eslint no-unused-vars: warn */
/* eslint no-underscore-dangle: warn  */
/* eslint import/no-dynamic-require:warn */

// include required items for testing & logging
const assert = require('assert');
const fs = require('fs');
const path = require('path');
const util = require('util');
const mocha = require('mocha');
const winston = require('winston');
const { expect } = require('chai');
const { use } = require('chai');
const td = require('testdouble');

const anything = td.matchers.anything();

// stub and attemptTimeout are used throughout the code so set them here
let logLevel = 'none';
const isRapidFail = false;
const isSaveMockData = false;

// read in the properties from the sampleProperties files
let adaptdir = __dirname;
if (adaptdir.endsWith('/test/integration')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 17);
} else if (adaptdir.endsWith('/test/unit')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 10);
}
const samProps = require(`${adaptdir}/sampleProperties.json`).properties;

// these variables can be changed to run in integrated mode so easier to set them here
// always check these in with bogus data!!!
samProps.stub = true;
samProps.host = 'replace.hostorip.here';
samProps.authentication.username = 'username';
samProps.authentication.password = 'password';
samProps.protocol = 'http';
samProps.port = 80;
samProps.ssl.enabled = false;
samProps.ssl.accept_invalid_cert = false;
if (samProps.request.attempt_timeout < 30000) {
  samProps.request.attempt_timeout = 30000;
}
samProps.devicebroker.enabled = true;
const attemptTimeout = samProps.request.attempt_timeout;
const { stub } = samProps;

// these are the adapter properties. You generally should not need to alter
// any of these after they are initially set up
global.pronghornProps = {
  pathProps: {
    encrypted: false
  },
  adapterProps: {
    adapters: [{
      id: 'Test-digicert_pki',
      type: 'Digicert',
      properties: samProps
    }]
  }
};

global.$HOME = `${__dirname}/../..`;

// set the log levels that Pronghorn uses, spam and trace are not defaulted in so without
// this you may error on log.trace calls.
const myCustomLevels = {
  levels: {
    spam: 6,
    trace: 5,
    debug: 4,
    info: 3,
    warn: 2,
    error: 1,
    none: 0
  }
};

// need to see if there is a log level passed in
process.argv.forEach((val) => {
  // is there a log level defined to be passed in?
  if (val.indexOf('--LOG') === 0) {
    // get the desired log level
    const inputVal = val.split('=')[1];

    // validate the log level is supported, if so set it
    if (Object.hasOwnProperty.call(myCustomLevels.levels, inputVal)) {
      logLevel = inputVal;
    }
  }
});

// need to set global logging
global.log = winston.createLogger({
  level: logLevel,
  levels: myCustomLevels.levels,
  transports: [
    new winston.transports.Console()
  ]
});

/**
 * Runs the common asserts for test
 */
function runCommonAsserts(data, error) {
  assert.equal(undefined, error);
  assert.notEqual(undefined, data);
  assert.notEqual(null, data);
  assert.notEqual(undefined, data.response);
  assert.notEqual(null, data.response);
}

/**
 * Runs the error asserts for the test
 */
function runErrorAsserts(data, error, code, origin, displayStr) {
  assert.equal(null, data);
  assert.notEqual(undefined, error);
  assert.notEqual(null, error);
  assert.notEqual(undefined, error.IAPerror);
  assert.notEqual(null, error.IAPerror);
  assert.notEqual(undefined, error.IAPerror.displayString);
  assert.notEqual(null, error.IAPerror.displayString);
  assert.equal(code, error.icode);
  assert.equal(origin, error.IAPerror.origin);
  assert.equal(displayStr, error.IAPerror.displayString);
}

/**
 * @function saveMockData
 * Attempts to take data from responses and place them in MockDataFiles to help create Mockdata.
 * Note, this was built based on entity file structure for Adapter-Engine 1.6.x
 * @param {string} entityName - Name of the entity saving mock data for
 * @param {string} actionName -  Name of the action saving mock data for
 * @param {string} descriptor -  Something to describe this test (used as a type)
 * @param {string or object} responseData - The data to put in the mock file.
 */
function saveMockData(entityName, actionName, descriptor, responseData) {
  // do not need to save mockdata if we are running in stub mode (already has mock data) or if told not to save
  if (stub || !isSaveMockData) {
    return false;
  }

  // must have a response in order to store the response
  if (responseData && responseData.response) {
    let data = responseData.response;

    // if there was a raw response that one is better as it is untranslated
    if (responseData.raw) {
      data = responseData.raw;

      try {
        const temp = JSON.parse(data);
        data = temp;
      } catch (pex) {
        // do not care if it did not parse as we will just use data
      }
    }

    try {
      const base = path.join(__dirname, `../../entities/${entityName}/`);
      const mockdatafolder = 'mockdatafiles';
      const filename = `mockdatafiles/${actionName}-${descriptor}.json`;

      if (!fs.existsSync(base + mockdatafolder)) {
        fs.mkdirSync(base + mockdatafolder);
      }

      // write the data we retrieved
      fs.writeFile(base + filename, JSON.stringify(data, null, 2), 'utf8', (errWritingMock) => {
        if (errWritingMock) throw errWritingMock;

        // update the action file to reflect the changes. Note: We're replacing the default object for now!
        fs.readFile(`${base}action.json`, (errRead, content) => {
          if (errRead) throw errRead;

          // parse the action file into JSON
          const parsedJson = JSON.parse(content);

          // The object update we'll write in.
          const responseObj = {
            type: descriptor,
            key: '',
            mockFile: filename
          };

          // get the object for method we're trying to change.
          const currentMethodAction = parsedJson.actions.find((obj) => obj.name === actionName);

          // if the method was not found - should never happen but...
          if (!currentMethodAction) {
            throw Error('Can\'t find an action for this method in the provided entity.');
          }

          // if there is a response object, we want to replace the Response object. Otherwise we'll create one.
          const actionResponseObj = currentMethodAction.responseObjects.find((obj) => obj.type === descriptor);

          // Add the action responseObj back into the array of response objects.
          if (!actionResponseObj) {
            // if there is a default response object, we want to get the key.
            const defaultResponseObj = currentMethodAction.responseObjects.find((obj) => obj.type === 'default');

            // save the default key into the new response object
            if (defaultResponseObj) {
              responseObj.key = defaultResponseObj.key;
            }

            // save the new response object
            currentMethodAction.responseObjects = [responseObj];
          } else {
            // update the location of the mock data file
            actionResponseObj.mockFile = responseObj.mockFile;
          }

          // Save results
          fs.writeFile(`${base}action.json`, JSON.stringify(parsedJson, null, 2), (err) => {
            if (err) throw err;
          });
        });
      });
    } catch (e) {
      log.debug(`Failed to save mock data for ${actionName}. ${e.message}`);
      return false;
    }
  }

  // no response to save
  log.debug(`No data passed to save into mockdata for ${actionName}`);
  return false;
}

// require the adapter that we are going to be using
const Digicert = require('../../adapter');

// begin the testing - these should be pretty well defined between the describe and the it!
describe('[integration] Digicert Adapter Test', () => {
  describe('Digicert Class Tests', () => {
    const a = new Digicert(
      pronghornProps.adapterProps.adapters[0].id,
      pronghornProps.adapterProps.adapters[0].properties
    );

    if (isRapidFail) {
      const state = {};
      state.passed = true;

      mocha.afterEach(function x() {
        state.passed = state.passed
        && (this.currentTest.state === 'passed');
      });
      mocha.beforeEach(function x() {
        if (!state.passed) {
          return this.currentTest.skip();
        }
        return true;
      });
    }

    describe('#class instance created', () => {
      it('should be a class with properties', (done) => {
        try {
          assert.notEqual(null, a);
          assert.notEqual(undefined, a);
          const checkId = global.pronghornProps.adapterProps.adapters[0].id;
          assert.equal(checkId, a.id);
          assert.notEqual(null, a.allProps);
          const check = global.pronghornProps.adapterProps.adapters[0].properties.healthcheck.type;
          assert.equal(check, a.healthcheckType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#connect', () => {
      it('should get connected - no healthcheck', (done) => {
        try {
          a.healthcheckType = 'none';
          a.connect();

          try {
            assert.equal(true, a.alive);
            done();
          } catch (error) {
            log.error(`Test Failure: ${error}`);
            done(error);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
      it('should get connected - startup healthcheck', (done) => {
        try {
          a.healthcheckType = 'startup';
          a.connect();

          try {
            assert.equal(true, a.alive);
            done();
          } catch (error) {
            log.error(`Test Failure: ${error}`);
            done(error);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
    });

    describe('#healthCheck', () => {
      it('should be healthy', (done) => {
        try {
          a.healthCheck(null, (data) => {
            try {
              assert.equal(true, a.healthy);
              saveMockData('system', 'healthcheck', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // broker tests
    describe('#getDevicesFiltered - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          const opts = {
            filter: {
              name: 'deviceName'
            }
          };
          a.getDevicesFiltered(opts, (data, error) => {
            try {
              if (stub) {
                if (samProps.devicebroker.getDevicesFiltered[0].handleFailure === 'ignore') {
                  assert.equal(null, error);
                  assert.notEqual(undefined, data);
                  assert.notEqual(null, data);
                  assert.equal(0, data.total);
                  assert.equal(0, data.list.length);
                } else {
                  const displayE = 'Error 400 received on request';
                  runErrorAsserts(data, error, 'AD.500', 'Test-digicert_pki-connectorRest-handleEndResponse', displayE);
                }
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapGetDeviceCount - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          const opts = {
            filter: {
              name: 'deviceName'
            }
          };
          a.iapGetDeviceCount((data, error) => {
            try {
              if (stub) {
                if (samProps.devicebroker.getDevicesFiltered[0].handleFailure === 'ignore') {
                  assert.equal(null, error);
                  assert.notEqual(undefined, data);
                  assert.notEqual(null, data);
                  assert.equal(0, data.count);
                } else {
                  const displayE = 'Error 400 received on request';
                  runErrorAsserts(data, error, 'AD.500', 'Test-digicert_pki-connectorRest-handleEndResponse', displayE);
                }
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // exposed cache tests
    describe('#iapPopulateEntityCache - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.iapPopulateEntityCache('Device', (data, error) => {
            try {
              if (stub) {
                assert.equal(null, data);
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                done();
              } else {
                assert.equal(undefined, error);
                assert.equal('success', data[0]);
                done();
              }
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapRetrieveEntitiesCache - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.iapRetrieveEntitiesCache('Device', {}, (data, error) => {
            try {
              if (stub) {
                assert.equal(null, data);
                assert.notEqual(null, error);
                assert.notEqual(undefined, error);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(null, data);
                assert.notEqual(undefined, data);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
    /*
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    *** All code above this comment will be replaced during a migration ***
    ******************* DO NOT REMOVE THIS COMMENT BLOCK ******************
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    */

    const auditAPIDateFrom = 'fakedata';
    const auditAPIDateTo = 'fakedata';
    describe('#getAuditsUsingGET - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getAuditsUsingGET(null, null, auditAPIDateFrom, auditAPIDateTo, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-digicert_pki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AuditAPI', 'getAuditsUsingGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const auditAPIAuditId = 555;
    describe('#getAuditUsingGET - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getAuditUsingGET(auditAPIAuditId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-digicert_pki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AuditAPI', 'getAuditUsingGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const cAAPICaId = 'fakedata';
    describe('#getCAInfoUsingGET - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getCAInfoUsingGET(cAAPICaId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-digicert_pki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CAAPI', 'getCAInfoUsingGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const cAAPISeatType = 'fakedata';
    describe('#getCAUsingGET - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getCAUsingGET(cAAPISeatType, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-digicert_pki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CAAPI', 'getCAUsingGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const certificateEnrollmentAPIEnrollCertificateUsingPOSTBodyParam = {
      attributes: {
        common_name: 'string',
        content_type: 'string',
        counter_signature: 'string',
        country: 'string',
        custom_attributes: {},
        dn_qualifier: 'string',
        domain_component: [
          {
            id: 'string',
            mandatory: true,
            type: 'string',
            value: 'string'
          }
        ],
        domain_name: 'string',
        email: 'string',
        given_name: 'string',
        ip_address: 'string',
        job_title: 'string',
        locality: 'string',
        message_digest: 'string',
        organization_name: 'string',
        organization_unit: [
          {
            id: 'string',
            mandatory: true,
            type: 'string',
            value: 'string'
          }
        ],
        postal_code: 'string',
        pseudonym: 'string',
        san: {
          custom_attributes: {},
          directory_name: 'string',
          dns_name: [
            {
              id: 'string',
              mandatory: true,
              type: 'string',
              value: 'string'
            }
          ],
          ip_address: [
            {
              id: 'string',
              mandatory: true,
              type: 'string',
              value: 'string'
            }
          ],
          other_name: [
            {
              id: 'string',
              mandatory: true,
              type: 'string',
              value: 'string'
            }
          ],
          registered_id: [
            {
              id: 'string',
              mandatory: false,
              type: 'string',
              value: 'string'
            }
          ],
          rfc822_name: [
            {
              id: 'string',
              mandatory: false,
              type: 'string',
              value: 'string'
            }
          ],
          user_principal_name: [
            {
              id: 'string',
              mandatory: true,
              type: 'string',
              value: 'string'
            }
          ]
        },
        serial_number: 'string',
        signing_time: 'string',
        state: 'string',
        street_address: [
          {
            id: 'string',
            mandatory: false,
            type: 'string',
            value: 'string'
          }
        ],
        surname: 'string',
        unique_identifier: 'string',
        unstructured_address: 'string',
        unstructured_name: 'string',
        user_id: 'string'
      },
      authentication: {},
      csr: 'string',
      profile: {
        id: 'string',
        name: 'string'
      },
      seat: {
        email: 'string',
        seat_id: 'string',
        seat_name: 'string'
      },
      session_key: 'string',
      validity: {
        duration: 8,
        unit: 'string'
      }
    };
    describe('#enrollCertificateUsingPOST - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.enrollCertificateUsingPOST(certificateEnrollmentAPIEnrollCertificateUsingPOSTBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-digicert_pki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CertificateEnrollmentAPI', 'enrollCertificateUsingPOST', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const certificateEnrollmentAPICreateCertificateUsingPOSTBodyParam = {
      csr: 'string',
      extensions: {},
      include_ca_chain: true,
      profile: {
        id: 'string',
        name: 'string'
      },
      seat: {
        email: 'string',
        seat_id: 'string',
        seat_name: 'string'
      },
      subject: {},
      validity: {
        duration: 10,
        unit: 'string'
      }
    };
    describe('#createCertificateUsingPOST - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createCertificateUsingPOST(certificateEnrollmentAPICreateCertificateUsingPOSTBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-digicert_pki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CertificateEnrollmentAPI', 'createCertificateUsingPOST', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const certificateEnrollmentAPISerialNumber = 'fakedata';
    const certificateEnrollmentAPIRenewExternalCertificateUsingPOSTBodyParam = {
      csr: 'string',
      extensions: {},
      include_ca_chain: false,
      profile: {
        id: 'string',
        name: 'string'
      },
      seat: {
        email: 'string',
        seat_id: 'string',
        seat_name: 'string'
      },
      subject: {},
      validity: {
        duration: 7,
        unit: 'string'
      }
    };
    describe('#renewExternalCertificateUsingPOST - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.renewExternalCertificateUsingPOST(certificateEnrollmentAPIRenewExternalCertificateUsingPOSTBodyParam, certificateEnrollmentAPISerialNumber, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-digicert_pki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CertificateEnrollmentAPI', 'renewExternalCertificateUsingPOST', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const certificateEnrollmentAPIRenewCertificateUsingPOSTBodyParam = {
      attributes: {
        common_name: 'string',
        content_type: 'string',
        counter_signature: 'string',
        country: 'string',
        custom_attributes: {},
        dn_qualifier: 'string',
        domain_component: [
          {
            id: 'string',
            mandatory: true,
            type: 'string',
            value: 'string'
          }
        ],
        domain_name: 'string',
        email: 'string',
        given_name: 'string',
        ip_address: 'string',
        job_title: 'string',
        locality: 'string',
        message_digest: 'string',
        organization_name: 'string',
        organization_unit: [
          {
            id: 'string',
            mandatory: false,
            type: 'string',
            value: 'string'
          }
        ],
        postal_code: 'string',
        pseudonym: 'string',
        san: {
          custom_attributes: {},
          directory_name: 'string',
          dns_name: [
            {
              id: 'string',
              mandatory: false,
              type: 'string',
              value: 'string'
            }
          ],
          ip_address: [
            {
              id: 'string',
              mandatory: false,
              type: 'string',
              value: 'string'
            }
          ],
          other_name: [
            {
              id: 'string',
              mandatory: true,
              type: 'string',
              value: 'string'
            }
          ],
          registered_id: [
            {
              id: 'string',
              mandatory: true,
              type: 'string',
              value: 'string'
            }
          ],
          rfc822_name: [
            {
              id: 'string',
              mandatory: true,
              type: 'string',
              value: 'string'
            }
          ],
          user_principal_name: [
            {
              id: 'string',
              mandatory: false,
              type: 'string',
              value: 'string'
            }
          ]
        },
        serial_number: 'string',
        signing_time: 'string',
        state: 'string',
        street_address: [
          {
            id: 'string',
            mandatory: true,
            type: 'string',
            value: 'string'
          }
        ],
        surname: 'string',
        unique_identifier: 'string',
        unstructured_address: 'string',
        unstructured_name: 'string',
        user_id: 'string'
      },
      authentication: {},
      csr: 'string',
      profile: {
        id: 'string',
        name: 'string'
      },
      seat: {
        email: 'string',
        seat_id: 'string',
        seat_name: 'string'
      },
      session_key: 'string',
      validity: {
        duration: 10,
        unit: 'string'
      }
    };
    describe('#renewCertificateUsingPOST - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.renewCertificateUsingPOST(certificateEnrollmentAPIRenewCertificateUsingPOSTBodyParam, certificateEnrollmentAPISerialNumber, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-digicert_pki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CertificateEnrollmentAPI', 'renewCertificateUsingPOST', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCertificateUsingGET - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getCertificateUsingGET(certificateEnrollmentAPISerialNumber, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-digicert_pki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CertificateEnrollmentAPI', 'getCertificateUsingGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#recoverKeyUsingGET - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.recoverKeyUsingGET(certificateEnrollmentAPISerialNumber, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-digicert_pki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CertificateEnrollmentAPI', 'recoverKeyUsingGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const certificateEnrollmentAPIRevokeCertificateUsingPUTBodyParam = {
      revocation_reason: 'string'
    };
    describe('#revokeCertificateUsingPUT - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.revokeCertificateUsingPUT(certificateEnrollmentAPIRevokeCertificateUsingPUTBodyParam, certificateEnrollmentAPISerialNumber, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-digicert_pki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CertificateEnrollmentAPI', 'revokeCertificateUsingPUT', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const userManagementAPICreatePasscodeUsingPOSTBodyParam = {
      attributes: {
        common_name: 'string',
        content_type: 'string',
        counter_signature: 'string',
        country: 'string',
        custom_attributes: {},
        dn_qualifier: 'string',
        domain_component: [
          {
            id: 'string',
            mandatory: false,
            type: 'string',
            value: 'string'
          }
        ],
        domain_name: 'string',
        email: 'string',
        given_name: 'string',
        ip_address: 'string',
        job_title: 'string',
        locality: 'string',
        message_digest: 'string',
        organization_name: 'string',
        organization_unit: [
          {
            id: 'string',
            mandatory: false,
            type: 'string',
            value: 'string'
          }
        ],
        postal_code: 'string',
        pseudonym: 'string',
        san: {
          custom_attributes: {},
          directory_name: 'string',
          dns_name: [
            {
              id: 'string',
              mandatory: false,
              type: 'string',
              value: 'string'
            }
          ],
          ip_address: [
            {
              id: 'string',
              mandatory: false,
              type: 'string',
              value: 'string'
            }
          ],
          other_name: [
            {
              id: 'string',
              mandatory: true,
              type: 'string',
              value: 'string'
            }
          ],
          registered_id: [
            {
              id: 'string',
              mandatory: false,
              type: 'string',
              value: 'string'
            }
          ],
          rfc822_name: [
            {
              id: 'string',
              mandatory: false,
              type: 'string',
              value: 'string'
            }
          ],
          user_principal_name: [
            {
              id: 'string',
              mandatory: true,
              type: 'string',
              value: 'string'
            }
          ]
        },
        serial_number: 'string',
        signing_time: 'string',
        state: 'string',
        street_address: [
          {
            id: 'string',
            mandatory: true,
            type: 'string',
            value: 'string'
          }
        ],
        surname: 'string',
        unique_identifier: 'string',
        unstructured_address: 'string',
        unstructured_name: 'string',
        user_id: 'string'
      },
      profile: {
        id: 'string',
        name: 'string'
      },
      seat: {
        email: 'string',
        seat_id: 'string',
        seat_name: 'string'
      }
    };
    describe('#createPasscodeUsingPOST - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createPasscodeUsingPOST(userManagementAPICreatePasscodeUsingPOSTBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-digicert_pki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UserManagementAPI', 'createPasscodeUsingPOST', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const userManagementAPIEnrollCode = 'fakedata';
    const userManagementAPIResetPasscodeUsingPUTBodyParam = {
      seat: {
        email: 'string',
        seat_id: 'string',
        seat_name: 'string'
      }
    };
    describe('#resetPasscodeUsingPUT - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.resetPasscodeUsingPUT(userManagementAPIEnrollCode, userManagementAPIResetPasscodeUsingPUTBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-digicert_pki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UserManagementAPI', 'resetPasscodeUsingPUT', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const userManagementAPISeatId = 'fakedata';
    describe('#getEnrollmentUsingGET - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getEnrollmentUsingGET(userManagementAPIEnrollCode, userManagementAPISeatId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-digicert_pki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UserManagementAPI', 'getEnrollmentUsingGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const enrollStatusAPISeatId = 'fakedata';
    describe('#enrollStatusUsingGET - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.enrollStatusUsingGET(null, enrollStatusAPISeatId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('EnrollStatusAPI', 'enrollStatusUsingGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#helloUsingGET - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.helloUsingGET((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-digicert_pki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('HelloAPI', 'helloUsingGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllProfilesUsingGET - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllProfilesUsingGET((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CertificateProfileAPI', 'getAllProfilesUsingGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const certificateProfileAPIProfileId = 'fakedata';
    describe('#getProfileUsingGET - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getProfileUsingGET(certificateProfileAPIProfileId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-digicert_pki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CertificateProfileAPI', 'getProfileUsingGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const pROFILEAPICreateProfileUsingPOSTBodyParam = {
      bctOid: 'string',
      caId: 'string',
      duplicateCertPolicy: true,
      extendedKeyUsages: {
        critical: false,
        extended_key_usages: [
          'string'
        ]
      },
      keyEscrowPolicy: {
        dual_admin_recovery: true,
        key_escrow_deployment_mode: 'CLOUD',
        key_escrow_enabled: false
      },
      keySize: 'string',
      keyUsages: {
        critical: true,
        key_usages: [
          'string'
        ]
      },
      overrideCertValidityViaApi: true,
      profileName: 'string',
      profileOid: 'string',
      renewalPeriodDays: 2,
      san: {
        attributes: [
          {
            id: 'string',
            mandatory: false,
            type: 'string',
            value: 'string'
          }
        ],
        critical: false
      },
      signatureAlgorithm: 'string',
      subject: {
        attributes: [
          {
            id: 'string',
            mandatory: true,
            multiple: false,
            sources: [
              {
                mandatory: false,
                value: {}
              }
            ],
            type: 'string',
            value: 'string'
          }
        ]
      },
      validity: {
        duration: 9,
        unit: 'string'
      }
    };
    describe('#createProfileUsingPOST - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createProfileUsingPOST(pROFILEAPICreateProfileUsingPOSTBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-digicert_pki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PROFILEAPI', 'createProfileUsingPOST', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#testCreateUsingPOST - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.testCreateUsingPOST((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-digicert_pki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PROFILEAPI', 'testCreateUsingPOST', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const pROFILEAPIActivateProfileUsingPUTBodyParam = {
      bctOid: 'string',
      caId: 'string',
      duplicateCertPolicy: false,
      extendedKeyUsages: {
        critical: false,
        extended_key_usages: [
          'string'
        ]
      },
      keyEscrowPolicy: {
        dual_admin_recovery: true,
        key_escrow_deployment_mode: 'LOCAL',
        key_escrow_enabled: true
      },
      keySize: 'string',
      keyUsages: {
        critical: false,
        key_usages: [
          'string'
        ]
      },
      overrideCertValidityViaApi: true,
      profileName: 'string',
      profileOid: 'string',
      renewalPeriodDays: 5,
      san: {
        attributes: [
          {
            id: 'string',
            mandatory: false,
            type: 'string',
            value: 'string'
          }
        ],
        critical: true
      },
      signatureAlgorithm: 'string',
      subject: {
        attributes: [
          {
            id: 'string',
            mandatory: true,
            multiple: false,
            sources: [
              {
                mandatory: true,
                value: {}
              }
            ],
            type: 'string',
            value: 'string'
          }
        ]
      },
      validity: {
        duration: 9,
        unit: 'string'
      }
    };
    describe('#activateProfileUsingPUT - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.activateProfileUsingPUT(pROFILEAPIActivateProfileUsingPUTBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-digicert_pki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PROFILEAPI', 'activateProfileUsingPUT', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const pROFILEAPIDeleteProfileUsingPUTBodyParam = {
      bctOid: 'string',
      caId: 'string',
      duplicateCertPolicy: false,
      extendedKeyUsages: {
        critical: true,
        extended_key_usages: [
          'string'
        ]
      },
      keyEscrowPolicy: {
        dual_admin_recovery: true,
        key_escrow_deployment_mode: 'CLOUD',
        key_escrow_enabled: true
      },
      keySize: 'string',
      keyUsages: {
        critical: false,
        key_usages: [
          'string'
        ]
      },
      overrideCertValidityViaApi: false,
      profileName: 'string',
      profileOid: 'string',
      renewalPeriodDays: 8,
      san: {
        attributes: [
          {
            id: 'string',
            mandatory: false,
            type: 'string',
            value: 'string'
          }
        ],
        critical: false
      },
      signatureAlgorithm: 'string',
      subject: {
        attributes: [
          {
            id: 'string',
            mandatory: true,
            multiple: true,
            sources: [
              {
                mandatory: false,
                value: {}
              }
            ],
            type: 'string',
            value: 'string'
          }
        ]
      },
      validity: {
        duration: 6,
        unit: 'string'
      }
    };
    describe('#deleteProfileUsingPUT - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteProfileUsingPUT(pROFILEAPIDeleteProfileUsingPUTBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-digicert_pki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PROFILEAPI', 'deleteProfileUsingPUT', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const pROFILEAPIUpdateProfileUsingPUTBodyParam = {
      bctOid: 'string',
      caId: 'string',
      duplicateCertPolicy: true,
      extendedKeyUsages: {
        critical: true,
        extended_key_usages: [
          'string'
        ]
      },
      keyEscrowPolicy: {
        dual_admin_recovery: true,
        key_escrow_deployment_mode: 'CLOUD',
        key_escrow_enabled: false
      },
      keySize: 'string',
      keyUsages: {
        critical: false,
        key_usages: [
          'string'
        ]
      },
      overrideCertValidityViaApi: true,
      profileName: 'string',
      profileOid: 'string',
      renewalPeriodDays: 3,
      san: {
        attributes: [
          {
            id: 'string',
            mandatory: true,
            type: 'string',
            value: 'string'
          }
        ],
        critical: true
      },
      signatureAlgorithm: 'string',
      subject: {
        attributes: [
          {
            id: 'string',
            mandatory: true,
            multiple: false,
            sources: [
              {
                mandatory: true,
                value: {}
              }
            ],
            type: 'string',
            value: 'string'
          }
        ]
      },
      validity: {
        duration: 9,
        unit: 'string'
      }
    };
    describe('#updateProfileUsingPUT - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateProfileUsingPUT(pROFILEAPIUpdateProfileUsingPUTBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-digicert_pki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PROFILEAPI', 'updateProfileUsingPUT', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const pROFILEAPISuspendProfileUsingPUTBodyParam = {
      bctOid: 'string',
      caId: 'string',
      duplicateCertPolicy: false,
      extendedKeyUsages: {
        critical: true,
        extended_key_usages: [
          'string'
        ]
      },
      keyEscrowPolicy: {
        dual_admin_recovery: false,
        key_escrow_deployment_mode: 'LOCAL',
        key_escrow_enabled: false
      },
      keySize: 'string',
      keyUsages: {
        critical: false,
        key_usages: [
          'string'
        ]
      },
      overrideCertValidityViaApi: false,
      profileName: 'string',
      profileOid: 'string',
      renewalPeriodDays: 7,
      san: {
        attributes: [
          {
            id: 'string',
            mandatory: true,
            type: 'string',
            value: 'string'
          }
        ],
        critical: false
      },
      signatureAlgorithm: 'string',
      subject: {
        attributes: [
          {
            id: 'string',
            mandatory: false,
            multiple: false,
            sources: [
              {
                mandatory: true,
                value: {}
              }
            ],
            type: 'string',
            value: 'string'
          }
        ]
      },
      validity: {
        duration: 4,
        unit: 'string'
      }
    };
    describe('#suspendProfileUsingPUT - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.suspendProfileUsingPUT(pROFILEAPISuspendProfileUsingPUTBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-digicert_pki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PROFILEAPI', 'suspendProfileUsingPUT', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const searchCertificateAPISearchCertUsingPOSTBodyParam = {
      common_name: 'string',
      email: 'string',
      issuing_ca: 'string',
      profile_id: 'string',
      seat_id: 'string',
      serial_number: 'string',
      start_index: 3,
      status: 'string',
      valid_from: 'string',
      valid_to: 'string'
    };
    describe('#searchCertUsingPOST - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.searchCertUsingPOST(searchCertificateAPISearchCertUsingPOSTBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-digicert_pki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SearchCertificateAPI', 'searchCertUsingPOST', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const seatManagementAPICreateSeatUsingPOSTBodyParam = {
      email: 'string',
      phone: 'string',
      seat_id: 'string',
      seat_name: 'string'
    };
    describe('#createSeatUsingPOST - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createSeatUsingPOST(seatManagementAPICreateSeatUsingPOSTBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-digicert_pki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SeatManagementAPI', 'createSeatUsingPOST', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const seatManagementAPISeatId = 'fakedata';
    const seatManagementAPIUpdateSeatUsingPUTBodyParam = {
      email: 'string',
      phone: 'string',
      seat_name: 'string'
    };
    describe('#updateSeatUsingPUT - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateSeatUsingPUT(seatManagementAPISeatId, seatManagementAPIUpdateSeatUsingPUTBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-digicert_pki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SeatManagementAPI', 'updateSeatUsingPUT', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSeatUsingGET - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getSeatUsingGET(seatManagementAPISeatId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-digicert_pki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SeatManagementAPI', 'getSeatUsingGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#unRevokeCertificateUsingDELETE - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.unRevokeCertificateUsingDELETE(certificateEnrollmentAPISerialNumber, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-digicert_pki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CertificateEnrollmentAPI', 'unRevokeCertificateUsingDELETE', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const userManagementAPIDeleteEnrollmentUsingDELETEBodyParam = {
      seat: {
        email: 'string',
        seat_id: 'string',
        seat_name: 'string'
      }
    };
    describe('#deleteEnrollmentUsingDELETE - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteEnrollmentUsingDELETE(userManagementAPIDeleteEnrollmentUsingDELETEBodyParam, userManagementAPIEnrollCode, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-digicert_pki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UserManagementAPI', 'deleteEnrollmentUsingDELETE', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSeatUsingDELETE - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteSeatUsingDELETE(seatManagementAPISeatId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-digicert_pki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SeatManagementAPI', 'deleteSeatUsingDELETE', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listCertificates - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.listCertificates(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-digicert_pki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Certificates', 'listCertificates', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getExampleCertificateRequestCertificateID - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getExampleCertificateRequestCertificateID('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-digicert_pki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Certificates', 'getExampleCertificateRequestCertificateID', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const certificatesRenewCertificateBodyParam = {};
    describe('#renewCertificate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.renewCertificate('fakedata', certificatesRenewCertificateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Certificates', 'renewCertificate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const certificatesRenewBySerialBodyParam = {};
    describe('#renewBySerial - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.renewBySerial('fakedata', certificatesRenewBySerialBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Certificates', 'renewBySerial', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const certificatesRevokeCertificateBodyParam = {
      reason: null
    };
    describe('#revokeCertificate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.revokeCertificate('fakedata', certificatesRevokeCertificateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-digicert_pki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Certificates', 'revokeCertificate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const certificatesUpdateNoteBodyParam = {
      note: 'Example certificate note'
    };
    describe('#updateNote - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateNote('fakedata', certificatesUpdateNoteBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-digicert_pki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Certificates', 'updateNote', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const certificatesRevokeCertificateBySerialNumberBodyParam = {
      reason: null
    };
    describe('#revokeCertificateBySerialNumber - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.revokeCertificateBySerialNumber('fakedata', certificatesRevokeCertificateBySerialNumberBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-digicert_pki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Certificates', 'revokeCertificateBySerialNumber', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#downloadCertificate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.downloadCertificate('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-digicert_pki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Certificates', 'downloadCertificate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#downloadCertificateBySerialNumber - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.downloadCertificateBySerialNumber('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-digicert_pki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Certificates', 'downloadCertificateBySerialNumber', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#importCertificate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.importCertificate('fakedata', null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-digicert_pki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Certificates', 'importCertificate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getImportDetails - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getImportDetails('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-digicert_pki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Certificates', 'getImportDetails', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCertificateStatus - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getCertificateStatus('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-digicert_pki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Certificates', 'getCertificateStatus', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCertificateStatusById - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getCertificateStatusById('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-digicert_pki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Certificates', 'getCertificateStatusById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listCertificatesByCertificateValue - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.listCertificatesByCertificateValue('fakedata', null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-digicert_pki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Certificates', 'listCertificatesByCertificateValue', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const certificatesDownloadApprovedCertificateBodyParam = {
      response_with_certificate_only: false,
      split_certificate_response: true,
      include_certificate_chain: true
    };
    describe('#downloadApprovedCertificate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.downloadApprovedCertificate('fakedata', certificatesDownloadApprovedCertificateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('1a4e7b7b-03cf-40ff-a93c-41cc0edf220c', data.response.certificate_request_id);
                assert.equal('AUTO_APPROVED', data.response.status);
                assert.equal('SUCCESS', data.response.result);
                assert.equal('df832ffd-7d5c-4277-99dc-99a1e2b20fc9', data.response.certificate_id);
                assert.equal('-----BEGIN CERTIFICATE-----\n...\n-----END CERTIFICATE-----\n-----BEGIN CERTIFICATE-----\n...\n-----END CERTIFICATE-----\n-----BEGIN CERTIFICATE-----\n...\n-----END CERTIFICATE-----\n', data.response.pem);
                assert.equal('MII...', data.response.ica);
                assert.equal(true, Array.isArray(data.response.chain));
                assert.equal('<private_key>', data.response.private_key);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Certificates', 'downloadApprovedCertificate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listAuthenticationCACertificates - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.listAuthenticationCACertificates(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-digicert_pki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AuthenticationCACertificates', 'listAuthenticationCACertificates', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listAuthenticationCACertificatesByAccount - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.listAuthenticationCACertificatesByAccount('fakedata', null, null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-digicert_pki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AuthenticationCACertificates', 'listAuthenticationCACertificatesByAccount', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#uploadAuthenticationCACertificate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.uploadAuthenticationCACertificate('fakedata', null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-digicert_pki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AuthenticationCACertificates', 'uploadAuthenticationCACertificate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAuthenticationCACertificate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getAuthenticationCACertificate('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-digicert_pki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AuthenticationCACertificates', 'getAuthenticationCACertificate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAuthenticationCACertificate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteAuthenticationCACertificate('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-digicert_pki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AuthenticationCACertificates', 'deleteAuthenticationCACertificate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const authenticationCATemplatesCreateAuthenticationCATemplateBodyParam = {
      enrollment_profile_id: 'IOT_502ac648-c826-4e71-991e-5629a23850c1',
      ica_id: '64a17c67-44b9-4ac1-b5a0-4f3fcb2e851b',
      ica_body: 'string',
      name: 'Example authentication CA template',
      can_be_used_for_any_certificate: true
    };
    describe('#createAuthenticationCATemplate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createAuthenticationCATemplate(authenticationCATemplatesCreateAuthenticationCATemplateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('55f6e0e5-8cbf-4f7b-a623-f5b9689a0dc2', data.response.id);
                assert.equal('Example authentication CA template', data.response.name);
                assert.equal(true, Array.isArray(data.response.certificate_attributes));
                assert.equal(true, Array.isArray(data.response.registered_values));
                assert.equal('IOT_58b96f06-bf12-49a4-939e-7247f147ec3e', data.response.enrollment_profile_id);
                assert.equal('Example enrollment profile', data.response.enrollment_profile_name);
                assert.equal('IoT Example ICA', data.response.ica_common_name);
                assert.equal('-----BEGIN CERTIFICATE-----\n...\n-----END CERTIFICATE-----\n', data.response.ica_body);
                assert.equal('IoT Example Organization', data.response.organization);
                assert.equal('IoT Example Root', data.response.parent_ca);
                assert.equal('1.2.840.10045.4.3.2', data.response.signature_algorithm);
                assert.equal('EC prime256v1', data.response.key_type);
                assert.equal(false, data.response.can_be_used_for_any_certificate);
                assert.equal('certificate_body_and_attributes', data.response.certificate_authentication_type);
                assert.equal(20, data.response.usage_limit);
                assert.equal('2022-07-21T00:00:00Z', data.response.start_date);
                assert.equal('2022-07-31T00:00:00Z', data.response.end_date);
                assert.equal('2049-01-01', data.response.expiry_date);
                assert.equal('ACTIVE', data.response.status);
                assert.equal('2022-07-21T10:09:13Z', data.response.created_at);
                assert.equal(false, data.response.force_passcode_for_auth_cert);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AuthenticationCATemplates', 'createAuthenticationCATemplate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listAuthenticationCATemplate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.listAuthenticationCATemplate(null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-digicert_pki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AuthenticationCATemplates', 'listAuthenticationCATemplate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const authenticationCATemplatesUpdateAuthenticationCATemplateBodyParam = {
      name: 'Example authentication CA template',
      can_be_used_for_any_certificate: true,
      certificate_authentication_type: null,
      certificate_attributes: [
        'subject.email'
      ],
      usage_limit: 9,
      start_date: '2021-01-01T00:00:00Z',
      end_date: '2021-01-01T00:00:00Z',
      registered_values: [
        {
          certificate_field: null,
          value: 'example'
        }
      ],
      force_passcode_for_auth_cert: true
    };
    describe('#updateAuthenticationCATemplate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateAuthenticationCATemplate('fakedata', authenticationCATemplatesUpdateAuthenticationCATemplateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AuthenticationCATemplates', 'updateAuthenticationCATemplate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAuthenticationCATemplate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAuthenticationCATemplate('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('55f6e0e5-8cbf-4f7b-a623-f5b9689a0dc2', data.response.id);
                assert.equal('Example authentication CA template', data.response.name);
                assert.equal(true, Array.isArray(data.response.certificate_attributes));
                assert.equal(true, Array.isArray(data.response.registered_values));
                assert.equal('IOT_58b96f06-bf12-49a4-939e-7247f147ec3e', data.response.enrollment_profile_id);
                assert.equal('Example enrollment profile', data.response.enrollment_profile_name);
                assert.equal('IoT Example ICA', data.response.ica_common_name);
                assert.equal('-----BEGIN CERTIFICATE-----\n...\n-----END CERTIFICATE-----\n', data.response.ica_body);
                assert.equal('IoT Example Organization', data.response.organization);
                assert.equal('IoT Example Root', data.response.parent_ca);
                assert.equal('1.2.840.10045.4.3.2', data.response.signature_algorithm);
                assert.equal('EC prime256v1', data.response.key_type);
                assert.equal(false, data.response.can_be_used_for_any_certificate);
                assert.equal('certificate_body_and_attributes', data.response.certificate_authentication_type);
                assert.equal(20, data.response.usage_limit);
                assert.equal('2022-07-21T00:00:00Z', data.response.start_date);
                assert.equal('2022-07-31T00:00:00Z', data.response.end_date);
                assert.equal('2049-01-01', data.response.expiry_date);
                assert.equal('ACTIVE', data.response.status);
                assert.equal('2022-07-21T10:09:13Z', data.response.created_at);
                assert.equal(false, data.response.force_passcode_for_auth_cert);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AuthenticationCATemplates', 'getAuthenticationCATemplate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#disableAuthenticationCATemplate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.disableAuthenticationCATemplate('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-digicert_pki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AuthenticationCATemplates', 'disableAuthenticationCATemplate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAuthenticationCATemplate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteAuthenticationCATemplate('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-digicert_pki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AuthenticationCATemplates', 'deleteAuthenticationCATemplate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#restoreAuthenticationCATemplate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.restoreAuthenticationCATemplate('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-digicert_pki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AuthenticationCATemplates', 'restoreAuthenticationCATemplate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#enableAuthenticationCATemplate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.enableAuthenticationCATemplate('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-digicert_pki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AuthenticationCATemplates', 'enableAuthenticationCATemplate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listAssignableAuthenticationCACertificates - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.listAssignableAuthenticationCACertificates('fakedata', null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-digicert_pki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AuthenticationCATemplates', 'listAssignableAuthenticationCACertificates', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const authenticationCertificatesAddAuthenticationCertificateBodyParam = {
      template_id: '<template_id>',
      pem: '<pem>',
      name: 'Example Authentication Certificate',
      start_date: '2021-05-20T00:00:00Z',
      end_date: '2022-05-20T00:00:00Z',
      registered_values: [
        {
          certificate_field: 'subject.common_name',
          matcher: 'equals',
          value: 'common name registered value'
        },
        {
          certificate_field: 'subject.organization_unit',
          matcher: 'equals',
          value: [
            'Unit 1',
            'Unit 2'
          ]
        }
      ]
    };
    describe('#addAuthenticationCertificate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addAuthenticationCertificate(authenticationCertificatesAddAuthenticationCertificateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('<template_id>', data.response.template_id);
                assert.equal('<authentication_certificate_id>', data.response.id);
                assert.equal('Example Name', data.response.name);
                assert.equal('object', typeof data.response.enrollment_profile);
                assert.equal('2021-05-20T00:00:00Z', data.response.start_date);
                assert.equal('2022-05-20T00:00:00Z', data.response.end_date);
                assert.equal('0', data.response.usage_limit);
                assert.equal(true, Array.isArray(data.response.registered_values));
                assert.equal('0', data.response.number_of_usage);
                assert.equal('ACTIVE', data.response.status);
                assert.equal('2021-05-13T21:05:43.273689Z', data.response.created_at);
                assert.equal('Issuing CA Name', data.response.certificate_issuer_common_name);
                assert.equal(false, data.response.use_passcode);
                assert.equal('string', data.response.generation_option);
                assert.equal('ExamplePasscode123', data.response.passcode);
                assert.equal(20, data.response.passcode_length);
                assert.equal('object', typeof data.response.certificate);
                assert.equal('<pem>', data.response.pem);
                assert.equal('2022-04-30T17:38:58Z', data.response.certificate_expires_on);
                assert.equal('<certificate_serial_number>', data.response.certificate_serial_number);
                assert.equal('ISSUED', data.response.certificate_status);
                assert.equal('<certificate_thumbprint>', data.response.certificate_thumbprint);
                assert.equal('Example Organization', data.response.certificate_organization);
                assert.equal(true, Array.isArray(data.response.certificate_organization_units));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AuthenticationCertificates', 'addAuthenticationCertificate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listAuthenticationCertificates - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listAuthenticationCertificates(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(20, data.response.limit);
                assert.equal(0, data.response.offset);
                assert.equal(true, data.response.pagination);
                assert.equal(false, data.response.next);
                assert.equal(1, data.response.total);
                assert.equal(true, Array.isArray(data.response.records));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AuthenticationCertificates', 'listAuthenticationCertificates', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAuthenticationCertificateDetails - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAuthenticationCertificateDetails('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('<template_id>', data.response.template_id);
                assert.equal('<authentication_certificate_id>', data.response.id);
                assert.equal('Example Name', data.response.name);
                assert.equal('object', typeof data.response.enrollment_profile);
                assert.equal('2021-05-20T00:00:00Z', data.response.start_date);
                assert.equal('2022-05-20T00:00:00Z', data.response.end_date);
                assert.equal('0', data.response.usage_limit);
                assert.equal(true, Array.isArray(data.response.registered_values));
                assert.equal('0', data.response.number_of_usage);
                assert.equal('ACTIVE', data.response.status);
                assert.equal('2021-05-13T21:05:43.273689Z', data.response.created_at);
                assert.equal('Issuing CA Name', data.response.certificate_issuer_common_name);
                assert.equal(false, data.response.use_passcode);
                assert.equal('string', data.response.generation_option);
                assert.equal('ExamplePasscode123', data.response.passcode);
                assert.equal(20, data.response.passcode_length);
                assert.equal('object', typeof data.response.certificate);
                assert.equal('<pem>', data.response.pem);
                assert.equal('2022-04-30T17:38:58Z', data.response.certificate_expires_on);
                assert.equal('<certificate_serial_number>', data.response.certificate_serial_number);
                assert.equal('ISSUED', data.response.certificate_status);
                assert.equal('<certificate_thumbprint>', data.response.certificate_thumbprint);
                assert.equal('Example Organization', data.response.certificate_organization);
                assert.equal(true, Array.isArray(data.response.certificate_organization_units));
                assert.equal(true, Array.isArray(data.response.certificate_attributes));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AuthenticationCertificates', 'getAuthenticationCertificateDetails', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const authenticationCertificatesUpdateAuthenticationCertificateBodyParam = {
      name: 'Updated name',
      start_date: '2021-12-05T00:00:00Z',
      end_date: '2022-12-10T00:00:00Z',
      usage_limit: '10',
      registered_values: [
        {
          certificate_field: 'subject.common_name',
          value: 'Updated common name value'
        }
      ]
    };
    describe('#updateAuthenticationCertificate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateAuthenticationCertificate('fakedata', authenticationCertificatesUpdateAuthenticationCertificateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AuthenticationCertificates', 'updateAuthenticationCertificate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#disableAuthenticationCertificate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.disableAuthenticationCertificate('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-digicert_pki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AuthenticationCertificates', 'disableAuthenticationCertificate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#enableAuthenticationCertificate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.enableAuthenticationCertificate('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-digicert_pki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AuthenticationCertificates', 'enableAuthenticationCertificate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAuthenticationCertificate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteAuthenticationCertificate('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-digicert_pki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AuthenticationCertificates', 'deleteAuthenticationCertificate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#restoreAuthenticationCertificate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.restoreAuthenticationCertificate('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-digicert_pki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AuthenticationCertificates', 'restoreAuthenticationCertificate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listCertificateProfiles - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listCertificateProfiles(null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(20, data.response.limit);
                assert.equal(0, data.response.offset);
                assert.equal(true, data.response.pagination);
                assert.equal(false, data.response.next);
                assert.equal(1, data.response.total);
                assert.equal(true, Array.isArray(data.response.records));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CertificateProfiles', 'listCertificateProfiles', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const certificateProfilesCreateCertificateProfileBodyParam = {
      account_id: '490e13ab-0a2f-4a9d-b52b-6942ea00588f',
      certificate_template_id: 'c4e3cf2a-8c4d-4145-be5f-a81140cfbf12',
      name: 'Example certificate profile',
      body: [
        {}
      ]
    };
    describe('#createCertificateProfile - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createCertificateProfile(certificateProfilesCreateCertificateProfileBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-digicert_pki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CertificateProfiles', 'createCertificateProfile', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCertificateProfile - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getCertificateProfile('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-digicert_pki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CertificateProfiles', 'getCertificateProfile', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const certificateProfilesUpdateCertificateProfileBodyParam = {
      name: 'Example certificate profile',
      body: [
        {}
      ]
    };
    describe('#updateCertificateProfile - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateCertificateProfile('fakedata', certificateProfilesUpdateCertificateProfileBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-digicert_pki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CertificateProfiles', 'updateCertificateProfile', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#disableCertificateProfile - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.disableCertificateProfile('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-digicert_pki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CertificateProfiles', 'disableCertificateProfile', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#enableCertificateProfile - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.enableCertificateProfile('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-digicert_pki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CertificateProfiles', 'enableCertificateProfile', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteCertificateProfile - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteCertificateProfile('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-digicert_pki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CertificateProfiles', 'deleteCertificateProfile', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#restoreCertificateProfile - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.restoreCertificateProfile('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-digicert_pki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CertificateProfiles', 'restoreCertificateProfile', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const certificateProfilesAssignCertificateProfileDivisionsBodyParam = [
      'string'
    ];
    describe('#assignCertificateProfileDivisions - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.assignCertificateProfileDivisions('fakedata', certificateProfilesAssignCertificateProfileDivisionsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-digicert_pki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CertificateProfiles', 'assignCertificateProfileDivisions', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const certificateProfilesUnassignCertificateProfileDivisionsBodyParam = [
      'string'
    ];
    describe('#unassignCertificateProfileDivisions - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.unassignCertificateProfileDivisions('fakedata', certificateProfilesUnassignCertificateProfileDivisionsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-digicert_pki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CertificateProfiles', 'unassignCertificateProfileDivisions', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRequestStatus - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRequestStatus('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('cad89ac2-fe65-4b11-bf28-3b84a9c59bdd', data.response.certificate_request_id);
                assert.equal('PENDING_APPROVAL', data.response.status);
                assert.equal('51967282-23d3-429b-b4c6-a52e2c2dd389', data.response.certificate_id);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CertificateRequests', 'getRequestStatus', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listCertificateTemplates - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.listCertificateTemplates(null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-digicert_pki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CertificateTemplates', 'listCertificateTemplates', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const certificateTemplatesCreateCertificateTemplateBodyParam = {
      name: 'Example template',
      body: {},
      accounts: [
        'string'
      ]
    };
    describe('#createCertificateTemplate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createCertificateTemplate(certificateTemplatesCreateCertificateTemplateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-digicert_pki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CertificateTemplates', 'createCertificateTemplate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCertificateTemplate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getCertificateTemplate('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-digicert_pki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CertificateTemplates', 'getCertificateTemplate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const certificateTemplatesUpdateCertificateTemplateBodyParam = {
      name: 'Example template',
      body: {},
      accounts: [
        'string'
      ]
    };
    describe('#updateCertificateTemplate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateCertificateTemplate('fakedata', certificateTemplatesUpdateCertificateTemplateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-digicert_pki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CertificateTemplates', 'updateCertificateTemplate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const certificateTemplatesClonecertificatetemplateBodyParam = {
      name: 'Example template'
    };
    describe('#clonecertificatetemplate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.clonecertificatetemplate('fakedata', certificateTemplatesClonecertificatetemplateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-digicert_pki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CertificateTemplates', 'clonecertificatetemplate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#disableCertificateTemplate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.disableCertificateTemplate('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-digicert_pki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CertificateTemplates', 'disableCertificateTemplate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#enableCertificateTemplate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.enableCertificateTemplate('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-digicert_pki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CertificateTemplates', 'enableCertificateTemplate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteCertificateTemplate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteCertificateTemplate('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-digicert_pki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CertificateTemplates', 'deleteCertificateTemplate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#restoreCertificateTemplate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.restoreCertificateTemplate('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-digicert_pki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CertificateTemplates', 'restoreCertificateTemplate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const devicesCreateDeviceBodyParam = {
      division_id: 'fd5faa1e-623a-47a9-831b-b9397c33395f',
      device_identifier: 'Device name',
      device_profile_id: 'a81a431a-be7b-4f2e-8afd-b2778491511f'
    };
    describe('#createDevice - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createDevice(devicesCreateDeviceBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-digicert_pki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Devices', 'createDevice', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getDeviceList(null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-digicert_pki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Devices', 'getDeviceList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceDetails - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getDeviceDetails('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-digicert_pki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Devices', 'getDeviceDetails', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const devicesUpdateDeviceBodyParam = {
      device_identifier: 'Device name',
      fields: [
        {
          id: '123c456e-789c-1e23-4567-ee90f39458fb',
          value: 'Field value'
        }
      ],
      device_api_allow_read: true,
      device_api_allow_write: true,
      device_api_allow_renew_certificate: true
    };
    describe('#updateDevice - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateDevice('fakedata', 'fakedata', devicesUpdateDeviceBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-digicert_pki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Devices', 'updateDevice', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#archiveDevice - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.archiveDevice('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Devices', 'archiveDevice', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#enableDevice - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.enableDevice('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Devices', 'enableDevice', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#disableDevice - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.disableDevice('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Devices', 'disableDevice', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#restoreDevice - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.restoreDevice('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Devices', 'restoreDevice', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const devicesV2CreateDeviceV2BodyParam = {
      division_id: 'fd5faa1e-623a-47a9-831b-b9397c33395f',
      device_identifier: 'Device name',
      device_profile_id: 'a81a431a-be7b-4f2e-8afd-b2778491511f'
    };
    describe('#createDeviceV2 - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createDeviceV2(devicesV2CreateDeviceV2BodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-digicert_pki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DevicesV2', 'createDeviceV2', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceListV2 - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getDeviceListV2(null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-digicert_pki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DevicesV2', 'getDeviceListV2', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceDetailsV2 - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getDeviceDetailsV2('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-digicert_pki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DevicesV2', 'getDeviceDetailsV2', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const devicesV2UpdateDeviceV2BodyParam = {
      device_identifier: 'Device name',
      fields: [
        {
          id: '123c456e-789c-1e23-4567-ee90f39458fb',
          value: 'Field value'
        }
      ],
      device_api_allow_read: true,
      device_api_allow_write: true,
      device_api_allow_renew_certificate: true
    };
    describe('#updateDeviceV2 - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateDeviceV2('fakedata', devicesV2UpdateDeviceV2BodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-digicert_pki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DevicesV2', 'updateDeviceV2', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#archiveDeviceV2 - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.archiveDeviceV2('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DevicesV2', 'archiveDeviceV2', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#enableDeviceV2 - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.enableDeviceV2('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DevicesV2', 'enableDeviceV2', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#disableDeviceV2 - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.disableDeviceV2('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DevicesV2', 'disableDeviceV2', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#restoreDeviceV2 - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.restoreDeviceV2('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DevicesV2', 'restoreDeviceV2', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const devicesV2RenewDeviceCertificateV2BodyParam = {
      csr: 'string'
    };
    describe('#renewDeviceCertificateV2 - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.renewDeviceCertificateV2('fakedata', devicesV2RenewDeviceCertificateV2BodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-digicert_pki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DevicesV2', 'renewDeviceCertificateV2', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listDivisions - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.listDivisions(null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-digicert_pki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Divisions', 'listDivisions', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const divisionsCreateDivisionBodyParam = {
      name: 'Example division name',
      account_id: 'f92834ce-cdea-1584-b154-193bb198345e'
    };
    describe('#createDivision - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createDivision(divisionsCreateDivisionBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-digicert_pki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Divisions', 'createDivision', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDivision - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getDivision('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-digicert_pki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Divisions', 'getDivision', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const divisionsUpdateDivisionBodyParam = {
      name: 'Example division name'
    };
    describe('#updateDivision - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateDivision('fakedata', divisionsUpdateDivisionBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-digicert_pki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Divisions', 'updateDivision', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#disableDivision - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.disableDivision('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-digicert_pki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Divisions', 'disableDivision', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#enableDivision - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.enableDivision('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-digicert_pki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Divisions', 'enableDivision', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDivision - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteDivision('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-digicert_pki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Divisions', 'deleteDivision', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#restoreDivision - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.restoreDivision('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-digicert_pki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Divisions', 'restoreDivision', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const divisionsAssignDivisionUsersBodyParam = [
      'string'
    ];
    describe('#assignDivisionUsers - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.assignDivisionUsers('fakedata', divisionsAssignDivisionUsersBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-digicert_pki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Divisions', 'assignDivisionUsers', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listDivisionUsers - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.listDivisionUsers('fakedata', null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-digicert_pki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Divisions', 'listDivisionUsers', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const divisionsRemoveDivisionUsersBodyParam = [
      'string'
    ];
    describe('#removeDivisionUsers - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.removeDivisionUsers('fakedata', divisionsRemoveDivisionUsersBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-digicert_pki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Divisions', 'removeDivisionUsers', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const divisionsAssignICAstodivisionBodyParam = [
      'string'
    ];
    describe('#assignICAstodivision - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.assignICAstodivision('fakedata', divisionsAssignICAstodivisionBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-digicert_pki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Divisions', 'assignICAstodivision', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const divisionsRemoveICAsfromdivisionBodyParam = [
      'string'
    ];
    describe('#removeICAsfromdivision - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.removeICAsfromdivision('fakedata', divisionsRemoveICAsfromdivisionBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-digicert_pki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Divisions', 'removeICAsfromdivision', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listdivisionICAassignments - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.listdivisionICAassignments('fakedata', null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-digicert_pki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Divisions', 'listdivisionICAassignments', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listEnrollmentProfiles - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.listEnrollmentProfiles(null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-digicert_pki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('EnrollmentProfiles', 'listEnrollmentProfiles', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const enrollmentProfilesCreateEnrollmentProfileBodyParam = {
      enrollment_profile: {
        name: 'Example enrollment profile',
        device_profile_id: '7e873179-aab6-4acb-bfea-7dda35513926',
        enrollment_methods: [
          'API'
        ],
        key_generation_option: 'client_or_server_side',
        key_generation_type: 'RSA_2048',
        key_generation_allow_to_change: true,
        certificate_profile_id: '803b8a1c-065e-4a11-95e4-40b19b76eab9',
        ica_id: 'AAC2E2DD6BEAE2E5EE93954BAE15E34D'
      }
    };
    describe('#createEnrollmentProfile - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createEnrollmentProfile(enrollmentProfilesCreateEnrollmentProfileBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-digicert_pki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('EnrollmentProfiles', 'createEnrollmentProfile', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getExampleCertificateRequestEnrollmentProfileID - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getExampleCertificateRequestEnrollmentProfileID('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-digicert_pki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('EnrollmentProfiles', 'getExampleCertificateRequestEnrollmentProfileID', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getEnrollmentProfile - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getEnrollmentProfile('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('Example name', data.response.name);
                assert.equal(true, Array.isArray(data.response.enrollment_methods));
                assert.equal(true, Array.isArray(data.response.allowed_ip_addresses));
                assert.equal('09:45:00', data.response.allowed_hours_from);
                assert.equal('17:30:00', data.response.allowed_hours_to);
                assert.equal('America/New_York', data.response.timezone);
                assert.equal('2021-06-25', data.response.valid_from);
                assert.equal('2022-06-25', data.response.expires_on);
                assert.equal(true, data.response.key_generation_allow_to_change);
                assert.equal(false, data.response.allow_key_cache);
                assert.equal('string', data.response.key_cache_validity_period);
                assert.equal(false, data.response.require_approval_for_enroll);
                assert.equal(false, data.response.require_approval_for_renew);
                assert.equal(true, Array.isArray(data.response.approvers));
                assert.equal(true, Array.isArray(data.response.notification_email_list));
                assert.equal('IOT_502ac648-c826-4e71-991e-5629a23850c1', data.response.id);
                assert.equal('https://one.digicert.com', data.response.connector_url);
                assert.equal('passcode', data.response.passcode);
                assert.equal('<api_token>', data.response.api_key);
                assert.equal('<auth_cert_pkcs12>', data.response.auth_cert_pkcs12);
                assert.equal('passcode', data.response.auth_cert_password_pkcs12);
                assert.equal('string', data.response.external_enrollment_profile_id);
                assert.equal('object', typeof data.response.device_profile);
                assert.equal('object', typeof data.response.certificate_profile);
                assert.equal('2021-06-09T13:49:54Z', data.response.created_at);
                assert.equal('object', typeof data.response.ica);
                assert.equal(true, Array.isArray(data.response.allowed_signature_algorithms));
                assert.equal(true, data.response.direct_mapping);
                assert.equal('742a7efb-0cf1-499d-abd5-abb87f42aa5f', data.response.account_id);
                assert.equal('<certificate_data>', data.response.ca_body);
                assert.equal('<certificate_data>', data.response.gp_ca_body);
                assert.equal(true, Array.isArray(data.response.ca_chain));
                assert.equal(true, Array.isArray(data.response.gp_ca_chain));
                assert.equal('object', typeof data.response.certificate_template);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('EnrollmentProfiles', 'getEnrollmentProfile', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const enrollmentProfilesUpdateEnrollmentProfileBodyParam = {
      name: 'Updated name',
      ica_id: 'AAC2E2DD6BEAE2E5EE93954BAE15E34D',
      allowed_ip_addresses: [
        '1.1.1.1',
        '2.2.2.2'
      ],
      certificate_profile_id: 'eb1ec97c-d0d6-4d66-ad29-af88fd86ab21',
      timezone: 'America/New_York',
      allowed_hours_from: '09:45:00',
      allowed_hours_to: '17:30:00',
      valid_from: '2021-06-25',
      expires_on: '2022-06-25',
      require_approval_for_enroll: true,
      require_approval_for_renew: true,
      allow_key_cache: true,
      key_cache_validity_period_type: null,
      key_cache_validity_period: '1',
      digest_email_notification_period: null,
      approvers: [
        'string'
      ],
      notification_email_list: [
        'string'
      ]
    };
    describe('#updateEnrollmentProfile - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateEnrollmentProfile('fakedata', enrollmentProfilesUpdateEnrollmentProfileBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('EnrollmentProfiles', 'updateEnrollmentProfile', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listAssignableICAs - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.listAssignableICAs('fakedata', null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-digicert_pki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('EnrollmentProfiles', 'listAssignableICAs', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIssuingICA - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getIssuingICA('fakedata', null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-digicert_pki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('EnrollmentProfiles', 'getIssuingICA', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getEnrollmentProfileSpecification - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getEnrollmentProfileSpecification('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.fields));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('EnrollmentProfiles', 'getEnrollmentProfileSpecification', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getEnrollmentProfileStatus - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getEnrollmentProfileStatus('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-digicert_pki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('EnrollmentProfiles', 'getEnrollmentProfileStatus', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listEnrollmentPasscodes - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.listEnrollmentPasscodes(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-digicert_pki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('EnrollmentProfilePasscodes', 'listEnrollmentPasscodes', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const enrollmentProfilePasscodesCreateEnrollmentPasscodeBodyParam = {
      name: 'Passcode name'
    };
    describe('#createEnrollmentPasscode - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createEnrollmentPasscode('fakedata', enrollmentProfilePasscodesCreateEnrollmentPasscodeBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-digicert_pki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('EnrollmentProfilePasscodes', 'createEnrollmentPasscode', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getEnrollmentPasscodeDetails - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getEnrollmentPasscodeDetails('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-digicert_pki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('EnrollmentProfilePasscodes', 'getEnrollmentPasscodeDetails', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const enrollmentProfilePasscodesUpdateEnrollmentPasscodeBodyParam = {
      name: 'Passcode name'
    };
    describe('#updateEnrollmentPasscode - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateEnrollmentPasscode('fakedata', 'fakedata', enrollmentProfilePasscodesUpdateEnrollmentPasscodeBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-digicert_pki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('EnrollmentProfilePasscodes', 'updateEnrollmentPasscode', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteEnrollmentPasscode - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteEnrollmentPasscode('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-digicert_pki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('EnrollmentProfilePasscodes', 'deleteEnrollmentPasscode', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#restoreEnrollmentPasscode - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.restoreEnrollmentPasscode('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-digicert_pki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('EnrollmentProfilePasscodes', 'restoreEnrollmentPasscode', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#disableEnrollmentPasscode - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.disableEnrollmentPasscode('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-digicert_pki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('EnrollmentProfilePasscodes', 'disableEnrollmentPasscode', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#enableEnrollmentPasscode - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.enableEnrollmentPasscode('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-digicert_pki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('EnrollmentProfilePasscodes', 'enableEnrollmentPasscode', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#regenerateEnrollmentPasscode - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.regenerateEnrollmentPasscode('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-digicert_pki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('EnrollmentProfilePasscodes', 'regenerateEnrollmentPasscode', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const aCMECreateAcmeAuthenticationBodyParam = {
      name: 'Custom name',
      account_id: 'e44757da-bdcc-5442-c568-734cc233281b',
      authentication_pem: '-----BEGIN PUBLIC KEY-----...'
    };
    describe('#createAcmeAuthentication - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createAcmeAuthentication(aCMECreateAcmeAuthenticationBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('f92834ce-cdea-1584-b154-193bb198345e', data.response.id);
                assert.equal('Example ACME Credentials', data.response.name);
                assert.equal('Test', data.response.description);
                assert.equal('1d1ec1ec-6502-11e9-a923-1681be663d3e', data.response.account_id);
                assert.equal('-----BEGIN PUBLIC KEY-----...', data.response.authentication_pem);
                assert.equal('9e00a421268a1e814805cb0571a157d2fdbd9b48a02286c77d4e76110727727d', data.response.thumbprint);
                assert.equal('ACTIVE', data.response.status);
                assert.equal('2021-02-18T21:02:15.285249Z', data.response.created_at);
                assert.equal(3, data.response.rsa_key_size);
                assert.equal('string', data.response.ecdsa_curve);
                assert.equal('string', data.response.key_description);
                assert.equal('example_common_name', data.response.certificate_common_name);
                assert.equal('example_issuing_certificate', data.response.issuer_common_name);
                assert.equal('FS3f8Q7yUZQQO4uLUP8vAyREMCKyUqRr7JoHy1aquKiPYwu42r67CcELWlYRy9oJyzJ09n5oojJzz9t0EhWiNQ', data.response.hmac_key);
                assert.equal(true, data.response.external_account_binding);
                assert.equal(false, data.response.account_bind);
                assert.equal('----BEGIN PUBLIC KEY-----...', data.response.public_key_pem);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ACME', 'createAcmeAuthentication', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listAcmeAuthentication - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listAcmeAuthentication(null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(20, data.response.limit);
                assert.equal(0, data.response.offset);
                assert.equal(true, data.response.pagination);
                assert.equal(false, data.response.next);
                assert.equal(2, data.response.total);
                assert.equal(true, Array.isArray(data.response.records));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ACME', 'listAcmeAuthentication', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAcmeAuthentication - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAcmeAuthentication('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('f92834ce-cdea-1584-b154-193bb198345e', data.response.id);
                assert.equal('Example ACME Credentials', data.response.name);
                assert.equal('Test', data.response.description);
                assert.equal('1d1ec1ec-6502-11e9-a923-1681be663d3e', data.response.account_id);
                assert.equal('-----BEGIN PUBLIC KEY-----...', data.response.authentication_pem);
                assert.equal('9e00a421268a1e814805cb0571a157d2fdbd9b48a02286c77d4e76110727727d', data.response.thumbprint);
                assert.equal('ACTIVE', data.response.status);
                assert.equal('2021-02-18T21:02:15.285249Z', data.response.created_at);
                assert.equal(8, data.response.rsa_key_size);
                assert.equal('string', data.response.ecdsa_curve);
                assert.equal('string', data.response.key_description);
                assert.equal('example_common_name', data.response.certificate_common_name);
                assert.equal('example_issuing_certificate', data.response.issuer_common_name);
                assert.equal('FS3f8Q7yUZQQO4uLUP8vAyREMCKyUqRr7JoHy1aquKiPYwu42r67CcELWlYRy9oJyzJ09n5oojJzz9t0EhWiNQ', data.response.hmac_key);
                assert.equal(true, data.response.external_account_binding);
                assert.equal(false, data.response.account_bind);
                assert.equal('----BEGIN PUBLIC KEY-----...', data.response.public_key_pem);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ACME', 'getAcmeAuthentication', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const aCMEUpdateAcmeAuthenticationBodyParam = {
      name: 'New name'
    };
    describe('#updateAcmeAuthentication - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateAcmeAuthentication('fakedata', aCMEUpdateAcmeAuthenticationBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ACME', 'updateAcmeAuthentication', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#disableAcmeAuthentication - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.disableAcmeAuthentication('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-digicert_pki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ACME', 'disableAcmeAuthentication', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#enableAcmeAuthentication - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.enableAcmeAuthentication('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-digicert_pki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ACME', 'enableAcmeAuthentication', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAcmeAuthentication - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteAcmeAuthentication('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-digicert_pki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ACME', 'deleteAcmeAuthentication', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#restoreAcmeAuthentication - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.restoreAcmeAuthentication('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-digicert_pki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ACME', 'restoreAcmeAuthentication', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#batchCSREnroll - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.batchCSREnroll('fakedata', null, 'fakedata', 'fakedata', [], null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('da342f63-dead-4bae-a065-eb8822948148', data.response.id);
                assert.equal('Example batch job', data.response.name);
                assert.equal(10, data.response.total_items);
                assert.equal(6, data.response.failed_items);
                assert.equal(900, data.response.processing_time);
                assert.equal('string', data.response.passcode);
                assert.equal('Example description.', data.response.description);
                assert.equal(true, Array.isArray(data.response.emails));
                assert.equal(true, Array.isArray(data.response.external_emails));
                assert.equal(false, data.response.data_available);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('BatchCertificateEnrollment', 'batchCSREnroll', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#batchEnrollWithKeyGen - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.batchEnrollWithKeyGen('fakedata', null, 'fakedata', null, null, null, 'fakedata', [], 'fakedata', 'fakedata', null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('da342f63-dead-4bae-a065-eb8822948148', data.response.id);
                assert.equal('Example batch job', data.response.name);
                assert.equal(10, data.response.total_items);
                assert.equal(7, data.response.failed_items);
                assert.equal(900, data.response.processing_time);
                assert.equal('string', data.response.passcode);
                assert.equal('Example description.', data.response.description);
                assert.equal(true, Array.isArray(data.response.emails));
                assert.equal(true, Array.isArray(data.response.external_emails));
                assert.equal(false, data.response.data_available);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('BatchCertificateEnrollment', 'batchEnrollWithKeyGen', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#batchEnrollWithKeyGenMac - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.batchEnrollWithKeyGenMac('fakedata', null, 'fakedata', null, null, null, 'fakedata', 555, 555, [], 'fakedata', 'fakedata', null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('da342f63-dead-4bae-a065-eb8822948148', data.response.id);
                assert.equal('Example batch job', data.response.name);
                assert.equal(10, data.response.total_items);
                assert.equal(1, data.response.failed_items);
                assert.equal(900, data.response.processing_time);
                assert.equal('string', data.response.passcode);
                assert.equal('Example description.', data.response.description);
                assert.equal(true, Array.isArray(data.response.emails));
                assert.equal(true, Array.isArray(data.response.external_emails));
                assert.equal(true, data.response.data_available);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('BatchCertificateEnrollment', 'batchEnrollWithKeyGenMac', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listBatchJobs - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.listBatchJobs(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-digicert_pki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('BatchCertificateEnrollment', 'listBatchJobs', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getBatchJobSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getBatchJobSettings('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('e197ba9d-3af3-4012-9861-d214cce33cd8', data.response.id);
                assert.equal('Example batch job', data.response.name);
                assert.equal(10, data.response.total_items);
                assert.equal(9, data.response.failed_items);
                assert.equal(900, data.response.processing_time);
                assert.equal('string', data.response.passcode);
                assert.equal('Example description.', data.response.description);
                assert.equal(true, Array.isArray(data.response.emails));
                assert.equal(true, Array.isArray(data.response.external_emails));
                assert.equal(false, data.response.data_available);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('BatchCertificateEnrollment', 'getBatchJobSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const batchCertificateEnrollmentUpdateBatchJobSettingsBodyParam = {
      name: 'string',
      emails: [
        'bob@example.com',
        'alice@example.com'
      ],
      description: 'Example description.',
      external_emails: [
        'john@example.com',
        'jane@example.com'
      ],
      passcode_generation_option: null,
      passcode: 'string'
    };
    describe('#updateBatchJobSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateBatchJobSettings('fakedata', batchCertificateEnrollmentUpdateBatchJobSettingsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('BatchCertificateEnrollment', 'updateBatchJobSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#regenerateBatchJobPasscode - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.regenerateBatchJobPasscode('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('BatchCertificateEnrollment', 'regenerateBatchJobPasscode', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#approveJob - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.approveJob('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-digicert_pki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('BatchCertificateEnrollment', 'approveJob', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const batchCertificateEnrollmentRejectJobBodyParam = {
      rejection_reason: 'string'
    };
    describe('#rejectJob - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.rejectJob('fakedata', batchCertificateEnrollmentRejectJobBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-digicert_pki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('BatchCertificateEnrollment', 'rejectJob', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#downloadReport - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.downloadReport('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-digicert_pki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('BatchCertificateEnrollment', 'downloadReport', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#downloadCertificates - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.downloadCertificates('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('04fa5382-00e0-4322-813d-fcce9747d51f', data.response.job_id);
                assert.equal(true, Array.isArray(data.response.chain));
                assert.equal(true, Array.isArray(data.response.certificates));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('BatchCertificateEnrollment', 'downloadCertificates', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDownloadUrls - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteDownloadUrls('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-digicert_pki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('BatchCertificateEnrollment', 'deleteDownloadUrls', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#resendDownloadUrls - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.resendDownloadUrls('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-digicert_pki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('BatchCertificateEnrollment', 'resendDownloadUrls', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#batchEnrollExternal - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.batchEnrollExternal('fakedata', null, 'fakedata', [], null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('a7cdd1ac-8e64-417e-80d7-c5c2d8bab0aa', data.response.id);
                assert.equal('Example batch job', data.response.name);
                assert.equal(10, data.response.total_items);
                assert.equal(3, data.response.failed_items);
                assert.equal(9, data.response.processing_time);
                assert.equal('example.admin', data.response.requested_by_name);
                assert.equal('Example description.', data.response.description);
                assert.equal(true, Array.isArray(data.response.emails));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ExternalBatchCertificateEnrollment', 'batchEnrollExternal', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#downloadMapping - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.downloadMapping('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-digicert_pki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ExternalBatchCertificateEnrollment', 'downloadMapping', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#downloadByJobIdAndUniqueId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.downloadByJobIdAndUniqueId('fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('b5db525b-fff8-44de-abab-fd4e8b68f2c5', data.response.certificate_id);
                assert.equal('string', data.response.certificate);
                assert.equal('string', data.response.private_key);
                assert.equal(true, Array.isArray(data.response.chain));
                assert.equal('<base64_encoded_PKCS12_file>', data.response.pkcs12);
                assert.equal('<passcode>', data.response.pkcs12_password);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ExternalBatchCertificateEnrollment', 'downloadByJobIdAndUniqueId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#downloadByCommonName - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.downloadByCommonName('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ExternalBatchCertificateEnrollment', 'downloadByCommonName', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#downloadByJobIdAndCommonName - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.downloadByJobIdAndCommonName('fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ExternalBatchCertificateEnrollment', 'downloadByJobIdAndCommonName', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#downloadByCertificateValue - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.downloadByCertificateValue('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ExternalBatchCertificateEnrollment', 'downloadByCertificateValue', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#downloadByJobIdAndCertificateValue - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.downloadByJobIdAndCertificateValue('fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ExternalBatchCertificateEnrollment', 'downloadByJobIdAndCertificateValue', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const deviceProfilesCreateDeviceProfileBodyParam = {
      name: 'Example Device Profile',
      division_id: '618b1e36-665e-475a-85cc-e2ebc73633b2',
      fields: [
        {
          name: 'Optional custom field'
        }
      ]
    };
    describe('#createDeviceProfile - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createDeviceProfile(deviceProfilesCreateDeviceProfileBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('dcfe811c-ecf1-4d2e-bf44-462963bc4ade', data.response.id);
                assert.equal('Device profile example', data.response.name);
                assert.equal('An example device profile', data.response.description);
                assert.equal('ACTIVE', data.response.status);
                assert.equal(true, data.response.device_api_allow_read);
                assert.equal(true, data.response.device_api_allow_write);
                assert.equal(true, data.response.device_api_allow_revoke);
                assert.equal(true, data.response.device_api_allow_renew_certificate);
                assert.equal(false, data.response.device_api_allow_enroll_certificate);
                assert.equal(true, Array.isArray(data.response.fields));
                assert.equal('object', typeof data.response.division);
                assert.equal('f92834ce-cdea-1584-b154-193bb198345e', data.response.account_id);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DeviceProfiles', 'createDeviceProfile', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listdeviceprofiles - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listdeviceprofiles(null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(20, data.response.limit);
                assert.equal(0, data.response.offset);
                assert.equal(true, data.response.pagination);
                assert.equal(false, data.response.next);
                assert.equal(2, data.response.total);
                assert.equal(true, Array.isArray(data.response.records));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DeviceProfiles', 'listdeviceprofiles', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceProfile - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDeviceProfile('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('dcfe811c-ecf1-4d2e-bf44-462963bc4ade', data.response.id);
                assert.equal('Device profile example', data.response.name);
                assert.equal('An example device profile', data.response.description);
                assert.equal('ACTIVE', data.response.status);
                assert.equal(true, data.response.device_api_allow_read);
                assert.equal(true, data.response.device_api_allow_write);
                assert.equal(true, data.response.device_api_allow_revoke);
                assert.equal(true, data.response.device_api_allow_renew_certificate);
                assert.equal(false, data.response.device_api_allow_enroll_certificate);
                assert.equal(true, Array.isArray(data.response.fields));
                assert.equal('object', typeof data.response.division);
                assert.equal('f92834ce-cdea-1584-b154-193bb198345e', data.response.account_id);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DeviceProfiles', 'getDeviceProfile', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const deviceProfilesUpdateDeviceProfileBodyParam = {
      name: 'Example Device Profile',
      division_id: '618b1e36-665e-475a-85cc-e2ebc73633b2',
      fields: [
        {
          name: 'Optional custom field'
        }
      ]
    };
    describe('#updateDeviceProfile - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateDeviceProfile('fakedata', deviceProfilesUpdateDeviceProfileBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DeviceProfiles', 'updateDeviceProfile', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDeviceProfile - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteDeviceProfile('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-digicert_pki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DeviceProfiles', 'deleteDeviceProfile', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#restoreDeviceProfile - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.restoreDeviceProfile('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-digicert_pki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DeviceProfiles', 'restoreDeviceProfile', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getICA - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getICA('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-digicert_pki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ICAs', 'getICA', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listICAs - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.listICAs(null, null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-digicert_pki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ICAs', 'listICAs', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getGatewayDetails - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getGatewayDetails('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-digicert_pki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Gateways', 'getGatewayDetails', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const gatewaysUpdateGatewayBodyParam = {
      account_id: '1331f16c-7e4e-4e5e-9187-27f951d46924',
      name: 'Example gateway',
      mac_address: 'gg-hh-ii-kk-ll-mm',
      http_enabled: false,
      http_port: '8080',
      ssl_enabled: true,
      ssl_port: '8443',
      ssl_certificate_pkcs12_file_path: 'config/gateway.p12',
      ssl_certificate_pkcs12_key_store_password: '********rd',
      use_authentication_api_token: true,
      authentication_api_token: '********1234',
      use_authentication_certificate: false,
      authentication_certificate_pkcs12: '<auth_cert_pkcs12>',
      authentication_certificate_pkcs12_password: '********123'
    };
    describe('#updateGateway - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateGateway('fakedata', gatewaysUpdateGatewayBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-digicert_pki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Gateways', 'updateGateway', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listGateways - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.listGateways(null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-digicert_pki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Gateways', 'listGateways', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const gatewaysCreateGatewayBodyParam = {
      account_id: '1331f16c-7e4e-4e5e-9187-27f951d46924',
      name: 'Example gateway',
      mac_address: 'gg-hh-ii-kk-ll-mm',
      http_enabled: false,
      http_port: '8080',
      ssl_enabled: false,
      ssl_port: '8443',
      ssl_certificate_pkcs12_file_path: 'config/gateway.p12',
      ssl_certificate_pkcs12_key_store_password: '********rd',
      use_authentication_api_token: false,
      authentication_api_token: '********1234',
      use_authentication_certificate: false,
      authentication_certificate_pkcs12: '<auth_cert_pkcs12>',
      authentication_certificate_pkcs12_password: '********123'
    };
    describe('#createGateway - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createGateway(gatewaysCreateGatewayBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-digicert_pki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Gateways', 'createGateway', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#enableGateway - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.enableGateway('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-digicert_pki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Gateways', 'enableGateway', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#disableGateway - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.disableGateway('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-digicert_pki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Gateways', 'disableGateway', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteGateway - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteGateway('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-digicert_pki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Gateways', 'deleteGateway', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#restoreGateway - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.restoreGateway('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-digicert_pki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Gateways', 'restoreGateway', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#downloadGatewayConfig - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.downloadGatewayConfig('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-digicert_pki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Gateways', 'downloadGatewayConfig', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listOCSPGroups - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.listOCSPGroups(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-digicert_pki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('OCSPGroups', 'listOCSPGroups', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const oCSPGroupsCreateOCSPGroupBodyParam = {
      name: 'Example OCSP group',
      certificates: [
        '81133e8f-6b0d-4bf1-ac9e-402e72e006b0',
        '6ef9246f-e071-4a8e-a871-0ea4314cde64',
        '5be444dd-ea46-4cac-9e15-823d16012c69'
      ],
      division_id: '3f8f4482-1e31-4840-831e-9416b1dec1f5'
    };
    describe('#createOCSPGroup - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createOCSPGroup(oCSPGroupsCreateOCSPGroupBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-digicert_pki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('OCSPGroups', 'createOCSPGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOCSPGroupDetails - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getOCSPGroupDetails('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-digicert_pki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('OCSPGroups', 'getOCSPGroupDetails', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const oCSPGroupsUpdateOCSPGroupBodyParam = {
      name: 'Updated OCSP group'
    };
    describe('#updateOCSPGroup - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateOCSPGroup('fakedata', oCSPGroupsUpdateOCSPGroupBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-digicert_pki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('OCSPGroups', 'updateOCSPGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteOCSPGroup - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteOCSPGroup('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-digicert_pki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('OCSPGroups', 'deleteOCSPGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const oCSPGroupsAddCertsOCSPGroupBodyParam = [
      'string'
    ];
    describe('#addCertsOCSPGroup - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addCertsOCSPGroup('fakedata', oCSPGroupsAddCertsOCSPGroupBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-digicert_pki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('OCSPGroups', 'addCertsOCSPGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const oCSPGroupsRemoveCertsOCSPGroupBodyParam = [
      'string'
    ];
    describe('#removeCertsOCSPGroup - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.removeCertsOCSPGroup('fakedata', oCSPGroupsRemoveCertsOCSPGroupBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-digicert_pki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('OCSPGroups', 'removeCertsOCSPGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listOCSPGroupCerts - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.listOCSPGroupCerts('fakedata', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-digicert_pki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('OCSPGroups', 'listOCSPGroupCerts', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
  });
});
